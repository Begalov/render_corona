import bpy
import math, mathutils
import hashlib, shutil
import time
from .util  import *
from .properties.nodes import CoronaMtlNode, CoronaNode
import xml.etree.cElementTree as ElementTree
from xml.etree.cElementTree import Element, Comment, SubElement
import traceback
from pprint import pprint
import inspect

def addTexture(target, tex):
    # Definition
    texture = get_tex_path(tex.texture)
    xml_tone = SubElement(target, 'map')
    xml_tone.set('class', 'ToneMap')
    xml_child = SubElement(xml_tone, 'child')
    SubElement(xml_tone, 'multiplier').text = '%.5f %.5f %.5f' % (tex.intensity, tex.intensity, tex.intensity)
    xml_map = SubElement(xml_child, 'map')
    xml_map.set('class', 'Texture')
    SubElement(xml_map, 'image').text = texture
    xml_uvmap = SubElement(xml_map, 'uvMap')
    SubElement(xml_uvmap, 'mode').text = 'uvw'
    SubElement(xml_uvmap, 'scale').text = '%.5f %.5f 1.0' % (tex.uScaling, tex.vScaling)
    SubElement(xml_uvmap, 'offset').text = '%.5f %.5f 1.0' % (tex.uOffset, tex.vOffset)

#------------------------------------
# Material library writing function.
#------------------------------------
def write_mtl( root, default_mat: bool, mat_data, mtl_name:str, mtl_lib:bool):
    '''
    Write the material to the open mtl file
    mat_data is the variable for bpy.data.materials[material name]
    '''

    if default_mat or mat_data is None:
        # fw( 'Kd 0.8, 0.8, 0.8\n')
        if mtl_lib == False:
            xml_mat = SubElement(root, 'material')
        else:
            xml_mat_def = SubElement(root, 'materialDefinition', name=mtl_name)
            xml_mat = SubElement(xml_mat_def, 'material')
        xml_mat.set('class', 'Native')
        SubElement(xml_mat, 'diffuse').text = '0.6 0.6 0.6'
        return


    node = getOutputNode(mat_data)
    if node:
        if mtl_lib == False:
            mtllib = Element('mtl')
            mat = node.get_node_params(mtllib, True)
            if mat != None:
                root.append(mat)
        else:
            params = node.get_node_params(root)
            if params != None:
                xml_mat_def = SubElement(root, 'materialDefinition', name=mtl_name)
                xml_mat_def.append(params)
    else:

        crn_mat = mat_data.corona
        if mtl_lib == False:
            xml_mat = SubElement(root, 'material')
        else:
            xml_mat_def = SubElement(root, 'materialDefinition', name=mtl_name)
            xml_mat = SubElement(xml_mat_def, 'material')
        xml_mat.set('class', 'Native')
        write_list = []
        if crn_mat.mtl_type == 'coronaportalmtl' or crn_mat.as_portal:
            SubElement(xml_mat, 'portal').text = 'true'
            SubElement(xml_mat, 'opacity').text = '0 0 0'
            return

        elif crn_mat.mtl_type == 'coronamtl':
            # Diffuse
            xml_diff = SubElement(xml_mat, 'diffuse')
            if crn_mat.use_map_kd and crn_mat.map_kd.texture != '' and is_uv_img( crn_mat.map_kd.texture):
                addTexture(xml_diff, crn_mat.map_kd)
            else:
                diffuseMult = crn_mat.diffuse_level
                diffuse_col = [crn_mat.kd[0] * diffuseMult,
                               crn_mat.kd[1] * diffuseMult,
                               crn_mat.kd[2] * diffuseMult]
                xml_diff.text = '%.5f %.5f %.5f' % (diffuse_col[0],
                                            diffuse_col[1],
                                            diffuse_col[2])

            # Translucency
            if crn_mat.translucency[:] > (0.0, 0.0, 0.0) or (crn_mat.use_map_translucency and crn_mat.map_translucency.texture != '' and is_uv_img( crn_mat.map_translucency.texture)):
                xml_trans = SubElement(xml_mat, 'translucency')
                if crn_mat.use_map_translucency and crn_mat.map_translucency.texture != '' and is_uv_img( crn_mat.map_translucency.texture):
                    xml_col = SubElement(xml_trans, 'color')
                    addTexture(xml_col, crn_mat.map_translucency)
                elif crn_mat.translucency[:] > (0.0, 0.0, 0.0):
                    SubElement(xml_trans, 'color').text = '%.5f %.5f %.5f' % (crn_mat.translucency[0],
                                                                  crn_mat.translucency[1],
                                                                  crn_mat.translucency[2])
                if crn_mat.use_map_translucency_level and crn_mat.map_translucency_level.texture != '' and is_uv_img( crn_mat.map_translucency_level.texture):
                    xml_tl = SubElement(xml_trans, 'level')
                    addTexture(xml_tl, crn_mat.map_translucency_level)
                elif crn_mat.translucency[:] > (0.0, 0.0, 0.0) or (crn_mat.use_map_translucency and crn_mat.map_translucency.texture != ''):
                    SubElement(xml_trans, 'level').text = '%.5f' % crn_mat.translucency_level

            # Reflection
            xml_reflect = SubElement(xml_mat, 'reflect')
            if crn_mat.use_map_ks and crn_mat.map_ks.texture != '' and is_uv_img( crn_mat.map_ks.texture):
                xml_rc = SubElement(xml_reflect, 'color')
                addTexture(xml_rc, crn_mat.map_ks)
            else:
                SubElement(xml_reflect, 'color').text = '%.5f %.5f %.5f' % ((crn_mat.ks * crn_mat.ns)[:])

            # if crn_mat.use_map_ns and crn_mat.map_ns.texture != '':
            #     if is_uv_img( crn_mat.map_ns.texture):
            #         texture = get_tex_path(crn_mat.map_ns.texture)
            #         fw('map_Ns ":bitmap %.5f %.5f %.5f %.5f %.5f \\"%s\\""\n' % ( crn_mat.map_ns.intensity,
            #                                                                 crn_mat.map_ns.uOffset,
            #                                                                 crn_mat.map_ns.vOffset,
            #                                                                 crn_mat.map_ns.uScaling,
            #                                                                 crn_mat.map_ns.vScaling,
            #                                                                 texture))

            if crn_mat.use_map_ks_gloss and crn_mat.map_ks_gloss.texture != '' and is_uv_img( crn_mat.map_ks_gloss.texture):
                xml_rg = SubElement(xml_reflect, 'glossiness')
                addTexture(xml_rg, crn_mat.map_ks_gloss)
            else:
                SubElement(xml_reflect, 'glossiness').text = '%.5f' % crn_mat.reflect_glossiness

            SubElement(xml_reflect, 'ior').text = '%.5f' % crn_mat.reflect_fresnel

            # Anisotropy
            if crn_mat.anisotropy > 0.0:
                xml_ani = SubElement(xml_reflect, 'anisotropy')
                xml_ani.text = '%.5f' % crn_mat.anisotropy
                xml_anirot = SubElement(xml_reflect, 'anisoRotation')
                xml_anirot.text = '%.5f' % crn_mat.aniso_rotation

                if crn_mat.use_map_aniso and crn_mat.map_aniso.texture != '':
                    if is_uv_img( crn_mat.map_aniso.texture):
                        addTexture(xml_ani, crn_mat.map_aniso)
                if crn_mat.use_map_aniso_rot and crn_mat.map_aniso_rot.texture != '':
                    if is_uv_img( crn_mat.map_aniso_rot.texture):
                        addTexture(xml_anirot, crn_mat.map_aniso_rot)

            # Refraction
            # Write RefractMode even if refract level is 0, because it may be used by volumetrics, if enabled.
            if crn_mat.refract_level > 0.0 or crn_mat.scattering_albedo[:] > (0, 0, 0):
                xml_refract = SubElement(xml_mat, 'refract')

                if crn_mat.refract_level > 0.0 or crn_mat.scattering_albedo[:] > (0, 0, 0):

                    if crn_mat.refract_thin and crn_mat.refract_caustics:
                        SubElement(xml_refract, 'glassMode').text = 'Hybrid'
                    elif crn_mat.refract_caustics:
                        # If caustics is enabled
                        SubElement(xml_refract, 'glassMode').text = 'Caustics'
                    elif crn_mat.refract_thin:
                        # If only thin is enabled
                        SubElement(xml_refract, 'glassMode').text = 'thin'
                    else:
                        # If neither are enabled
                        SubElement(xml_refract, 'glassMode').text = 'Hybrid'
                        # fw( '#CORONA RefractMode \n')

                refract_level = crn_mat.refract_level
                xml_ref = SubElement(xml_refract, 'color')
                xml_ref.text = '%.5f %.5f %.5f' % ( (crn_mat.refract * refract_level)[:])
                if crn_mat.use_map_refract and crn_mat.map_refract.texture != '':
                    if is_uv_img( crn_mat.map_refract.texture):
                        addTexture(xml_ref, crn_mat.map_refract)

                SubElement(xml_refract, 'glossiness').text = '%.5f' % crn_mat.refract_glossiness
                SubElement(xml_refract, 'ior').text = '%.6f' % crn_mat.ni

            # Volumetrics
            xml_vol = None

            if crn_mat.absorption_distance > 0.0 or crn_mat.scattering_albedo[:] > (0, 0, 0):
                xml_vol = SubElement(xml_mat, 'volume')

                xml_attc = SubElement(xml_vol, 'attenuationColor')
                xml_attc.text = '%.4f %.4f %.4f' % (crn_mat.absorption_color)[:]
                SubElement(xml_vol, 'attenuationDist').text = '%.8f \n' % crn_mat.absorption_distance
                if crn_mat.use_map_absorption and crn_mat.map_absorption != '':
                    if is_uv_img( crn_mat.map_absorption.texture):
                        addTexture(xml_attc, crn_mat.map_absorption)

            xml_scatt = None
            if crn_mat.scattering_albedo[:] > (0, 0, 0):
                xml_scatt = SubElement(xml_vol, 'scatteringAlbedo')
                xml_scatt.text = '%.4f %.4f %.4f\n' % (crn_mat.scattering_albedo)[:]

            # Include scattering map even if scattering albedo is 0
            if crn_mat.use_map_scattering and crn_mat.map_scattering != '':
                if is_uv_img( crn_mat.map_scattering.texture):
                    if xml_scatt == None:
                        xml_scatt = SubElement(xml_vol, 'scatteringAlbedo')
                    addTexture(xml_scatt, crn_mat.map_scattering)

            if crn_mat.scattering_albedo[:] > (0, 0, 0): # or (crn_mat.use_map_scattering and crn_mat.map_scattering != ''):
                SubElement(xml_vol, 'meanCosine').text = '%.4f\n' % crn_mat.mean_cosine

            # Opacity
            # fw('#CORONA Opacity %.5f %.5f %.5f\n' % (crn_mat.opacity)[:])
            xml_opacity = SubElement(xml_mat, 'opacity')
            xml_opacity.text = '%.5f %.5f %.5f' % (crn_mat.opacity)[:]
            if crn_mat.use_map_opacity and crn_mat.map_opacity.texture != '':
                if is_uv_img( crn_mat.map_opacity.texture):
                    addTexture(xml_opacity, crn_mat.map_opacity)

            # # Normal
            # if crn_mat.use_map_normal and crn_mat.map_normal.texture != '':
            #     if is_uv_img( crn_mat.map_normal.texture):
            #         texture = get_tex_path(crn_mat.map_normal.texture)
            #         fw('#CORONA map_normal ":bitmap %.5f %.5f %.5f %.5f %.5f \\"%s\\""\n' % ( crn_mat.map_normal.intensity,
            #                                                                 crn_mat.map_normal.uOffset,
            #                                                                 crn_mat.map_normal.vOffset,
            #                                                                 crn_mat.map_normal.uScaling,
            #                                                                 crn_mat.map_normal.vScaling,
            #                                                                 texture))

            # Bump
            if crn_mat.use_map_bump and crn_mat.map_bump.texture != '':
                if is_uv_img( crn_mat.map_bump.texture):
                    addTexture(SubElement(xml_mat, 'bump'), crn_mat.map_bump)

            # # Rounded corners
            # if crn_mat.rounded_corners > 0.0:
            #     fw( '#CORONA RoundedCorners %.5f\n' % crn_mat.rounded_corners)
            #
            # # Ray invisibility
            # inv_dict = { 'ray_gi_inv':'gi',
            #              'ray_direct_inv':'direct',
            #              'ray_reflect_inv':'reflect',
            #              'ray_refract_inv':'refract',
            #              'ray_shadows_inv':'shadows'}
            # attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            # if len( attr_list) > 0:
            #     fw( 'Invisible ')
            #     for i in attr_list:
            #         fw( '%s ' % i)
            #     fw( '\n')
            # return

            emission = crn_mat.emission_mult
            if emission > 0.0:
                xml_em = SubElement(xml_mat, 'emission')
                xml_col = SubElement(xml_em, 'color')
                xml_gloss = SubElement(xml_em, 'glossiness').text = '%.6f' % crn_mat.emission_gloss
                if crn_mat.use_map_ke and crn_mat.map_ke.texture != '' and is_uv_img( crn_mat.map_ke.texture):
                    addTexture(xml_col, crn_mat.map_ke)
                else:
                    xml_col.text = '%.6f %.6f %.6f' % ( (crn_mat.ke * emission)[:])

                SubElement(xml_em, 'twosided').text = 'True' if crn_mat.both_sides else 'False'
                SubElement(xml_em, 'shadowcatcherIlluminator').text = 'True' if crn_mat.shadowcatcher_illuminator else 'False'

        elif crn_mat.mtl_type == 'coronalightmtl':
            emission = crn_mat.emission_mult
            SubElement(xml_mat, 'diffuse').text = '0 0 0'
            xml_em = SubElement(xml_mat, 'emission')
            xml_col = SubElement(xml_em, 'color')
            xml_gloss = SubElement(xml_em, 'glossiness').text = '%.6f' % crn_mat.emission_gloss
            if crn_mat.use_map_ke and crn_mat.map_ke.texture != '' and is_uv_img( crn_mat.map_ke.texture):
                addTexture(xml_col, crn_mat.map_ke)
            else:
                xml_col.text = '%.6f %.6f %.6f' % ( (crn_mat.ke * emission)[:])

            SubElement(xml_em, 'twosided').text = 'True' if crn_mat.both_sides else 'False'
            SubElement(xml_em, 'shadowcatcherIlluminator').text = 'True' if crn_mat.shadowcatcher_illuminator else 'False'

            if crn_mat.ies_profile != '':
                xml_ies = SubElement(xml_em, 'ies')

                SubElement(xml_ies, 'file').text = realpath( crn_mat.ies_profile)
                SubElement(xml_ies, 'sharpnessFake').text = str(crn_mat.keep_sharp).lower()
                mat = (mathutils.Matrix.Translation(crn_mat.ies_translate)
                    * mathutils.Matrix.Rotation((crn_mat.ies_rotate[0]), 4, 'X')
                    * mathutils.Matrix.Rotation((crn_mat.ies_rotate[1]), 4, 'Y')
                    * mathutils.Matrix.Rotation((crn_mat.ies_rotate[2]), 4, 'Z')
                    * mathutils.Matrix.Scale(crn_mat.ies_scale[0], 4, (1.0, 0.0, 0.0))
                    * mathutils.Matrix.Scale(crn_mat.ies_scale[1], 4, (0.0, 1.0, 0.0))
                    * mathutils.Matrix.Scale(crn_mat.ies_scale[2], 4, (0.0, 0.0, 1.0))
                    );
                SubElement(xml_ies, 'tm').text = getTransform(mat)

            xml_opacity = SubElement(xml_mat, 'opacity')
            xml_opacity.text = '%.5f %.5f %.5f' % (crn_mat.opacity)[:]
            if crn_mat.use_map_opacity and crn_mat.map_opacity.texture != '':
                if is_uv_img( crn_mat.map_opacity.texture):
                    addTexture(xml_opacity, crn_mat.map_opacity)

            # # Ray invisibility
            # inv_dict = { 'ray_gi_inv':'gi',
            #              'ray_direct_inv':'direct',
            #              'ray_reflect_inv':'reflect',
            #              'ray_refract_inv':'refract',
            #              'ray_shadows_inv':'shadows'}
            # attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            # if len( attr_list) > 0:
            #     fw( 'Invisible ')
            #     for i in attr_list:
            #         fw( '%s ' % i)
            #     fw( '\n')
            # return

        elif crn_mat.mtl_type == 'coronavolumemtl':
            # Defaults
            SubElement(xml_mat, 'diffuse').text = '0 0 0'
            SubElement(xml_mat, 'opacity').text = '0 0 0'
            # xml_refract = SubElement(xml_mat, 'refract')
            # SubElement(xml_refract, 'glassMode').text = 'Caustics'

            # Absorption
            if crn_mat.absorption_distance > 0.0 or crn_mat.scattering_albedo[:] > (0, 0, 0):
                xml_vol = SubElement(xml_mat, 'volume')

            xml_attc = SubElement(xml_vol, 'attenuationColor')
            xml_attc.text = '%.4f %.4f %.4f' % (crn_mat.absorption_color)[:]
            SubElement(xml_vol, 'attenuationDist').text = '%.8f \n' % crn_mat.absorption_distance
            if crn_mat.use_map_absorption and crn_mat.map_absorption != '':
                if is_uv_img( crn_mat.map_absorption.texture):
                    addTexture(xml_attc, crn_mat.map_absorption)

            xml_scatt = None
            if crn_mat.scattering_albedo[:] > (0, 0, 0):
                xml_scatt = SubElement(xml_vol, 'scatteringAlbedo')
                xml_scatt.text = '%.4f %.4f %.4f' % (crn_mat.scattering_albedo)[:]

            # Include scattering map even if scattering albedo is 0
            if crn_mat.use_map_scattering and crn_mat.map_scattering != '':
                if is_uv_img( crn_mat.map_scattering.texture):
                    if xml_scatt == None:
                        xml_scatt = SubElement(xml_vol, 'scatteringAlbedo')
                    addTexture(xml_scatt, crn_mat.map_scattering)

            if crn_mat.scattering_albedo[:] > (0, 0, 0): # or (crn_mat.use_map_scattering and crn_mat.map_scattering != ''):
                SubElement(xml_vol, 'meanCosine').text = '%.4f' % crn_mat.mean_cosine

            # Emission
            emission = crn_mat.emission_mult
            xml_em = SubElement(xml_mat, 'emission')
            xml_col = SubElement(xml_em, 'color')
            xml_gloss = SubElement(xml_em, 'glossiness')
            xml_gloss.text = '%.6f' % crn_mat.emission_gloss
            if crn_mat.use_map_ke and crn_mat.map_ke.texture != '' and is_uv_img( crn_mat.map_ke.texture):
                addTexture(xml_col, crn_mat.map_ke)
            else:
                xml_col.text = '%.6f %.6f %.6f' % ( (crn_mat.ke * emission)[:])

            # # Ray invisibility
            # inv_dict = { 'ray_gi_inv':'gi',
            #              'ray_direct_inv':'direct',
            #              'ray_reflect_inv':'reflect',
            #              'ray_refract_inv':'refract',
            #              'ray_shadows_inv':'shadows'}
            # attr_list = [inv_dict[c] for c in inv_dict if getattr( crn_mat, c)]
            # if len( attr_list) > 0:
            #     fw( 'Invisible ')
            #     for i in attr_list:
            #         fw( '%s ' % i)
            #     fw( '\n')

#------------------------------------
# Function for writing global medium to mtl file
#------------------------------------
def write_global_medium( root, scene):
    crn_world = scene.world.corona

    # if crn_world.absorption_distance > 0.0 or crn_world.scattering_albedo[:] > (0, 0, 0) or (crn_world.use_map_scattering and crn_world.map_scattering != '' and is_uv_img( crn_world.map_scattering.texture)):
    xml_mat = SubElement(root, 'material')
    xml_mat.set('class', 'Native')
    xml_vol = SubElement(xml_mat, 'volume')

    # if crn_world.absorption_distance > 0.0:
    xml_atcol = SubElement(xml_vol, 'attenuationColor')
    xml_atcol.text = '%.4f %.4f %.4f ' % (crn_world.absorption_color)[:]
    SubElement(xml_vol, 'attenuationDist').text = '%.8f' % crn_world.absorption_distance
    if crn_world.use_map_absorption and crn_world.map_absorption != '':
        if is_uv_img( crn_world.map_absorption.texture):
            addTexture(xml_atcol, crn_world.map_absorption)

    xml_scatcol = SubElement(xml_vol, 'scatteringAlbedo')
    xml_scatcol.text = '%.4f %.4f %.4f ' % (crn_world.scattering_albedo)[:]

    # Include scattering map even if scattering albedo is 0
    if crn_world.use_map_scattering and crn_world.map_scattering != '':
        if is_uv_img( crn_world.map_scattering.texture):
            addTexture(xml_scatcol, crn_world.map_scattering)

    # if crn_world.scattering_albedo[:] > (0, 0, 0) or (crn_world.use_map_scattering and crn_world.map_scattering != ''):
    SubElement(xml_vol, 'meanCosine').text = '%.4f' % crn_world.mean_cosine

#------------------------------------
# Material preview  writing function.
#------------------------------------
def export_preview( depsgraph,
                output_file,
                prev_mat,
                width,
                height,
                bpy_materials,
                bpy_textures):
    '''
    Configure for preview rendering
    '''
    # Used in engine.render_preview
    try:
        crn_mat = prev_mat.corona
        root = Element('mtlLib')

        result = "%d %d %s " % ( width, height, output_file)

        write_mtl( root, False, prev_mat, 'Preview', False)

        mtllib = list(root)
        if len(mtllib) > 0:
            # material = '<material class="Native"><diffuse>0.15 0.05 0.78</diffuse></material>'
            material = ElementTree.tostring(mtllib[0], encoding="unicode")
            result += material
            result += '\0'
            return result
        else:
            CrnError("No material is defined, is anything connected to the output node?")

        return None
        #mat_file.close()
    except:

        traceback.print_exc()
        CrnError("Unable to write material preview. Check permissions on addons directories, and that texture formats are of supported type (PNG, EXR, BMP, JPG).")
        return None

#------------------------------------
# Configuration file writing function.
#------------------------------------
def write_conf(self, context, conf_file):
    '''
    Open and write the .conf file
    '''
    scene = context.scene
    crn_scn = scene.corona
    camera = scene.camera
    try:
        conf = open(conf_file, 'w')
        cw = conf.write
        CrnUpdate("Writing configuration file ", conf_file.split(os.path.sep)[-1])

        cw('string system.threadPriority = %s\n'            % crn_scn.low_threadpriority)
        # cw('  bool fb.showBucketOrder = %s\n'           % ('true' if crn_scn.vfb_show_bucket else 'false'))
        cw('  bool shading.enable = true\n')
        cw('  bool shading.enableAa = %s\n'                         % ('true' if crn_scn.do_aa else 'false'))
        cw('string shading.renderEngine = %s\n'                 % crn_scn.renderer_type)
        cw('string geometry.accelerationStructure = Embree\n') # None, BvhSAH, Embree
        cw('   int shading.primarySolver = %s\n'             % crn_scn.gi_primarysolver)
        cw('   int shading.secondarySolver = %s\n'           % crn_scn.gi_secondarysolver)
        cw('string image.prefilter.type = %s\n'               % crn_scn.image_filter)
        cw('string lights.samplingMode = %s\n'               % 'MIS') # LightsOnly, Bsdf, MIS
        cw('string lights.solver = GroupNew\n') # Combined, GroupNew, Group, Photon, Hierarchical
        cw('string lights.enviroSolver = NormalDependent\n') # (def: NormalDependent, allowed values: Fast, Precise, FastCompensate, NormalDependent)
        cw(' float lights.enviroSubdivThreshold = %.5f\n' % crn_scn.subdiv_env_threshold)
        cw('string geometry.displace.subdivType = %s\n' % crn_scn.displace_type)
        cw(' float geometry.displace.maxSizeScreen = %.6f\n' % crn_scn.displace_max_screen) #  (def: 2, min: 0.01, max: 100)
        cw(' float geometry.displace.maxSizeWorld = %.6f\n' % crn_scn.displace_max_world) #  (def: 1, min: 1e-05, max: inf)
        cw('  bool geometry.displace.filterMap = %s\n' % ('true' if crn_scn.displace_filter_map else 'false')) #  (def: false)
        cw('  bool geometry.displace.smoothNormals = %s\n' % ('true' if crn_scn.displace_smooth_normals else 'false')) #  (def: true)
        cw(' float geometry.displace.maxSizeScreenOutFrustumMultiplier = %.6f\n' % crn_scn.displace_frustrum_mult) #  (def: 100, min: 1, max: 1e+09)
        cw(' float lights.texturedRes = %.5f\n'  % crn_scn.lights_tex_res)
        # cw('   int embree.triangles = 1\n')
        # cw('string system.randomSampler = HighDim5D\n') # Shared, PPerixelShirley, Deterministic5d, HighDim5D, HighDimOneSample, PerPixelTea, HighDOptimal, PerPixelXorshift128, PerPixelXorshift64Star

        cw('   int system.drMaxDumpPixels = 2048\n')

        cw('string denoise.filterType = %s\n'        % crn_scn.denoise_filter)
        cw(' float denoise.sensitivity = %.6f\n'        % crn_scn.denoise_sensitivity)
        cw(' float denoise.textureBlur = %.6f\n'        % crn_scn.denoise_blur)
        cw(' float denoise.blendAmount = %.6f\n'        % crn_scn.denoise_amt)

        # Max passes
        # max_passes = 0 if crn_scn.limit_prog_time else crn_scn.prog_max_passes
        cw('   int progressive.passLimit = %d\n'        % crn_scn.prog_max_passes)

        # Time limit
        hours = crn_scn.prog_timelimit_hour * 3600000 if crn_scn.limit_prog_time else 0
        mins = crn_scn.prog_timelimit_min * 60000 if crn_scn.limit_prog_time else 0
        secs = crn_scn.prog_timelimit_sec * 1000 if crn_scn.limit_prog_time else 0
        cw('   int progressive.timeLimit = %d\n'        % (hours + mins + secs))

        cw(' float lights.samplesMult = %.5f\n'         % crn_scn.arealight_samples)
        cw('   int lights.envResolution = %d\n'         % crn_scn.lights_env_res)
        cw('   int shading.giToAaRatio = %d\n'               % crn_scn.path_samples)
        cw('string lights.areaMethod = %s\n'            % crn_scn.arealight_method)
        cw('   int shading.maxRayDepth = %d\n'                  % crn_scn.max_depth)
        cw('   int system.vfbUpdateInterval = %d\n'            % crn_scn.vfb_update)
        cw('  bool system.vfbHistoryAutosave = false\n')
        cw('   int system.vfbHistoryMaxSizeMb = 512\n')

        # Check if we're using border render
        resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
        resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)

        if not scene.render.use_border:
            # If not, write 0 to .conf file
            cw('   int image.region.startX = 0\n')
            cw('   int image.region.startY = 0\n')
            cw('   int image.region.endX = 0\n')
            cw('   int image.region.endY = 0\n')
            cw('   int image.width = %d\n'              % resX)
            cw('   int image.height = %d\n'             % resY)
        elif scene.render.use_border and not scene.camera.data.corona.use_region:
            cw('   int image.region.startX = %d\n'      % int(scene.render.border_min_x * resX))
            cw('   int image.region.startY = %d\n'      % int(scene.render.border_min_y * resY))
            cw('   int image.region.endX   = %d\n'      % int(scene.render.border_max_x * resX))
            cw('   int image.region.endY   = %d\n'      % int(scene.render.border_max_y * resY))
            cw('   int image.width = %d\n'              % int(resX))
            cw('   int image.height = %d\n'             % int(resY))

        cw('  vec3 shading.exitColor = %.5f %.5f %.5f\n'        % (crn_scn.ray_exit_color)[:])

        # cw('   int fb.internalResolutionMult = %d\n'    % crn_scn.fb_res_mult)
        # cw(' float fb.filterWidth = %.5f\n'             % crn_scn.filter_width)
        # cw(' float fb.filterBlurring = %.5f\n'          % crn_scn.filter_blur)

        cw(' float geometry.maxNormalDiff = %.5f\n'              % crn_scn.max_normal_dev)
        cw('  bool adaptivity.enable = true\n')
        cw('   int adaptivity.interval = %d\n' % crn_scn.prog_recalculate)
        cw(' float adaptivity.targetError = %.5f\n' % crn_scn.prog_adaptivity)
        cw(' float shading.maxSampleIntensity = %.5f\n'         % crn_scn.max_sample_intensity)
        cw('   int system.randomSeed = %d\n'                   % crn_scn.random_seed)

        threads = thread_count if crn_scn.auto_threads else crn_scn.num_threads
        cw('   int system.numThreads = %d\n' % threads)

        cw(' float lights.solverFracLocal = 0.33\n')
        cw(' float lights.solverFractGlobal = 0.33\n')
        cw(' float lights.portalSampleFraction = %.5f\n' % crn_scn.portal_samples)
        cw(' float geometry.shadowBias = -6.07\n')
        #cw('  bool resumeRendering = true\n')
        cw('   int geometry.minInstanceSaving     = 50000\n')

        cw('string gi.uhdCache.precalcMode = %s\n' % crn_scn.hd_precomp_mode)
        cw(' float gi.uhdcache.precompDensity = %.5f\n'  % crn_scn.hd_precomp_mult)
        cw(' float gi.uhdcache.dirSensitivity = %.5f\n'  % crn_scn.hd_sens_direct)
        #cw(' float gi.hdCache.posSensitivity = %.5f\n'  % crn_scn.hd_sens_position)
        cw('   int gi.uhdcache.interpolationCount = %d\n'  % crn_scn.hd_interpolation_count)
        cw(' float gi.uhdcache.normalSensitivity = %.5f\n' % crn_scn.hd_sens_normal)
        cw('   int gi.uhdcache.recordQuality = %d\n'     % crn_scn.hd_pt_samples)
        cw(' float gi.uhdcache.glossThreshold = %.5f\n' % crn_scn.hd_glossy_thresh)
        cw(' float gi.uhdcache.strictness = %.3f\n'     % crn_scn.uhd_strictness) #   (def: 0.075, min: 0, max: 99)
        cw(' float gi.uhdcache.msi = %.3f\n'            % crn_scn.uhd_msi) #   (def: 3, min: 0, max: 9999)
        cw('  bool gi.uhdcache.correlatePrecomp = %s\n' % ('true' if crn_scn.uhd_correlatePrecomp else 'false')) #   (def: true)
        cw(' float gi.uhdcache.precision = %.3f\n'      % crn_scn.uhd_precision) #   (def: 1, min: 0.01, max: 20)

        # cw('   int gi.hdCache.maxRecords = %d\n'        % crn_scn.hd_max_records)
        # cw('   int gi.hdCache.writablePasses = %d\n'    % crn_scn.hd_write_passes)
        cw('  bool gi.uhdCache.save = %s\n'              % ('true' if crn_scn.save_secondary_gi and crn_scn.gi_secondarysolver == 'UHDCache' else 'false'))
        if crn_scn.save_secondary_gi or crn_scn.load_secondary_gi:
            cw('string gi.uhdCache.file = %s\n'            % confpath(crn_scn.gi_secondaryfile))
        # cw('  bool gi.uhdCache.doViz    = true\n')

        cw('   int gi.photons.emitted = %d\n'           % crn_scn.photons_emitted)
        cw('string gi.photons.filter = %s\n'            % crn_scn.photons_filter)
        cw('  bool gi.photons.storeDirect = %s\n'       % ('true' if crn_scn.photons_store_direct else 'false'))
        cw('   int gi.photons.depth = %d\n'             % crn_scn.photons_depth)
        cw('   int gi.photons.lookupCount = %d\n'       % crn_scn.photons_lookup)

        cw('   int gi.vpl.emittedCount = %d\n'          % crn_scn.vpl_emitted_count)
        cw('   int gi.vpl.usedCount = %d\n'             % crn_scn.vpl_used_count)
        cw('   int gi.vpl.progressiveBatch = %d\n'      % crn_scn.vpl_progressive_batch)
        cw(' float gi.vpl.clamping = %.5f\n'            % crn_scn.vpl_clamping)

        cw(' float geometry.bvh.costIteration = 1.0\n')
        cw(' float geometry.bvh.costTriangle = 1.0\n')
        cw('   int geometry.bvh.leafSizeMin = 2\n')
        cw('   int geometry.bvh.leafSizeMax = 6\n')

        cw(' float colorMap.simpleExposure = %.5f\n'       % crn_scn.colmap_exposure)
        cw(' float colorMap.gamma = %.5f\n'                % crn_scn.colmap_gamma)
        cw(' float colorMap.highlightCompression = %.5f\n' % crn_scn.colmap_compression)
        cw('  bool colorMap.usePhotographic = %s\n'        % ('true' if crn_scn.colmap_use_photographic else 'false'))
        cw(' float colorMap.iso = %.6f\n'                  % crn_scn.colmap_iso)
        cw(' float colorMap.fStop = %.5f\n'                % camera.data.corona.camera_dof)
        cw(' float colorMap.shutterSpeed = %.6f\n'         % crn_scn.shutter_speed)
        cw(' float colorMap.colorTemp = %.5f\n'            % crn_scn.colmap_color_temp)
        cw('  vec3 colorMap.tint = %.5f %.5f %.5f\n'       % (crn_scn.colmap_tint[0],
                                                                  crn_scn.colmap_tint[1],
                                                                  crn_scn.colmap_tint[2]))
        cw(' float colorMap.contrast = %.5f\n'             % crn_scn.colmap_contrast)
        cw(' float colorMap.saturation = %.3f\n'           % crn_scn.colmap_saturation)    # (def: 0, min: -1, max: 1)
        cw(' float colorMap.filmic.richShadows = %.3f\n'   % crn_scn.colmap_filmic_shadow) # (def: 0, min: 0, max: 1)
        cw(' float colorMap.filmic.highlightCompression = %.3f\n' % crn_scn.colmap_filmic_highlight) # (def: 0, min: 0, max: 1)

        #  Vec3Array colorMap.lightmixColors   (def: )
        # FloatArray colorMap.lightmixIntensities   (def: )
        cw('  bool colorMap.lut.enable = %s\n'         % ('true' if crn_scn.colmap_lut_enable else 'false')) # (def: false)
        cw('string colorMap.lut.path = %s\n'           % confpath(crn_scn.colmap_path))       # (def: )
        cw(' float colorMap.lut.opacity = %.3f\n'      % crn_scn.colmap_opacity)    # (def: 1, min: 0, max: 1)
        cw('  bool colorMap.lut.convertToLog = %s\n'   % ('true' if crn_scn.colmap_use_log else 'false'))    # (def: false)

        cw(' float vignette.intensity = %.3f\n'        % crn_scn.vignette_intensity) # (def: 0, min: 0, max: 1)
        cw(' float vignette.falloff = %.3f\n'          % crn_scn.vignette_falloff)   # (def: 0, min: 0, max: 1)
        cw(' float vignette.offset.x = %.3f\n'         % crn_scn.vignette_offset_x)  # (def: 0, min: -1, max: 1)
        cw(' float vignette.offset.y = %.3f\n'         % crn_scn.vignette_offset_y)  # (def: 0, min: -1, max: 1)

        cw('  bool bloomGlare.enabled = %s\n'          % ("true" if crn_scn.bloom_enabled else "false")) # (def: false)
        cw(' float bloomGlare.bloomIntensity = %.3f\n' % crn_scn.bloom_intensity)       # (def: 0, min: 0, max: 100000)
        cw(' float bloomGlare.glareIntensity = %.3f\n' % crn_scn.bloom_glare_intensity) # (def: 0, min: 0, max: 100000)
        cw('   int bloomGlare.streakCount = %d\n'      % crn_scn.bloom_streak_count)    # (def: 3, min: 1, max: 8)
        cw(' float bloomGlare.rotation = %.3f\n'       % crn_scn.bloom_rotation)        # (def: 0, min: -1e+10, max: 1e+10)
        cw(' float bloomGlare.streakBlur = %.3f\n'     % crn_scn.bloom_streak_blur)     # (def: 0, min: 0, max: 1)
        cw(' float bloomGlare.colorIntensity = %.3f\n' % crn_scn.bloom_color_intensity) # (def: 0, min: 0, max: 1)
        cw(' float bloomGlare.colorShift = %.3f\n'     % crn_scn.bloom_color_shift)     # (def: 0, min: 0, max: 1)
        cw(' float bloomGlare.threshold = %.3f\n'      % crn_scn.bloom_threshold)       # (def: 1, min: 0, max: 100000)

        cw('   int gi.ppm.ptSamplesPerIter = %d\n'     % crn_scn.ppm_samples)
        cw('   int gi.ppm.photonsPerIter = %d\n'       % crn_scn.ppm_photons)
        cw(' float gi.ppm.alpha = %.5f\n'              % crn_scn.ppm_alpha)
        cw(' float gi.ppm.initialRadius = %.5f\n'      % crn_scn.ppm_initial_rad)
        cw('  bool gi.vcm.mis = %s\n'                  % ("true" if crn_scn.bidir_mis else "false"))
        cw('string gi.vcm.mode = %s\n'                 % crn_scn.vcm_mode)

        # # Displacement isn't implemented yet.
        # # cw('  bool displace.useProjectionSize = true\n')
        # # cw(' float displace.maxProjectSize = 2\n')
        # # cw(' float displace.maxWorldSize = 1\n')
        # # cw('   int displace.maxSubdiv = 100\n')
        # cw('  bool saveExr = %s\n'                      % ("true" if crn_scn.image_format == '.exr' else "false"))
        # cw('  bool savePng = %s\n'                      % ("true" if crn_scn.image_format == '.png' else "false"))
        # cw('  bool saveJpg = %s\n'                      % ("true" if crn_scn.image_format == '.jpg' else "false"))
        # cw('  bool saveTiff     = %s\n'                 % ("true" if crn_scn.image_format == '.tiff' else "false"))
        # cw('  bool saveTga = %s\n'                      % ("true" if crn_scn.image_format == '.tga' else "false"))
        # cw('  bool saveAlpha    = %s\n'                 % str( crn_scn.save_alpha).lower())
        cw('  bool renderstamp.use = %s\n'              % ("true" if crn_scn.renderstamp_use else "false"))
        # cw(r'string renderStamp = "Corona Renderer Alpha | %c | Time: %pt | Passes: %pp | Primitives: %si | Rays/s: %pr"')
        #cw('\n   int vfb.type = %s\n'                     % crn_scn.vfb_type)

        # cw('Vec3Array colorMap.lightmixColors = 1.0 1.0 1.0, 1.0 1.0 1.0')
        # cw('FloatArray colorMap.lightmixIntensities = 0.8, 0.8')

        cw('  Float sharpening.amount = %.3f\n' % crn_scn.sharpen_amount)              #   (def: 1, min: 0, max: 10)
        cw('  Float sharpening.radius = %.3f\n' % crn_scn.sharpen_radius)              #   (def: 0.5, min: 0, max: 50)
        cw('  Float blurring.radius = %.3f\n' % crn_scn.blur_radius)                   #   (def: 1.33, min: 0, max: 50)
        cw('   Bool sharpening.blurring.enable = %s\n' % ("true" if crn_scn.sharpen_blur_enable else "false")) #   (def: false)

        #Close the file
        conf.close()
        return True
    except:
        traceback.print_exc()
        self.report({'ERROR'}, traceback.format_exc())
        return False

def setObjTransform(scene, xml_inst, obj, name, matrix):
    xml_trns = SubElement(xml_inst, 'transform')
    if use_ob_mb(obj, scene):
        mat_list = sample_mblur( obj, scene)
        xml_tm = SubElement(xml_trns, 'animatedTm')
        xml_tm.set('samples', "%i" % len(mat_list))
        for ob_mat in mat_list:
            SubElement(xml_tm, 'sample').text = getTransform(ob_mat)
    else:
        xml_trns.text = getTransform(matrix)
    if name:
        xml_trns.set('name', name_compat(name))

def readMaterials(mtlfilename):

    materials = []
    with open(mtlfilename, 'r', encoding="utf8") as file:
        materials = [(material[:-1]) for material in list(file)]

    # mtlid = materials[0];
    # materials = materials[1:]
    return materials

def addMaterials(root, obj, mtlfilename, write, crn_scn, mtllib=None):
    fn = None
    if mtlfilename:
        fn = os.path.join(resolve_export_path(crn_scn), mtlfilename)

    mtlid = None
    # Special handling here for material overrides even for proxy objects
    isoverride = crn_scn.material_override and not obj.corona.override_exclude
    if mtlfilename and not write and not isoverride:
        try:
            materials = readMaterials(fn)
        except:
            try:
                materials = get_instance_materials(obj, crn_scn)
            except:
                pass
    else:
        materials = get_instance_materials(obj, crn_scn) #obj.data.materials[:]
    # material_names = [m.name if m else None for m in materials]

    if mtlfilename and not mtllib:
        mtllib = os.path.join(resolve_export_path(crn_scn), mtlfilename.replace('.mtlindex', '.mtl'))
        sha = hashlib.sha1()
        sha.update(mtllib.replace(resolve_export_path(crn_scn), "").replace('/','\\').encode('utf8'))
        mtlid = sha.hexdigest()

    mtltxt = ''

    # avoid bad index errors
    if not materials or len(materials) == 0:
        xml_mat = SubElement(root, 'material')
        xml_mat.set('class', 'Reference')
        xml_mat.text = 'Default_corona_blender'
        return

    for mat_name in materials:
        xml_mat = SubElement(root, 'material')
        xml_mat.set('class', 'Reference')
        if not mat_name:
            xml_mat.text = 'Default_corona_blender'
        else:
            if hasattr(obj, "proxy") and obj.proxy:
                obj = obj.proxy
            # debug("Material debug", mat_name, obj)
            #if not is_proxy(obj, bpy.context.scene) and hasattr(obj, "library") and obj.library:
            if obj.corona.external_mtllib == '' and hasattr(obj, "library") and obj.library:
                mat_name = obj.library.name + '_' + mat_name
            xml_mat.text = resolveMaterialName(mat_name)
            if mtlid:
                mat_name = mat_name + '_' + mtlid
            mtltxt = mtltxt + mat_name + '\n'

    if mtlfilename and write:
        mtlfile = open(fn, 'w', encoding = "utf8")
        mtlfile.write(mtltxt)
        mtlfile.close()

def addPointLightMaterial(root, obj):
    xml_ray = SubElement(root, 'material')
    xml_ray.set('class', 'Rayswitcher')
    xml_normal = SubElement(xml_ray, 'normal')
    xml_mat = SubElement(xml_normal, 'material')
    xml_mat.set('class', 'Native')
    xml_emit = SubElement(xml_mat, 'emission')

    lamp = obj.data
    color = None
    energy = None
    debug("Point light material", lamp.use_nodes)
    if lamp.use_nodes:
        output = None
        for n in lamp.node_tree.nodes:
            if getattr(n, "is_active_output", True):
                output = n
        if output:
            socket = output.inputs["Surface"]
            if socket.is_linked:
                node = socket.links[0].from_node
            if node:
                nodeColor = node.inputs["Color"]
                nodeStrength = node.inputs["Strength"]
                color = nodeColor.default_value
                energy = nodeStrength.default_value * 2
                debug("Node setup: ", color, energy)

    if not color or not energy:
        # Use blender settings
        color = lamp.color
        energy = lamp.energy * 26400
    debug("Final color/energy", color, energy)
    SubElement(xml_emit, 'color').text = '%.4f %.4f %.4f' % (color[0] * energy, color[1] * energy, color[2] * energy)
    xml_mat.text = 'PointLight-%s' % obj.name

def saveLightmixGroup(obj, lightmixGroups, scn):

    group = None
    materials = get_instance_materials(obj, scn.corona)
    for matname in materials:
        try:
            mat = bpy.data.materials[matname]
            if hasattr(mat, 'corona'):
                crn_mat = mat.corona
                # node_tree = bpy.data.node_groups[ crn_mat.node_tree]
                # if node_tree is not None:
                #     for node in node_tree.nodes:
                #         if isinstance(node, CoronaMtlNode):
                #             print(dict(node))
                if crn_mat.use_lightmix:
                    empty = crn_mat.lightmix_group is None or crn_mat.lightmix_group == ""
                    if not empty:
                        group = crn_mat.lightmix_group
                    if empty and group is None:
                        group = obj.name
        except:
            pass

    if group is None:
        return

    debug("Found group " + group)
    if group not in lightmixGroups:
        lightmixGroups[group] = []

    debug("  appending " + obj.name)
    lightmixGroups[group].append(obj.name)

def add_camera(scene, root, camera):

    if scene.corona.use_cam_mblur:
        mat_list = sample_mblur( camera, scene)
        samples = "%i" % len(mat_list)
        xml_origin = SubElement(SubElement(root, 'origin'), 'animatedXyz')
        xml_origin.set('samples', samples)
        xml_target = SubElement(SubElement(root, 'target'), 'animatedXyz')
        xml_target.set('samples', samples)
        xml_roll = SubElement(SubElement(root, 'roll'), 'animatedXyz')
        xml_roll.set('samples', samples)

        for matrix in mat_list:
            a, b, roll = get_origin_target_roll(matrix)
            SubElement(xml_origin, 'sample').text = '%.4f %.4f %.4f' % a[0:3]
            SubElement(xml_target, 'sample').text = '%.4f %.4f %.4f' % b[0:3]
            SubElement(xml_roll, 'sample').text = '%.4f %.4f %.4f' % roll[0:3]

    else:
        matrix = camera.matrix_world
        a, b, roll = get_origin_target_roll(matrix)
        SubElement(root, 'origin').text = '%.4f %.4f %.4f' % a[0:3]
        SubElement(root, 'target').text = '%.4f %.4f %.4f' % b[0:3]
        SubElement(root, 'roll').text = '%.4f %.4f %.4f' % roll[0:3]

def get_origin_target_roll(matrix):
    a = matrix @ mathutils.Vector((0, 0, 0))
    b = matrix @ mathutils.Vector((0, 0, -1))
    c = matrix @ mathutils.Vector((0, 1, -1))
    roll = c - a
    roll = mathutils.Vector((roll[0], roll[1], roll[2])).normalized()
    return a, b, roll

def get_or_create_group(grp_dict, root, mesh_name):
    xml_grp = None
    if not mesh_name in grp_dict:
        xml_grp = SubElement(root, 'geometryGroup')
        xml_obj = SubElement(xml_grp, 'object')
        xml_obj.set('class', 'file')
        xml_obj.text = mesh_name
        grp_dict[mesh_name] = xml_grp
    else:
        xml_grp = grp_dict[mesh_name]
    return xml_grp


def add_region(xml_cam, scene, camera, crn_cam):
    if camera.data.shift_x != 0 or camera.data.shift_y != 0:
        xml_region = SubElement(xml_cam, 'region')
        resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
        resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)
        aspect = resX/resY
        if resX > resY:
            SubElement(xml_region, 'startX').text = '%.6f' % (0.0 + camera.data.shift_x)
            SubElement(xml_region, 'endX').text = '%.6f' % (1.0 + camera.data.shift_x)
            SubElement(xml_region, 'startY').text = '%.6f' % (0.0 + camera.data.shift_y * aspect)
            SubElement(xml_region, 'endY').text = '%.6f' % (1.0 + camera.data.shift_y * aspect)
        else:
            SubElement(xml_region, 'startX').text = '%.6f' % (0.0 + camera.data.shift_x / aspect)
            SubElement(xml_region, 'endX').text = '%.6f' % (1.0 + camera.data.shift_x / aspect)
            SubElement(xml_region, 'startY').text = '%.6f' % (0.0 + camera.data.shift_y)
            SubElement(xml_region, 'endY').text = '%.6f' % (1.0 + camera.data.shift_y)
        SubElement(xml_region, 'originalAspectRatio').text = '%.6f' % (aspect)

    if crn_cam.render_clipping:
        SubElement(xml_cam, 'minT').text = '%.6f' % camera.data.clip_start
        SubElement(xml_cam, 'maxT').text = '%.6f' % camera.data.clip_end

def write_scn( self, context, scn_file, conf_filename):
    '''
    Open and write the xml .scn file
    '''

    scene = context.depsgraph.scene
    crn_scn = scene.corona
    obj_ext = '.cgeo' if crn_scn.binary_obj else '.obj'
    mtlindex_ext = '.mtlindex'
    mtl_ext = '.mtl'
    camera = scene.camera
    width = scene.render.resolution_x
    height = scene.render.resolution_y
    SUCCESS = True
    FAILED = False

    lightmixGroups = {}

    try:
        CrnUpdate("Writing scene file ", scn_file.split(os.path.sep)[-1])
        scn = open(scn_file, 'wb')
        scnw = scn.write

        # CrnUpdate("creating root ")
        root = Element('scene')
        root.append(Comment('# Automatic Corona scene export - main file'))
        version = get_version_string()
        time_stamp = get_timestamp()
        root.append(Comment('# Generated by %s addon, %s, %s' % (script_name, version, time_stamp)))
        SubElement(root, 'confFile').text='%s' % conf_filename
        if scene.corona.corona_conf and os.path.exists(realpath(scene.corona.corona_conf)):
            SubElement(root, 'confFile').text='%s' % realpath(scene.corona.corona_conf)
        SubElement(root, 'mtllib').text='%s' % (name_compat(scene.name) + mtl_ext)

        # Camera.
        crn_cam = camera.data.corona

        xml_cam = SubElement(root, 'camera')
        xml_cam.set('class', crn_cam.camera_type)
        # pprint(camera.matrix_world, indent=2, depth=6)
        add_region(xml_cam, scene, camera, crn_cam)

        debug("Camera type", crn_cam.camera_type)
        # Ortho camera.
        if crn_cam.camera_type == 'ortho':
            SubElement(xml_cam, 'width').text='%.6f' % (crn_cam.ortho_width * 2)
            matrix = camera.matrix_world
            a, b, roll = get_origin_target_roll(matrix)
            SubElement(xml_cam, 'origin').text = '%.8f %.8f %.8f' % a[0:3]
            SubElement(xml_cam, 'target').text = '%.8f %.8f %.8f' % b[0:3]
            SubElement(xml_cam, 'roll').text = '%.8f %.8f %.8f' % roll[0:3]

        # Perspective camera.
        elif crn_cam.camera_type == 'perspective':

            add_camera(scene, xml_cam, camera)
            # FOV.
            SubElement(xml_cam, 'fov').text='%.6f' % (calc_fov(camera, width, height))

        # This is for TILTING the image plane for tilt shift photography
            # if camera.data.shift_x != 0 or camera.data.shift_y = 0:
            # # else:
            #     roll = camera.matrix_world.to_euler('XYZ')
            #     xVect = mathutils.Vector((1, 0, 0))
            #     # xVect = mathutils.Vector((camera.data.shift_x * math.sin(roll[1]), camera.data.shift_x * math.cos(roll[1]), 0))
            #     # xVect.rotate(camera.matrix_world.to_euler('XYZ'))
            #     yVect = mathutils.Vector((0, 0, 50000))
            #     # yVect = mathutils.Vector((camera.data.shift_y * math.sin(roll[1]), camera.data.shift_y * math.cos(roll[1]), 0))
            #     # yVect.rotate(camera.matrix_world.to_euler('XYZ'))
            #     SubElement(xml_cam, 'xVect').text='%.6f %.6f %.6f' % xVect[:]
            #     SubElement(xml_cam, 'yVect').text='%.6f %.6f %.6f' % yVect[:]

            debug("DOF", crn_cam.use_dof)
            if crn_cam.use_dof:
                debug("Work out focal distance")
                if camera.data.dof_object is not None:
                    focal_obj = bpy.data.objects[camera.data.dof_object.name]
                    focal_distance = (focal_obj.location - camera.location).magnitude
                else:
                    focal_distance = camera.data.dof_distance

                debug("Focal distance", focal_distance)
                debug("fstop", crn_cam.camera_dof)
                debug('filmWidth', camera.data.sensor_width)
                debug("Scale length", scene.unit_settings.scale_length)
                SubElement(xml_cam, 'focalDist').text = '%.6f' % focal_distance
                SubElement(xml_cam, 'fstop').text = '%.6f' % crn_cam.camera_dof
                SubElement(xml_cam, 'filmWidth').text = '%.6f' % (camera.data.sensor_width / (1000 * scene.unit_settings.scale_length))  # Convert to scene units
                debug("Finished setting fstop")
                if crn_cam.bokeh_blades >= 3:
                    xml_bok = SubElement(xml_cam, 'bokehPolygonal')
                    SubElement(xml_bok, 'blades').text = '%d' % crn_cam.bokeh_blades
                    SubElement(xml_bok, 'rotation').text = '%d' % math.degrees( crn_cam.bokeh_rotation)
                debug("Finished bokeh")
                debug(prettify(xml_cam))
                #     if crn_cam.use_bokeh_img and crn_cam.bokeh_img != '':
                #         scnw( 'bokehImg %s ' % realpath( crn_cam.bokeh_img))
        elif crn_cam.camera_type == 'cylindrical':
            add_camera(scene, xml_cam, camera)
            SubElement(xml_cam, 'height').text = '%f' % crn_cam.cylindrical_height

        elif crn_cam.camera_type == 'spherical' or crn_cam.camera_type == 'cubemap':
            add_camera(scene, xml_cam, camera)
            if crn_cam.use_vr:
                xml_vr = SubElement(xml_cam, 'vr')
                SubElement(xml_vr, 'eyeSeparation').text = '%f' % crn_cam.vr_eyeSeparation
                SubElement(xml_vr, 'frontOffset').text = '%f' % crn_cam.vr_frontOffset
                SubElement(xml_vr, 'convergenceDistance').text = '%f' % crn_cam.vr_convergenceDistance

        # if crn_cam.use_region and scene.render.use_border:
        #     resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
        #     resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)
        #     xml_reg = SubElement(xml_cam, 'region')
        #     SubElement(xml_reg, 'startX').text = '%f' % scene.render.border_min_x * resX
        #     SubElement(xml_reg, 'startY').text = '%f' % scene.render.border_min_y * resY
        #     SubElement(xml_reg, 'endX').text = '%f' % scene.render.border_max_x * resX
        #     SubElement(xml_reg, 'endY').text = '%f' % scene.render.border_max_y * resY

        # Objects
        self._no_export = {ob.corona.instance_mesh for ob in scene.objects if (ob.corona.is_proxy and not ob.corona.use_external)}

        # Add all objects used as dupli_objects for particle systems
        self._no_export.update( {ob.name for ob in get_all_psysobs()})

        #Export lamps
        for obj in scene.objects:
            if obj.type == 'LAMP' and not obj.hide_render and obj.is_visible(scene):
                if obj.data.type == 'POINT':
                    xml_grp = SubElement(root, 'geometryGroup')
                    xml_obj = SubElement(xml_grp, 'object')
                    xml_obj.set('class', 'sphere')
                    SubElement(xml_obj, 'materialId').text = '0'
                    xml_inst = SubElement(xml_grp, 'instance')
                    addPointLightMaterial(xml_inst, obj)
                    xml_trns = SubElement(xml_inst, 'transform')
                    matrix = obj.matrix_world.copy()
                    matrix[0][0] = obj.data.shadow_soft_size
                    matrix[1][1] = obj.data.shadow_soft_size
                    matrix[2][2] = obj.data.shadow_soft_size
                    xml_trns.text = getTransform(matrix)
                    xml_trns.set('name', obj.name)
                    saveLightmixGroup(obj, lightmixGroups, scene)

        resetMaterialNames()
        grp_dict = {}
        self._ext_psys_obs.clear()
        self._exported_obs.clear()
        self._dupli_obs.clear()
        self._ext_obs.clear()

        #-----------------------------------------------------------------------
        #Export objects
        for obj in scene.objects:
            debug("Object", obj, obj.type, obj.instance_type)
            self._psys_obs.clear()


            if obj.instance_type == 'COLLECTION':
                gi_in_export_layer = is_in_export_layer(obj, scene)
            else:
                gi_in_export_layer = None

            if do_export(obj, scene) or (obj.instance_type == 'COLLECTION' and gi_in_export_layer):
                debug("  Do export")

                # Don't include dupli- child objects.
                if obj.parent and obj.parent.instance_type in {'VERTS', 'FACES', 'COLLECTION'}:
                    continue

                # Include the duplis themselves.
                elif obj.is_instancer and obj.instance_type in {'VERTS', 'FACES', 'COLLECTION'}:
                    debug("Duplicaator")
                    dupli_list = get_instances(obj, scene)
                    total = sum([len(dupli[1]) for dupli in dupli_list])
                    debug("  Dupli obj count", total)
                    self._dupli_obs.extend(dupli_list)

                # Particle system emitter.
                elif is_psys_emitter( obj):
                    self._psys_obs = get_psys_instances(obj, scene)

                    #Include emitter geometry if set to render
                    if render_emitter( obj):
                        xml_grp = get_or_create_group(grp_dict, root, os.path.join('meshes', (name_compat(obj.name) + obj_ext)))
                        xml_inst = SubElement(xml_grp, 'instance')
                        addMaterials(xml_inst, obj, os.path.join('meshes', (name_compat(obj.name) + mtlindex_ext)), True, crn_scn)
                        setObjTransform(scene, xml_inst, obj, obj.name, obj.matrix_world.copy())
                        saveLightmixGroup(obj, lightmixGroups, scene)

                # Proxy object.
                elif is_proxy(obj, scene):
                    if obj.corona.use_external:
                        self._ext_obs.append( [ realpath( obj.corona.external_mtllib, obj),
                                        realpath( obj.corona.external_instance_mesh, obj),
                                        obj])
                    else:
                        self._dupli_obs.append([ scene.objects[obj.corona.instance_mesh], [obj.matrix_world]])

                elif obj.name not in self._no_export and not obj.corona.is_proxy:

                    name = get_export_name(self._exported_obs, obj)

                    xml_grp = get_or_create_group(grp_dict, root, os.path.join('meshes', (name_compat(name) + obj_ext)))
                    xml_inst = SubElement(xml_grp, 'instance')
                    addMaterials(xml_inst, obj, os.path.join('meshes', (name_compat(obj.name) + mtlindex_ext)), True, crn_scn)
                    setObjTransform(scene, xml_inst, obj, obj.name, obj.matrix_world.copy())
                    saveLightmixGroup(obj, lightmixGroups, scene)

            # Particle systems.
            if self._psys_obs and len( self._psys_obs) > 0:
                for dupli, inst_mats in self._psys_obs:
                    # debug("PSYS OBJ", ob)
                    # each 'ob' is a particle, as dict key
                    # The value is a list, containing the dupli.object and another list of matrices
                    # dupli = self._psys_obs[ob][0]       # The dupli.object
                    # inst_mats = self._psys_obs[ob][1] # The list of matrices
                    debug("dupli", dupli)
                    debug("matrices", inst_mats)
                    if is_proxy( dupli, scene) and dupli.corona.use_external:
                        self._ext_psys_obs.append( [ realpath( dupli.corona.external_mtllib, dupli),
                                        inst_mats,
                                        realpath( dupli.corona.external_instance_mesh, dupli),
                                        dupli])
                    else:
                        # debug("Dupli name", dupli.data.name, dupli.name)
                        xml_grp = get_or_create_group(grp_dict, root, os.path.join('meshes', (name_compat(dupli.data.name) + obj_ext)))
                        xml_inst = SubElement(xml_grp, 'instance')
                        addMaterials(xml_inst, dupli, os.path.join('meshes', (name_compat(dupli.data.name) + mtlindex_ext)), True, crn_scn)
                        for mat in inst_mats:
                            # For each matrix in the list
                            setObjTransform(scene, xml_inst, dupli, None, mat)
                            # xml_trns.set('name', dupli.name)
                            # saveLightmixGroup(ob, lightmixGroups, scene)

        debug("Duplis found", len(self._dupli_obs))
        dupli_start = time.time()
        # # Duplis.
        if len( self._dupli_obs) > 0:
            for ob in self._dupli_obs:
                # [object, list of matrices]
                # Write the transform first
                dupli = ob[0]
                inst_matrices = ob[1]
                # if len(ob) > 2:
                #     group = ob[2]
                #     name = name_compat(dupli.data.name) # name_compat(group.name) + '_' +
                # else:
                #     name = name_compat(dupli.data.name)

                if is_proxy( dupli, scene) and dupli.corona.use_external:
                    self._ext_psys_obs.append( [ realpath( dupli.corona.external_mtllib, dupli),
                                    inst_matrices,
                                    realpath( dupli.corona.external_instance_mesh, dupli),
                                    dupli])
                else:
                    name = get_export_name(self._exported_obs, dupli)
                    xml_grp = get_or_create_group(grp_dict, root, os.path.join('meshes', (name + obj_ext)))
                    xml_inst = SubElement(xml_grp, 'instance')
                    addMaterials(xml_inst, dupli, os.path.join('meshes', (dupli.name + mtlindex_ext)), True, crn_scn)
                    saveLightmixGroup(dupli, lightmixGroups, scene)
                    for mat in inst_matrices:
                        setObjTransform(scene, xml_inst, dupli, name, mat)
        dupli_end = time.time()
        debug('Dupli export took ', dupli_end - dupli_start, 's')

        external_mtllibs = []
        # External .objs.
        if len(self._ext_obs) > 0:
            for ob in self._ext_obs:
                mtllib = ob[0]
                inst_mesh = ob[1]
                dupli = ob[2]
                debug( 'mtllib %s\n' % mtllib)
                xml_grp = get_or_create_group(grp_dict, root, inst_mesh)
                xml_inst = SubElement(xml_grp, 'instance')
                mtlfilename = mtllib.replace('.mtl', mtlindex_ext)

                if dupli.corona.external_mtllib == '':
                    addMaterials(xml_inst, dupli, os.path.join('meshes', (dupli.name + mtlindex_ext)), True, crn_scn)
                else:
                    addMaterials(xml_inst, dupli, mtlfilename, False, crn_scn, mtllib)


                setObjTransform(scene, xml_inst, dupli, dupli.name, dupli.matrix_world.copy())
                saveLightmixGroup(dupli, lightmixGroups, scene)
                if mtllib and not mtllib in external_mtllibs and dupli.corona.external_mtllib != '':
                    external_mtllibs.append(mtllib)
                    try:
                        shutil.copy(mtllib, resolve_export_path(crn_scn))
                        mtllib = os.path.basename(mtllib)
                    except:
                        traceback.print_exc()
                    # to get around bug in corona
                    SubElement(root, 'mtllib').text='%s' % (mtllib)

        # External psys .objs.
        if len( self._ext_psys_obs) > 0:
            for ob in self._ext_psys_obs:
                mtllib = ob[0]
                inst_mats = ob[1]
                inst_mesh = ob[2]
                dupli = ob[3]

                xml_grp = get_or_create_group(grp_dict, root, inst_mesh)
                xml_inst = SubElement(xml_grp, 'instance')
                mtlfilename = mtllib.replace('.mtl', mtlindex_ext)

                if dupli.corona.external_mtllib == '':
                    mtlfilename = os.path.join('meshes', (dupli.name + mtlindex_ext))
                    addMaterials(xml_inst, dupli, mtlfilename, True, crn_scn)
                else:
                    addMaterials(xml_inst, dupli, mtlfilename, False, crn_scn, mtllib)

                for mat in inst_mats:
                    # For each matrix in the list
                    setObjTransform(scene, xml_inst, dupli, None, mat)
                    # xml_trns.set('name', obj.name)
                    # saveLightmixGroup(obj, lightmixGroups, scene)

                if mtllib and not mtllib in external_mtllibs and dupli.corona.external_mtllib != '':
                    external_mtllibs.append(mtllib)
                    try:
                        shutil.copy(mtllib, resolve_export_path(crn_scn))
                        mtllib = os.path.basename(mtllib)
                    except:
                        traceback.print_exc()
                    # to get around bug in corona
                    SubElement(root, 'mtllib').text='%s' % (mtllib)

        grp_dict = None

        # Environment.
        if scene.world is not None:
            crn_world = scene.world.corona
            if crn_world.mode == 'color':
                SubElement(root, 'environment').text = '%.5f %.5f %.5f' % (scene.world.corona.enviro_color)[:]
            if crn_world.mode == 'latlong' and crn_world.enviro_tex != '':
                xml_env = SubElement(root, 'environment')
                xml_tone_map = SubElement(xml_env, 'map')
                xml_tone_map.set('class', 'ToneMap')
                SubElement(xml_tone_map, 'multiplier').text = '%.3f %.3f %.3f' % (crn_world.map_gi.intensity, crn_world.map_gi.intensity, crn_world.map_gi.intensity)
                xml_child = SubElement(xml_tone_map, 'child')
                xml_map = SubElement(xml_child, 'map')
                xml_map.set('class', 'Texture')
                SubElement(xml_map, 'image').text = realpath(crn_world.enviro_tex)
                xml_uv = SubElement(xml_map, 'uvMap')
                SubElement(xml_uv, 'mode').text = crn_world.latlong_mode
                if crn_world.latlong_map_channel != -1:
                    SubElement(xml_uv, 'mapChannel').text = '%d' % crn_world.latlong_map_channel

                # Negative here is so the environment map is the right way around!
                SubElement(xml_uv, 'scale').text = '%.6f %.6f %.6f' % (-crn_world.latlong_scale[0], crn_world.latlong_scale[1], crn_world.latlong_scale[2])
                SubElement(xml_uv, 'offset').text = '%.6f %.6f %.6f' % crn_world.latlong_offset[:]
                SubElement(xml_uv, 'rotateZ').text = '%.6f' % (crn_world.latlong_rotate_z * 180 / math.pi)
                SubElement(xml_uv, 'enviroRotate').text = '%.6f' % (crn_world.latlong_enviro_rotate * 180 / math.pi + 180)
                SubElement(xml_uv, 'enviroMode').text = crn_world.latlong_enviro_mode
                SubElement(xml_uv, 'wrapModeU').text = crn_world.latlong_wrap_mode_u
                SubElement(xml_uv, 'wrapModeV').text = crn_world.latlong_wrap_mode_v
                SubElement(xml_uv, 'blur').text = '%.6f' % crn_world.latlong_blur
                SubElement(xml_uv, 'useRealWorldScale').text = 'True' if crn_world.latlong_use_real_scale else 'False'
                if crn_world.latlong_use_gamma == True:
                    SubElement(xml_map, 'gamma').text = '%.6f' % crn_world.latlong_gamma

            if crn_world.mode == 'sky':
                xml_env = SubElement(root, 'environment')
                xml_map = SubElement(xml_env, 'map')
                xml_map.set('class', 'Sky')
                SubElement(xml_map, 'groundColor').text = '%.5f %.5f %.5f' % (crn_world.sky_ground_color)[:]
                SubElement(xml_map, 'turbidity').text = '%.5f' % crn_world.sky_turbidity
                SubElement(xml_map, 'multiplier').text = '%.5f' % crn_world.sky_intensity
                SubElement(xml_map, 'skyAffectGround').text = 'true' if crn_world.sky_ground_affects_sky else 'false'

            if crn_world.mode == 'node':
                xml_global = SubElement(root, 'environment')
                world = bpy.data.worlds[0]
                mtl_name = world.name
                mat_data = world
                write_mtl(xml_global, False, mat_data, mtl_name, False)
            # if crn_world.mode == 'rayswitch':
            #     gi_switch = '\\":solid %.6f %.6f %.6f\\"' % ( crn_world.gi_color[0],
            #                                                   crn_world.gi_color[1],
            #                                                   crn_world.gi_color[2])

            #     reflect_switch = '\\":solid %.6f %.6f %.6f\\"' % ( crn_world.reflect_color[0],
            #                                                   crn_world.reflect_color[1],
            #                                                   crn_world.reflect_color[2])

            #     refract_switch = '\\":solid %.6f %.6f %.6f\\"' % ( crn_world.refract_color[0],
            #                                                   crn_world.refract_color[1],
            #                                                   crn_world.refract_color[2])

            #     direct_switch = '\\":solid %.6f %.6f %.6f\\"' % ( crn_world.direct_color[0],
            #                                                   crn_world.direct_color[1],
            #                                                   crn_world.direct_color[2])

            #     if crn_world.gi_use_tex and crn_world.map_gi.texture != '':
            #         map_gi = crn_world.map_gi
            #         scnw( 'newmap GiSwitch ":bitmap %.6f %.6f %.6f %.6f %.6f \\"%s\\""\n' % (map_gi.intensity,
            #                                                                           map_gi.uOffset,
            #                                                                           map_gi.vOffset,
            #                                                                           map_gi.uScaling,
            #                                                                           map_gi.vScaling,
            #                                                                           realpath( map_gi.texture)))
            #         gi_switch = ':GiSwitch'
            #     if crn_world.reflect_use_tex and crn_world.map_reflect.texture != '':
            #         map_reflect = crn_world.map_reflect
            #         scnw( 'newmap ReflectSwitch ":bitmap %.6f %.6f %.6f %.6f %.6f \\"%s\\""\n' % (map_reflect.intensity,
            #                                                                           map_reflect.uOffset,
            #                                                                           map_reflect.vOffset,
            #                                                                           map_reflect.uScaling,
            #                                                                           map_reflect.vScaling,
            #                                                                           realpath( map_reflect.texture)))
            #         reflect_switch = ':ReflectSwitch'
            #     if crn_world.refract_use_tex and crn_world.map_refract.texture != '':
            #         map_refract = crn_world.map_refract
            #         scnw( 'newmap RefractSwitch ":bitmap %.6f %.6f %.6f %.6f %.6f \\"%s\\""\n' % (map_refract.intensity,
            #                                                                           map_refract.uOffset,
            #                                                                           map_refract.vOffset,
            #                                                                           map_refract.uScaling,
            #                                                                           map_refract.vScaling,
            #                                                                           realpath( map_refract.texture)))
            #         refract_switch = ':RefractSwitch'
            #     if crn_world.direct_use_tex and crn_world.map_direct.texture != '':
            #         map_direct = crn_world.map_direct
            #         scnw( 'newmap DirectSwitch ":bitmap %.6f %.6f %.6f %.6f %.6f \\"%s\\""\n' % (map_direct.intensity,
            #                                                                           map_direct.uOffset,
            #                                                                           map_direct.vOffset,
            #                                                                           map_direct.uScaling,
            #                                                                           map_direct.vScaling,
            #                                                                           realpath( map_direct.texture)))
            #         direct_switch = ':DirectSwitch'

            #     scnw( 'enviro texmap ":rayswitch %s %s %s %s"\n' % ( gi_switch, reflect_switch, refract_switch, direct_switch))

            if crn_world.use_global_medium:
                xml_global = SubElement(root, 'globalMedium')
                write_global_medium(xml_global, scene)

        else:
            CrnInfo( "No environment is specified! Add a new world in the World tab. Rendering with default environment.")
            SubElement(root, 'environment').text = '0.8 0.8 0.8'

        if scene.corona_sky.use_sun:
            if scene.corona_sky.sun_lamp == '':
                CrnInfo( "Sun lighting is enabled, but no sun lamp found in the scene!")
            else:
                sun = scene.objects[scene.corona_sky.sun_lamp]
                matrix = sun.matrix_world
                xml_sun = SubElement(root, 'sun')
                SubElement(xml_sun, 'dirTo').text = '%s %s %s' % (matrix[0][2], matrix[1][2], matrix[2][2])

                sun_col = scene.corona_sky.sun_color

                SubElement(xml_sun, 'colorMult').text = '%.5f %.5f %.5f' % ((sun_col)[:])
                SubElement(xml_sun, 'colorMode').text = '%s' % scene.corona_sky.color_mode
                SubElement(xml_sun, 'turbidity').text = '%.3f' % scene.corona_sky.turbidity

                SubElement(xml_sun, 'sizeMult').text = '%.5f' % scene.corona_sky.sun_size_mult
                SubElement(xml_sun, 'visibleDirect').text = 'true'
                SubElement(xml_sun, 'visibleReflect').text = 'true'
                SubElement(xml_sun, 'visibleRefract').text = 'true'

        # Render passes.
        # attr_list = []
        components_list = ['diffuseDirect',
                            'reflectDirect',
                            'refractDirect',
                            'translucencyDirect',
                            'volumetricDirect',
                            'diffuseIndirect',
                            'reflectIndirect',
                            'refractIndirect',
                            'translucencyIndirect',
                            'volumetricIndirect',
                            'emission']

        debug("Finished scene exporting")
        if lightmixGroups:
            if scene.corona_sky.use_sun and scene.corona_sky.sun_lamp != '':
                xml_el = SubElement(root, 'renderElement')
                xml_el.set('class', 'LightSelect')
                xml_el.set('instanceName', 'Sun')
                SubElement(xml_el, 'includeSun')
            xml_el = SubElement(root, 'renderElement')
            xml_el.set('class', 'LightSelect')
            xml_el.set('instanceName', 'Environment')
            SubElement(xml_el, 'includeEnvironment') # this includes the sun!

            for group in lightmixGroups:
                xml_el = SubElement(root, 'renderElement')
                xml_el.set('class', 'LightSelect')
                xml_el.set('instanceName', group)
                objects = lightmixGroups[group]
                for obj in objects:
                    SubElement(xml_el, 'included').text = obj

            xml_el = SubElement(root, 'renderElement')
            xml_el.set('class', 'LightMix')
            xml_el.set('instanceName', 'LightMix')

        # SubElement(xml_el, 'includeSun')
        # <renderElement class="LightSelect">*<included>[string]</included>?<includeEnvironment>?<includeSun></renderElement>
        for item in scene.corona_passes.passes:
            if item:
                # attr_list.clear()
                item_name = ('_').join(item.name.split())
                pass_type = item.render_pass
                xml_el = SubElement(root, 'renderElement')
                xml_el.set('class', pass_type)
                xml_el.set('instanceName', item_name)
                if pass_type in {'ZDepth', 'Components', 'RawComponent', 'SourceColor', 'Id', 'Normals'}:
                    # scnw( 'renderPass %s "%s" ' % (item.render_pass, item_name))
                    if pass_type == 'ZDepth':
                        # attr_list.extend( [item.z_min, item.z_max])
                        SubElement(xml_el, 'minimalT').text = '%.4f' % item.z_min
                        SubElement(xml_el, 'maximalT').text = '%.4f' % item.z_max
                    if pass_type == 'Components':
                        for c in components_list:
                            if getattr( item, c):
                                SubElement(xml_el, 'componentName').text = c
                    if pass_type == 'RawComponent':
                        xml_el.text = item.components
                    if pass_type == 'SourceColor':
                        xml_el.text = item.source_color
                        # attr_list.append( item.source_color)
                    if pass_type == 'Id':
                        xml_el.text = item.mask_id
                        # attr_list.append( item.mask_id)
                    if pass_type == 'Normals':
                        xml_el.text = item.normals
                        # attr_list.append( item.normals)
                    # for attr in attr_list:
                    #     if isinstance( attr, str):
                    #         attr = attr.replace("_i", "I")
                    #     scnw('%s ' % attr)
                    # scnw( '\n')
                # else:
                #     xml_el = SubElement(root, 'renderElement')
                #     xml_el.set('class', pass_type)
                #     xml_el.set('instanceName', item_name)
                    # scnw('renderPass %s "%s"\n' % (item.render_pass, item_name))


        # Cleanup.
        scnw(prettify(root))
        scn.close()


        return SUCCESS
    except:
        traceback.print_exc()
        CrnUpdate("Failed :(")
        self.report({'ERROR'}, traceback.format_exc())
        # This exception doesn't actually work, it just catches tracebacks
        return FAILED
