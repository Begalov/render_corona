import bpy
import nodeitems_utils
import subprocess, os, time, sys, signal, platform
import threading
import tempfile
from .util                  import realpath, resolution, plugin_path, get_instance_materials, get_version
#, get_bitbucket_version
from .util                  import sep, CrnUpdate, CrnProgress, CrnError, CrnInfo, debug, name_compat, resolve_export_path, findCoronaOutputNode
from .outputs               import export_preview
from .properties.nodes      import CoronaNode
from .cycles                import migrate_cycles_nodes
import traceback
import json
from bpy.app.handlers   import persistent
#------------------
# Generic Utilities
#------------------

@persistent
def scene_loaded(depsgraph):
    # Used in __init__.registerHandlers,unregisterHandlers
    test_renderer_changed()
    renderer = bpy.context.scene.render.engine
    if renderer == "CORONA":
        try:
            migrate_old_node_tree()
            # migrate_corona_use_nodes()
            migrate_cycles_nodes()
        except:
            print("Unexpected error during scene change:", sys.exc_info()[0])
            traceback.print_exc()

def migrate_corona_use_nodes():
    materials = bpy.data.materials
    debug("Migrating corona.uses_nodes", len(materials))
    for material in materials:
        debug(" ", material)
        crn_mat = material.corona
        if not crn_mat.use_nodes:
            if findCoronaOutputNode(material):
                crn_mat.use_nodes = True

def migrate_old_node_tree():
    # For each material
    # Get corona_material
    # Check if it has a node_tree
    # Load from bpy.data.node_groups
    # migrate all nodes and links to material.node_tree
    # remove corona_material.node_tree
    materials = bpy.data.materials
    debug("Migrating materials", len(materials))
    for material in materials:
        debug(" ", material)
        crn_mat = material.corona
        node_tree = None
        if hasattr(crn_mat, "node_tree") and \
            crn_mat.node_tree != '' and \
            bpy.data.node_groups.find(crn_mat.node_tree) != -1:
            node_tree = bpy.data.node_groups[crn_mat.node_tree]
            # reset it here so we don't get into a loop
            crn_mat.node_tree = ''
        debug("  ", node_tree)
        material.use_nodes = True
        if node_tree:
            crn_mat.use_nodes = True
            mtree = material.node_tree
            debug("     migration tree", mtree)
            if mtree:
                mnodes = mtree.nodes
                mnewNode = mnodes.new
                mnewLink = mtree.links.new
                nodes = node_tree.nodes
                links = node_tree.links
                debug("     Migrating nodes ", len(nodes), len(links))
                for node in nodes:
                    if isinstance(node, CoronaNode):
                        mnode = mnewNode(node.bl_idname)
                        debug("       New node", mnode, dict(node))
                        for key, value in dict(node).items():
                            debug("           ", key, value)
                            mnode[key] = value
                        mnode.name = node.name
                        mnode.height = node.height
                        mnode.width = node.width
                        mnode.location = node.location
                        for input in node.inputs:
                            if mnode.inputs.find(input.name) != -1:
                                minput = mnode.inputs[input.name]
                                for key, value in dict(input).items():
                                    debug("           ", input.name, key, value)
                                    mkey = key
                                    if mkey == "socket_val":
                                        mkey = "default_value"
                                    minput[mkey] = value
                for link in links:
                    try:
                        if isinstance(link.from_node, CoronaNode):
                            debug("       Old link", link.from_node, link.to_node)
                            mfrom_node = mnodes[link.from_node.name]
                            if link.from_socket.is_output:
                                mfrom_socket = mfrom_node.outputs[link.from_socket.identifier]
                            else:
                                mfrom_socket = mfrom_node.inputs[link.from_socket.identifier]
                            mto_node = mnodes[link.to_node.name]
                            if link.to_socket.is_output:
                                mto_socket = mto_node.outputs[link.to_socket.identifier]
                            else:
                                mto_socket = mto_node.inputs[link.to_socket.identifier]
                            mlink = mnewLink(mfrom_socket, mto_socket)
                            debug("       New link", mlink)
                    except:
                        print("Unexpected error:", sys.exc_info()[0])
                        raise



_old_renderer = '_____'
unregistered_categories = {}
COMMON_NODE_CATEGORIES = ['COMPOSITING', 'TEXTURE', 'CORONA_NODES']

@persistent
def test_renderer_changed(dummy = None):
    # Used in __init__.registerHandlers,unregisterHandlers and scene_loaded()
    global _old_renderer
    renderer = bpy.context.scene.render.engine
    if _old_renderer != renderer:
        debug("Renderer change", _old_renderer, '->', renderer)
        if renderer == "CORONA":
            try:
                hide_other_categories()
            except:
                print("Unexpected error during render change:", sys.exc_info()[0])
        else:
            show_other_categories()
        _old_renderer = renderer

def hide_other_categories():
    global unregistered_categories, COMMON_NODE_CATEGORIES
    unregistered_categories = {}
    for ident, node_item in nodeitems_utils._node_categories.items():
        if ident not in COMMON_NODE_CATEGORIES:
            debug("Hiding", ident)
            unregistered_categories[ident] = node_item
    for ident, node_item in unregistered_categories.items():
        nodeitems_utils.unregister_node_categories(ident)

def show_other_categories():
    global unregistered_categories
    for ident, node_item in unregistered_categories.items():
        nodeitems_utils.register_node_categories(ident, node_item[0])
    unregistered_categories = {}

def update_start( engine, data, depsgraph):
    if engine.is_preview:
        update_preview( engine, data, depsgraph)
    else:
        update_scene( engine, data, depsgraph)

def render_start( engine, depsgraph):
    # Used in RenderCorona.render
    if engine.is_preview:
        render_preview( engine, depsgraph)
    else:
        if engine.animation:
            old_vfb = crn_scene_props.vfb_type
            crn_scene_props.vfb_type = 0
            # Step through frames to render animation
            # animation can only be True if rendering with command line
            frame_start = depsgraph.scene.frame_start
            frame_current = frame_start
            depsgraph.scene.frame_set( frame_start)
            frame_end = depsgraph.scene.frame_end
            step = depsgraph.scene.frame_step
            while frame_current <= frame_end:
                render_scene( engine, depsgraph)
                frame_current += frame_step
                depsgraph.scene.frame_set( frame_current)
            crn_scene_props.vfb_type = old_vfb
        else:
            render_scene( engine, depsgraph)

def render_init( engine):
    pass

def update_preview( engine, data, depsgraph):
    pass

def update_scene( engine, data, depsgraph):
    pass

from pprint import pprint

def gatherOutput(proc, startup, until, showOutput = True):
    buf = ''
    while proc.poll() == None:
        data = proc.stdout.read(1)
        buf += data
        if showOutput:
            sys.stdout.write(data)
            sys.stdout.flush()
        if buf == until or data == '\n':
            if buf == until:
                return True
            else:
                buf = ''
    return False

def update_result(engine, width, height, output_file):
    # debug("Reading result %s %d %d" % (output_file, width, height))
    try:
        result = engine.begin_result( 0, 0, width, height)
        lay = result.layers[0]
        lay.load_from_file( output_file)
        engine.end_result( result, cancel=False, do_merge_results=True)
        return True
    except:
        traceback.print_exc()
        engine.end_result( None, cancel=True, do_merge_results=False)
        pass
    return False

corona_proc = None
uid = 0
def kill_preview():
    global corona_proc
    # debug("Preview process", corona_proc)
    if corona_proc and corona_proc.poll() == None:
        corona_proc.terminate()
#----------------------------------
# Render the material preview
#---------------------------------
def render_preview( engine, depsgraph):
    # Used in RenderCorona.render, render_start
    global corona_proc, uid

    if bpy.context.preferences.addons[__package__].preferences.corona_path == '':
        engine.report( {'INFO'}, 'Error: The binary path is unspecified! Check Corona addon user preferences.')
        return

    if not os.path.exists(bpy.context.preferences.addons[__package__].preferences.corona_path):
        engine.report( {'INFO'}, 'Error: Could not find the corona executable, please check the addon user perferences')
        return

    # Iterate through the preview scene, finding objects with materials attached
    objects_materials = {}
    (width, height) = resolution( depsgraph.scene)

    for object in [ob for ob in depsgraph.objects if not ob.hide_viewport and not ob.hide_render]: #ob.is_visible( scene) 
        for matname in get_instance_materials( object, depsgraph.scene.corona):
            try:
                mat = bpy.data.materials[matname]
                if mat is not None:
                    if not object.name in objects_materials.keys(): objects_materials[object] = []
                    objects_materials[object].append( mat)
            except:
                pass
    # find objects that are likely to be the preview objects
    preview_objects = [o for o in objects_materials.keys() if o.name.startswith( 'preview')]
    if len( preview_objects) < 1:
        return

    # find the materials attached to the likely preview object
    likely_materials = objects_materials[preview_objects[0]]
    if len( likely_materials) < 1:
        return

    corona_path = None
    debug('Use material preview path: %s' % bpy.context.preferences.addons[__package__].preferences.corona_use_mtl_preview)
    if bpy.context.preferences.addons[__package__].preferences.corona_use_mtl_preview:
        corona_path = realpath( bpy.context.preferences.addons[__package__].preferences.corona_mtl_preview )
        debug('Material preview path: %s' % corona_path)

    if not corona_path or not os.path.exists(corona_path):
        corona_path = realpath( bpy.context.preferences.addons[__package__].preferences.corona_path )

    debug('Final material preview path: %s' % corona_path)

    uid += 1
    if uid > 20:
        uid = 0

    # with tempfile.TemporaryDirectory() as tempdir:
    tempdir = tempfile.TemporaryDirectory()
    if tempdir:
        output_path = tempdir.name
        if not os.path.isdir( output_path):
            os.makedirs( output_path, exist_ok = True)
        output_file = os.path.join( output_path, "matpreview%d.jpg" % uid)
        pm = likely_materials[0]

        preview_quality = pm.corona.preview_quality
        if preview_quality > 0.5 and width < 50:
            preview_quality = 0.5
        debug('%.2f %d %d %s' % (preview_quality, width, height, output_file))
        exporter = export_preview( depsgraph, output_file, pm, width, height, bpy.data.materials, bpy.data.textures)
        debug('material', exporter)
        if exporter == None:
            CrnError( 'Error while exporting -- check the console for details.')
            return
        else:
            if not bpy.app.background:
                startup = False
                # Start the preview process if it isn't running
                if corona_proc == None or corona_proc.poll() != None:
                    debug("Starting preview process")
                    cmd = ( corona_path, '-mtlPreview')
                    startup = True
                    corona_proc = subprocess.Popen( cmd,stderr=subprocess.PIPE,stdout=subprocess.PIPE,stdin=subprocess.PIPE,shell=(os.name!='posix'),cwd=output_path,bufsize=1,universal_newlines=True)
                    gatherOutput(corona_proc, startup, '\n')
                    debug("Ready to render previews")
                    startup = False

                # Preview at real low quality first
                # if pm.corona.preview_quality > 1 and width > 100:
                #     preview_quality = 0.1
                #     corona_proc.stdin.write('%.3f %s' % (preview_quality, exporter))
                #     corona_proc.stdin.flush()
                #     gatherOutput(corona_proc, startup, '.')
                #     update_result(engine, width, height, output_file)

                # This is the real preview render
                preview_quality = pm.corona.preview_quality
                if preview_quality > 1 and width < 50:
                    preview_quality = 1
                corona_proc.stdin.write('%.3f %s' % (preview_quality, exporter))
                corona_proc.stdin.flush()
                gatherOutput(corona_proc, startup, '.', showOutput = False)

                if not update_result(engine, width, height, output_file):
                    err_msg = 'Error: Could not load render result from %s.' % output_file
                    CrnError( err_msg)

                # sys.stdout.write('\n')
                # sys.stdout.flush()
                # try:
                #     os.remove(output_file)
                # except:
                #     pass

def kill(process: subprocess.Popen):
    DELAY = 0.5
    debug('Trying to kill the process')
    if process.poll() == None:
        try:
            debug('Terminate')
            process.terminate()
            process.wait(DELAY)
        except:
            traceback.print_exc()
    if process.poll() == None:
        try:
            debug('Kill')
            process.kill()
            time.sleep( DELAY)
        except:
            traceback.print_exc()
    if process.poll() == None and sys.platform == 'win32':
        try:
            debug('taskkill')
            import ctypes
            ctypes.windll.kernel32.GenerateConsoleCtrlEvent(1, process.pid)
            # os.kill(process.pid, signal.CTRL_C_EVENT)
            # os.system('taskkill /f /pid %d' % process.pid)
            time.sleep( DELAY)
        except:
            traceback.print_exc()

#----------------------------------
# Render and export the scene
#---------------------------------
def render_scene( engine, depsgraph):
    # Used in render_start()
    DELAY = 0.5

    if depsgraph.scene.corona.export_path == '':
        engine.report({'WARNING'}, "Export path is not specified!  Set export path in Render tab, under Corona Render panel.")
        return

    render_dir = 'render'
    bpy.ops.corona.export_scene()
    bpy.ops.corona.export_mat()
    scene = depsgraph.scene
    if scene.corona.obj_export_bool == True:
        # bpy.context.window_manager['corona'] = None
        result = bpy.ops.corona.export()
        if result != {'FINISHED'}:

            engine.report({'WARNING'}, bpy.context.window_manager['corona']['msg'])
            # bpy.context.window_manager['corona'] = None
            return

    export_path = resolve_export_path(scene.corona)
    render_output = os.path.join( export_path, render_dir)
    width = scene.render.resolution_x
    height = scene.render.resolution_y

    # Make the render directory, if it doesn't exist
    if not os.path.isdir( render_output):
        os.makedirs( render_output, exist_ok = True)

    try:
        item == os.path.join(render_output, name_compat(scene.name) + '_' + str( scene.frame_current) + scene.corona.image_format)
        if os.path.exists(item):
            os.remove(item)
    except:
        pass

    # Set filename to render
    filename = name_compat(scene.name) + ".scn"

    # filename = os.path.join( export_path, filename)
    imagename = os.path.join( render_output, ( name_compat(scene.name) + str( scene.frame_current)))

    # Start the Corona executable
    # Get the absolute path to the executable dir
    corona_path = realpath( bpy.context.preferences.addons[__package__].preferences.corona_path)

    cmd = []

    # Doesn't work due to bug in corona
    # Can't set current directory
    #
    # if sys.platform == 'darwin':
    #     cmd.append('/usr/bin/open')
    #     cmd.append('-a')
    #     cmd.append('/Applications/Corona Standalone.app')
    #     cmd.append('-W') # Wait till it ends
    #     cmd.append('-n') # Open new instance
    #     cmd.append(os.path.join(export_path, filename))
    # else:
    #     cmd.append(corona_path)
    #     cmd.append(filename)
    # if sys.platform == 'darwin':
    #     cmd.append('--args')

    cmd.append(corona_path)
    cmd.append(filename)

    if scene.corona.vfb_type == '0':
        cmd.append('-silent')

    render_image = imagename + scene.corona.image_format
    cmd.append('-oA' if scene.corona.save_alpha and scene.corona.image_format != '.jpg' else '-o')
    cmd.append(render_image)
    if scene.corona.image_format_jpg and scene.corona.image_format != '.jpg':
        cmd.append('-o')
        cmd.append(imagename + '.jpg')
    if scene.corona.image_format_png and scene.corona.image_format != '.png':
        cmd.append('-oA' if scene.corona.save_alpha else '-o')
        cmd.append(imagename + '.png')
    if scene.corona.image_format_exr and scene.corona.image_format != '.exr':
        cmd.append('-oA' if scene.corona.save_alpha else '-o')
        cmd.append(imagename + '.exr')
    if scene.corona.image_format_tiff and scene.corona.image_format != '.tiff':
        cmd.append('-oA' if scene.corona.save_alpha else '-o')
        cmd.append(imagename + '.tiff')
    if scene.corona.image_format_tga and scene.corona.image_format != '.tga':
        cmd.append('-oA' if scene.corona.save_alpha else '-o')
        cmd.append(imagename + '.tga')

    debug("Command", len(cmd), (',').join(cmd))
    CrnUpdate( "corona %s file %s image %s" % (corona_path, filename, render_image))
    CrnUpdate( "Rendering scene file {0}...".format( filename.split( '\\')[-1]))
    CrnUpdate( "Launching Corona Renderer...")
    CREATE_NEW_PROCESS_GROUP = 0x00000200
    args = {}
    if platform.system() == 'Windows':
        # from msdn [1]
        CREATE_NEW_PROCESS_GROUP = 0x00000200  # note: could get it from subprocess
        DETACHED_PROCESS = 0x00000008          # 0x8 | 0x200 == 0x208
        args.update(creationflags=CREATE_NEW_PROCESS_GROUP)
    elif sys.version_info < (3, 2):  # assume posix
        args.update(preexec_fn=os.setsid)
    # else:  # Python 3.2+ and Unix
    #     args.update(start_new_session=True)
    process = subprocess.Popen( cmd, cwd=export_path, shell=False, bufsize=1, universal_newlines=True, **args)

    # The rendered image name and path
    # render_image = imagename + scene.corona.image_format
    # Wait for the file to be created
    # while not os.path.exists( render_image):
    #     if engine.test_break():
    #         kill(process)
    #         break

    #     if process.poll() != None:
    #         engine.update_stats( "", "Corona: Error")
    #         break

    #     time.sleep( DELAY)


    print( "Image exists...", os.path.exists( render_image) )
    if os.path.exists( render_image):
        CrnUpdate( "Output image exists...")
    engine.update_stats( "", "Corona: Rendering")

    prev_size = -1

    def update_image(retries=1):
        # Camera.
        camera = scene.camera
        crn_cam = camera.data.corona
        startx = 0
        starty = 0
        endx = width
        endy = height
        if scene.render.use_border:
            resX = scene.render.resolution_x * (scene.render.resolution_percentage / 100)
            resY = scene.render.resolution_y * (scene.render.resolution_percentage / 100)
            startx = int(scene.render.border_min_x * resX)
            starty = int(scene.render.border_min_y * resY)

        result = engine.begin_result( 0, 0, width, height)
        lay = result.layers[0]
        # possible the image wont load early on.
        try:
            lay.load_from_file( render_image, startx, starty)
        except Exception as e:
            traceback.print_exc()
            pass
        else:
            engine.end_result( result)
            return
        if retries > 0:
            print('Waiting to retry update image', retries)
            time.sleep( DELAY)
            update_image(retries - 1)

    # Update while rendering
    while True:
        # debug('Process', process.poll())
        if process.poll() != None:
            # debug( "Updating Image")
            # update_image()
            break

        #user exit
        if engine.test_break():
            kill(process)
            time.sleep( DELAY)
            break

        # # check if the file updated
        # new_size = os.path.getmtime( render_image)

        # # debug( "Image Size:", new_size)
        # if new_size != prev_size:
        #     update_image()
        #     prev_size = new_size

        # if engine.test_break():
        #     break
        # time.sleep( DELAY)

    debug( "Updating Image")
    update_image()


# __STARTUP__ = True

#----------------------------------------------
# Render engine/settings
#----------------------------------------------

class RenderCorona( bpy.types.RenderEngine):
    """Corona render engine class"""
    bl_idname = "CORONA"
    bl_label = "Corona"
    bl_use_preview = True
    bl_use_shading_nodes = True
    bl_use_shading_nodes_custom = False

    render_lock = threading.Lock()
    preview_lock = threading.Lock()
    animation = False

    def __init__( self):
        render_init( self)

    # final rendering
    def update( self, data, depsgraph):
        update_start( self, data, depsgraph)

    def render( self, depsgraph):


        # global __STARTUP__

        # if __STARTUP__:
        #     bitbucketVersion = get_bitbucket_version()
        #     version = get_version()

        #     if bitbucketVersion != version:
        #         self.report({'INFO'}, 'New Corona export update available')
        #     __STARTUP__ = False

        # debug("render")
        if self is None or depsgraph is None:
            CrnError( 'Scene is missing! Please select a scene to render')
            return
        if bpy.context.preferences.addons[__package__].preferences.corona_path == '':
            CrnError( 'The binary path is unspecified! Check Corona addon user preferences.')
            return

        if self.is_preview: # preview and normal shouldn't clash
            # debug("Render before lock")
            with self.preview_lock:
                render_preview( self, depsgraph)
            # debug("Render after lock")
        else:
            with self.render_lock:  # just render one thing at a time
                scene = depsgraph.scene
                old_vfb = scene.corona.vfb_type
                if self.is_animation:
                    scene.corona.vfb_type = '0'
                render_start( self, depsgraph)
                if self.is_animation:
                    scene.corona.vfb_type = old_vfb

def register():
    bpy.utils.register_class(RenderCorona)



def unregister():
    bpy.utils.unregister_class(RenderCorona)


