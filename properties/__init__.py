import bpy
from . import camera, material, node_sockets, nodes, object, particle, passes, scene, world

submodules = (
    camera,
    #curve_node,
    material,
    node_sockets,
    nodes,
    object,
    particle,
    passes,
    scene,
    world
    )

def register():
    for sub in submodules:
        sub.register()
    # camera.register()
    # material.register()
    # nodes.register()
    # node_sockets.register()
    # object.register()
    # particle.register()
    # passes.register()
    # scene.register()
    # world.register()
    # # curve_node.register()

def unregister():
    for sub in reversed(submodules):
        sub.unregister()
    # world.unregister()
    # camera.unregister()
    # material.unregister()
    # nodes.unregister()
    # node_sockets.unregister()
    # object.unregister()
    # particle.unregister()
    # passes.unregister()
    # scene.unregister()
    # # curve_node.unregister()


