import bpy
from bpy.props  import StringProperty, BoolProperty, FloatVectorProperty
from bpy.props  import IntProperty, FloatProperty, EnumProperty, PointerProperty
from ..util     import mat_enumerator, random_seed, thread_count

def update_display_mode( self, context):
    scene = context.scene
    if scene.corona.local_view_only:
        # Save current display mode
        scene.corona.display_mode_temp = scene.render.display_mode
        scene.render.display_mode = 'NONE'
    else:
        scene.render.display_mode = scene.corona.display_mode_temp

#------------------------------------
# Render properties
#------------------------------------
class CoronaRenderSettings( bpy.types.PropertyGroup):

    save_alpha: BoolProperty( name = "Transparent Background",
                                        description = "Render background as transparent",
                                        default = True)
    # Motion blur.
    use_ob_mblur: BoolProperty( name = "Enable Transformation",
                                        description = "Enable rendering of rigid (transformation) motion blur",
                                        default = False)

    use_def_mblur: BoolProperty( name = "Enable Geometry",
                                        description = "Enable rendering of non-rigid (deformation) motion blur",
                                        default = False)

    use_cam_mblur: BoolProperty( name = "Enable Camera",
                                        description = "Enable rendering of camera motion blur",
                                        default = False)

    ob_mblur_segments: IntProperty( name = "Transform Segments",
                                        description = "Quality of non-linear rigid (transformation) motion blur",
                                        default = 4,
                                        min = 1,
                                        max = 16)

    def_mblur_segments: IntProperty( name = "Geometry Segments",
                                        description = "Quality of non-linear non-rigid (deformation) motion blur. Increasing this value increases quality at the expense of some render speed and memory usage",
                                        default = 1,
                                        min = 1,
                                        max = 8)

    cam_mblur_segments: IntProperty( name = "Camera Segments",
                                        description = "Quality of non-linear rigid (transformation) motion blur",
                                        default = 4,
                                        min = 1,
                                        max = 16)

    shutter_speed: FloatProperty( name = "Shutter Speed 1/",
                                        description = "Camera shutter speed in 1/x seconds. For photographic results, use standard values such as 1/30, 1/60, etc.",
                                        default = 50,
                                        min = 0.001,
                                        max = 999999)

    frame_offset: FloatProperty( name = "Frame Offset",
                                        description = "Exposure start time offset.  A value of 1 starts exposure at the current frame",
                                        default = 0,
                                        min = -1,
                                        max = 1)

    limit_prog_time: BoolProperty( name = "Use Progressive Time Limit",
                                        description = "Set a time limit on progressive rendering",
                                        default = False)
    image_format: EnumProperty( name = "File Format",
                                        description = "File format to save the rendered images as",
                                        items = [
                                        ('.exr', 'EXR', 'Output images in EXR format'),
                                        ('.png', 'PNG', 'Output images in PNG format'),
                                        ('.jpg', 'JPEG', 'Output images in JPEG format'),
                                        ('.tiff', 'TIFF', 'Output images in TIFF format'),
                                        ('.tga', 'Targa', 'Output images in Targa format')],
                                        default = '.png')

    image_format_exr: BoolProperty( name = "EXR",
                                        description = "",
                                        default = False)

    image_format_png: BoolProperty( name = "PNG",
                                        description = "",
                                        default = False)

    image_format_jpg: BoolProperty( name = "JPEG",
                                        description = "",
                                        default = False)

    image_format_tiff: BoolProperty( name = "TIFF",
                                        description = "",
                                        default = False)

    image_format_tga: BoolProperty( name = "Targa",
                                        description = "",
                                        default = False)

    export_hair: BoolProperty( name = "Export Hair",
                                        description = "Export hair particle systems as renderable geometry",
                                        default = False)

    stream_hair: BoolProperty( name = "Stream Hair",
                                        description = "Use newer method to stream hair, should be faster exporting and use less memory, requires binary objects and hair exporting",
                                        default = True)

    material_override: BoolProperty( name = "Material Override",
                                        description = "Override all materials with selected shader",
                                        default = False)

    clay_render: BoolProperty( name = "Clay Render",
                                        description = "Render all materials as default diagnostic shader",
                                        default = False)

    override_material: EnumProperty( name = "Material",
                                        description = "Use this material as override shader",
                                        items = mat_enumerator)

    binary_obj: BoolProperty( name = "Binary Mesh Export",
                                        description = "Export objects as binary '.cgeo' format",
                                        default = False)

    obj_export_mode: EnumProperty( name = "Mode",
                                        description = "Geometry export mode",
                                        items = [
                                        ('ALL', 'All', 'Export all renderable objects, overwriting files if any exist.'),
                                        ('PARTIAL', 'Partial', 'Export only renderable objects that have not been previously written to disk. Existing files will not be overwritten.'),
                                        ('SELECTED', 'Only Selected', 'Export only the selected objects, overwriting files if any exist.'),
                                        ('MODIFIED', 'Only Modified', '*Experimental* export only the modified objects')],
                                        default = 'ALL')

    obj_export_bool: BoolProperty( name = "Overwrite Geometry",
                                        description = "Export geometry when rendering. Disable if no changes have been made to geometry and objects are not animated",
                                        default = True)

    local_view_only: BoolProperty( name = "Render Local Mode",
                                        description = "Render only objects in local view if using local view mode. Display mode will be set to 'Keep UI'.  There must be a 3D View window open to render the local view",
                                        default = False,
                                        update = update_display_mode)

    corona_conf: StringProperty( name = "Load Corona Config",
                                    description = "Load a second corona config, values in this file will overwrite values defined in Blender",
                                    default = "",
                                    subtype = "FILE_PATH")

    display_mode_temp: StringProperty()

    save_secondary_gi: BoolProperty( name= "Save Secondary GI",
                                        description = "Save secondary GI",
                                        default = False)

    load_secondary_gi: BoolProperty( name = "Load Secondary GI",
                                        description = "Load secondary GI", default = False)

    gi_primaryfile: StringProperty( name = "Primary GI File",
                                        description = "Location of primary GI file", default = "primaryGI.dat", subtype = "FILE_PATH")

    gi_secondaryfile: StringProperty( name = "Secondary GI File",
                                        description = "Location of secondary GI file", default = "secondaryGI.dat", subtype = "FILE_PATH")

    low_threadpriority: EnumProperty( items = [
                                        ('Low', 'Low', 'Won\'t interfere with other processes on your system'),
                                        ('BelowNormal', 'Below Normal', 'Limited interference with other low priority threads'),
                                        ('Normal', 'Normal', 'Shares computation with other processes'),
                                        ('AboveNormal', 'Above Normal', 'Prefers render threads to other processes'),
                                        ('High', 'High', 'May impact on your system performance')
                                        ],
                                        name = "Thread Priority",
                                        description = "Thread priority for the renderer", default = 'Low')

    vfb_show_bucket: BoolProperty( name = "Show Bucket Order",
                                        description = "Show bucket order in virtual frame buffer", default = False)

    do_shading: BoolProperty( name = "Use Shading",
                                        description = "Use material shading settings - otherwise, render without shading (for fast visualization)", default = True)

    do_aa: BoolProperty( name = "Use Anti-aliasing",
                                        description = "Use anti-aliasing", default = True)

    renderer_type: EnumProperty( items = [ #SimplePT, LightTracer, Progressive, VCM, VPL, PPM
                                        ('SimplePT', 'SimplePT', 'Simple Path Tracer'),
                                        ('LightTracer', 'LightTracer', 'Light Tracer'),
                                        ('Progressive', 'Progressive', 'Progressive renderer'),
                                        ('VCM', 'VCM', 'Vertex Connection Merging'),
                                        ('VPL', 'VPL', 'Virtual point lights'),
                                        ('PPM', 'PPM', 'Progressive photon mapping')],
                                        name = "Renderer",
                                        description = "Renderer type",
                                        default = 'Progressive')

    #Leave Irradiance caching out right now - no parameters to write
    gi_primarysolver: EnumProperty( items = [
                                        ('None', 'None', 'No global illumination'),
                                        ('PathTracing', 'Path Tracing', 'Brute force per-pixel global illumination'),
                                        ('PhotonMap', 'Photon Map', ''),
                                        ('VPL', 'Virtual Point Lights', 'Not recommended'),
                                        ('UHDCache', 'UHDCache', '')],
                                        name = "GI Primary Solver",
                                        description = "Primary global illumination solver",
                                        default = 'PathTracing')

    gi_secondarysolver: EnumProperty( items = [
                                        ('None', 'None', 'No global illumination'),
                                        ('PathTracing', 'Path Tracing', 'Brute force per-pixel global illumination'),
                                        ('PhotonMap', 'Photon Map', ''),
                                        ('VPL', 'Virtual Point Lights', 'Not recommended'),
                                        ('UHDCache', 'UHDCache', '')],
                                        name = "GI Secondary Solver",
                                        description = "Secondary global illumination solver",
                                        default = 'UHDCache')

    image_filter: EnumProperty( items = [
                                        ('None', 'None', 'No image filtering'),
                                        ('Box', 'Box', 'Box filtering'),
                                        ('Tent', 'Tent', 'Tent filtering'),
                                        ('Parabolic', 'Parabolic', 'Parabolic filtering'),
                                        ('Symmetric Tent', 'Symmetric Tent', 'Tent filtering'),
                                        ('Hann', 'Hann', 'Tent filtering'),
                                        ('Blackman-Harris', 'Blackman-Harris', 'Tent filtering'),
                                        ],
                                        name = "Image Filter",
                                        description = "Image filtering to reduce noise",
                                        default = 'None')

    filter_width: FloatProperty( name = "Filter Width",
                                        description = "Image filter width (in pixels)", default = 2, min = 0.0, max = 3.0)
    filter_blur: FloatProperty( name = "Filter Blurring",
                                        description = "Image filter blurring", default = 0.5, min = 0.0, max = 1.0)

    embree_tris: EnumProperty( items = [
                                        ('0', 'Embree Fast', 'Embree Fast'),
                                        ('1', 'Embree AVX', 'Embree AVX')],
                                        name = "Embree Triangles",
                                        description = "Embree triangles",
                                        default = '0')


    denoise_filter: EnumProperty( items = [
                                        ('None', 'None', 'No denoise'),
                                        ('Fireflies', 'Fireflies', 'Denoise fireflies only'),
                                        ('Full', 'Full', 'Denoise everything'),
                                        ('Gather', 'Gather', 'Gather mode')
                                        ],
                                        name = "Filter Type",
                                        description = "Denoise filter type",
                                        default = 'Fireflies')

    denoise_sensitivity: FloatProperty( name = "Sensitivity",
                                        description = "", default = 1.0, min = 0.1, max = 10.0)

    denoise_blur: FloatProperty( name = "Denoise Blur",
                                        description = "", default = 1.0, min = 0.00001, max = 100.0)

    denoise_amt: FloatProperty( name = "Amount",
                                        description = "Amount of Corona's denoise to apply", default = 1.0, min = 0.0, max = 1.0)

    displace_type: EnumProperty( items = [
                                        ('Projected', 'Projected', 'Projected (screen or pixel based) subdivision calculation, the pixel grid is used to determine subdivision size'),
                                        ('World', 'World', 'World subdivision calculation')
                                        ],
                                        name = "Subdivide Type",
                                        description = "Type of subdivision to perform",
                                        default = 'Projected')

    displace_max_screen: FloatProperty( name = "Max Screen Size",
                                        description = "Maximum size in screen pixels without subdividing, increase to speed up and use less memory, decrease for more details", default = 2.0, min = 0.01, max = 100.0)

    displace_max_world: FloatProperty( name = "Max World Size",
                                        description = "Maximum size in world units without subdividing, increase to speed up and use less memory, decrease for more details", default = 1.0, min = 0.00001)

    displace_filter_map: BoolProperty( name = "Filter Map",
                                        description = "",
                                        default = False)

    displace_smooth_normals: BoolProperty( name = "Smooth Normals",
                                        description = "Smooth generated normals",
                                        default = True)

    displace_frustrum_mult: FloatProperty( name = "Maximum Frustrum Multiplier",
                                        description = "Maximum size in screen pixels to include in subdivision", default = 100.0, min = 1.0, max = 100000000)

    prog_max_passes: IntProperty( name = "Max Passes",
                                        description = "Progressive rendering maximum passes",
                                        default = 0,
                                        min = 0,
                                        max = 9999)

    prog_timelimit_hour: IntProperty( name = "h",
                                        description = "Progressive rendering time limit per frame (in hours). Default of 0 in all fields disables time limit.",
                                        default = 0,
                                        min = 0 )

    prog_timelimit_min: IntProperty( name = "m",
                                        description = "Progressive rendering time limit per frame (in minutes). Default of 0 in all fields disables time limit.",
                                        default = 0,
                                        min = 0 )

    prog_timelimit_sec: IntProperty( name = "s",
                                        description = "Progressive rendering time limit per frame (in seconds). Default of 0 in all fields disables time limit.",
                                        default = 0,
                                        min = 0 )

    arealight_samples: FloatProperty( name = "Area Light Sample Multiplier",
                                        description = "Multiplies number of samples devoted to sampling area lights relative to other sampling",
                                        default =2.0,
                                        min = 0.1,
                                        max = 100)

    arealight_method: EnumProperty( items = [ # Simple, Reproject, ReprojectBidir
                                        ('Simple', 'Area Light Simple', ''),
                                        ('Reproject', 'Area Light Re-project', ''),
                                        ('ReprojectBidir', 'Area Light Re-project Bi Directional', '')],
                                        name = "Area Light Method",
                                        description = "",
                                        default = 'ReprojectBidir')

    path_samples: IntProperty( name = "Path Tracing Samples",
                                        description = "Number of rays traced per sample",
                                        default = 16,
                                        min = 1,
                                        max = 1024)

    max_depth: IntProperty( name = "Max Ray Depth",
                                        description = "Maximum ray depth",
                                        default = 25,
                                        min = 0,
                                        max = 100)

    min_depth: IntProperty( name = "Min Ray Depth",
                                        description = "Minimum ray depth",
                                        default = 0,
                                        min = 0,
                                        max = 100)

    vfb_update: IntProperty( name = "Frame Buffer Update Interval",
                                        description = "Virtual frame buffer update interval in milliseconds",
                                        default = 1000,
                                        min = 0,
                                        max = 3600000)

    ray_exit_color: FloatVectorProperty( name = "Ray Exit Color",
                                        description = "Color a ray returns after reaching maximum trace depth",
                                        default = (0.0, 0.0, 0.0),
                                        subtype = "COLOR")

    fb_res_mult: IntProperty( name = "Internal Resolution Multiplier",
                                        description = "Multiplier of internally computed output resolution",
                                        default = 2,
                                        min = 1,
                                        max = 4)

    max_normal_dev: FloatProperty( name = "Max Normal Difference",
                                        description = "Maximum acceptable difference between geometric and shading normal",
                                        default = 0.55,
                                        min = 0.0,
                                        max = 2.0)

    prog_recalculate: IntProperty( name = "Noise Interval",
                                        description = "Recalculate every nth pass",
                                        default = 1,
                                        min = 1,
                                        max = 999)

    prog_adaptivity: FloatProperty( name = "Target Noise Amount",
                                        description = "Stop rendering with this percentage of noise",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 99)

    max_sample_intensity: FloatProperty( name = "Max Sample Intensity",
                                        description = "Reduce fireflies at the cost of accuracy -- lower values = larger bias. A setting of 0 disables this feature = unbiased path tracing",
                                        default = 20.0,
                                        min = 0.0,
                                        max = 99999)

    subdiv_env_threshold: FloatProperty( name= "Subdiv Threshold",
                                        description = "Maximum difference of color on textured lights that can be merged with likely colors in order to optimize segmentation of environment lighting - DO NOT TOUCH unless you know what you are doing.",
                                        default = 0.005,
                                        min = 0.0,
                                        max = 1.0)

    lights_tex_res: FloatProperty( name = "Texture Resolution",
                                        description = "Resolution of illumination generated by textured lights",
                                        default = 0.3,
                                        min = 0.0001,
                                        max = 99)

    lights_env_res: IntProperty( name = "Environment Resolution",
                                        description = "Environment Resolution",
                                        default = 512,
                                        min = 0,
                                        max = 100000)

    enviro_solver: EnumProperty( items = [
                                        ('0', 'Fast', ''),
                                        ('1', 'Fast Compensate', '')],
                                        name = "Environment Solver",
                                        description = "",
                                        default = '0')

    vfb_aa: IntProperty( name = "Frame Buffer AA",
                                        description = "Virtual framebuffer display antialiasing when displaying image at scale < 1:1",
                                        default = 0,
                                        min = 0)

    random_seed: IntProperty( name = "Random Seed",
                                        description = "Random seed",
                                        default = random_seed(),
                                        min = 1,
                                        max = 2000)

    num_threads: IntProperty( name = "Render Threads",
                                        description = "Number of threads to use for rendering",
                                        default = 8,
                                        min = 1,
                                        max = thread_count)

    auto_threads: BoolProperty( name = "Auto",
                                        description = "Automatic CPU thread detection",
                                        default = True)


    solid_alpha: BoolProperty( name = "Material Solid Alpha",
                                        description = "All materials have solid alpha, regardless of opacity settings",
                                        default = False)

    portal_samples: FloatProperty( name = "Portal Samples Fraction",
                                        description = "",
                                        default = 0.75,
                                        min = 0.001,
                                        max = 1.0,
                                        precision = 3)

    # HD Cache parameters
    hd_precomp_mode: EnumProperty( items = [ #Compute, Load, Append
                                        ('Compute', 'Compute', ''),
                                        ('Load', 'Load', ''),
                                        ('Append', 'Append', '')],
                                        name = "Precompute mode",
                                        description = "",
                                        default = 'Compute')

    hd_precomp_mult: FloatProperty( name = "Precomputation Density",
                                        description = "",
                                        default = 1.00,
                                        min = 0.01,
                                        max = 10 )

    hd_interpolation_count: IntProperty( name = "Interpolation Count",
                                        description = "Maximum number of samples used for interpolation of HD Cache",
                                        default = 3,
                                        min = 1,
                                        max = 512)

    hd_sens_direct: IntProperty( name = "Direct",
                                        description = "Directional sensitivity of HD cache samples interpolation",
                                        default = 2,
                                        min = 1,
                                        max = 20)

    hd_sens_normal: IntProperty( name = "Normal",
                                        description = "Normal sensitivity of HD cache samples interpolation",
                                        default = 3,
                                        min = 1,
                                        max = 20)

    hd_pt_samples: IntProperty( name = "Record Quality",
                                        description = "Quality of HD cache interpolation - higher values eliminate artifacts and animation flickering",
                                        default = 256,
                                        min = 1,
                                        max = 2048)

    hd_glossy_thresh: FloatProperty( name = "Glossy Threshold",
                                        description = "Glossiness threshold to decide between ray tracing glossy reflections and using HD cache to interpolate them",
                                        default = 0.9,
                                        min = 0.1,
                                        max = 1.0)

    uhd_strictness: FloatProperty( name = "UHD Strictness",
                                        description = "",
                                        default = 0.075,
                                        min = 0,
                                        max = 99.0)#   (def: 0.075, min: 0, max: 99)

    uhd_msi: FloatProperty( name = "MSI",
                                        description = "",
                                        default = 3,
                                        min = 0,
                                        max = 9999)#   (def: 3, min: 0, max: 9999)

    uhd_correlatePrecomp: BoolProperty( name = "Correlate Precomp",
                                        description = "",
                                        default = True)#   (def: true)

    uhd_precision: FloatProperty( name = "Precision",
                                        description = "",
                                        default = 1.0,
                                        min = 0.01,
                                        max = 20.0)#   (def: 1, min: 0.01, max: 20)

    hd_max_records: IntProperty( name = "Max Records",
                                        description = "Maximum count of HD cache records in thousands. Higher values can add slightly to GI computation time but limited values cause missing records to get replaced by path tracing = longer convergence time",
                                        default = 100000,
                                        min = 1,
                                        max = 100000)

    hd_write_passes: IntProperty( name = "Writeable for Passes",
                                        description = "Number of passes for which HD cache will be used to write new samples. 0 = precomputation phase only, 1 = precomputation and first pass, etc.",
                                        default = 0)

    # Photon mapping parameters
    photons_emitted: IntProperty( name = "Emitted Photons",
                                        description = "Total number of photons emitted into the scene",
                                        default = 100000,
                                        min = 50)

    photons_store_direct: BoolProperty( name = "Store Direct Lighting",
                                        description = "Enable storage of direct lighting within photon map",
                                        default = True)

    photons_depth: IntProperty( name = "Depth",
                                        description = "Maximum photon depth",
                                        default = 1,
                                        min = 1,
                                        max = 256)

    photons_lookup: IntProperty( name = "Lookup Count",
                                        description = "How many photons will be interpolated when final information from photon map is extracted",
                                        default = 50,
                                        min = 1,
                                        max = 512)

    # Not exposed to the UI yet
    photons_filter: EnumProperty ( items = [ #
                                    ('Constant', 'Constant', ''),
                                    ('Linear', 'Linear', ''),
                                    ('Epanechnikov', 'Epanechnikov', ''),
                                    ('Biweight', 'Biweight', ''),
                                    ('Gaussian', 'Gaussian', '')
                                    ],
                                    name = "Filter",
                                    description = "",
                                    default = 'Linear')

    # What do these VPL values mean?
    vpl_emitted_count: IntProperty( name = "Emitted Count",
                                        description = "",
                                        default = 100000,
                                        min = 1,
                                        max = 9999999)

    vpl_used_count: IntProperty( name = "Used Count",
                                        description = "",
                                        default = 150,
                                        min = 1,
                                        max = 9999)

    vpl_progressive_batch: IntProperty( name = "Progressive Batch",
                                        description = "",
                                        default = 150,
                                        min = 1,
                                        max = 9999)

    vpl_clamping: FloatProperty( name = "Clamping",
                                        description = "Clamping of white pixel values",
                                        default = 50.0,
                                        min = 0.0,
                                        max = 999)

    colmap_compression: FloatProperty( name = "Highlight Compression",
                                        description = "Controls compression of highlights",
                                        default = 1.0,
                                        min = 0.01,
                                        max = 999.0)

    colmap_exposure: FloatProperty( name = "Exposure",
                                        description = "Controls brightness of the image - increasing = brighter image, decreasing = darker image",
                                        default = 0.0,
                                        min = -20.0,
                                        max = 20.0)

    colmap_iso: FloatProperty( name = "ISO",
                                        description = "Film speed",
                                        default = 100,
                                        min = 0.001,
                                        max = 999999)

    colmap_gamma: FloatProperty( name = "Gamma",
                                        description = "Gamma correction of output image",
                                        default = 2.2,
                                        min = 0.1,
                                        max = 10.0)

    colmap_color_temp: FloatProperty( name = "White Balance [K]",
                                        description = "Controls white balance balance of the image output (values in Kelvins) - values toward the left are warmer, values toward right are cooler",
                                        default = 6500.0,
                                        min = 1800.0,
                                        max = 99999.0)

    colmap_tint: FloatVectorProperty( name = "Color Tint",
                                        description = "RGB color balance of output image",
                                        default = (1.0, 1.0, 1.0),
                                        subtype = "COLOR",
                                        min = 0.0,
                                        max = 1)

    colmap_contrast: FloatProperty( name = "Contrast",
                                        description = "Image contrast",
                                        default = 1.0,
                                        min = 1.0,
                                        max = 99)

    colmap_use_photographic: BoolProperty( name = "Use Photographic Exposure",
                                        description = "When enabled, photographic exposure parameters in camera settings will be used to control exposure.  When disabled, exposure value below will be used to control exposure",
                                        default = False)

    colmap_saturation: FloatProperty( name = "Saturation",
                                        description = "image saturation",
                                        default = 0.0,
                                        min = -1.0,
                                        max = 1.0) # (def: 0, min: -1, max: 1)
    colmap_filmic_shadow: FloatProperty( name = "Filmic Rich Shadows",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0) # (def: 0, min: 0, max: 1)
    colmap_filmic_highlight: FloatProperty( name = "Filmic Highlight Compression",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0) # (def: 0, min: 0, max: 1)

    colmap_lut_enable: BoolProperty( name = "Use LUT",
                                        description = "Use a color mapping look up table to adjust output colors",
                                        default = False) # (def: false)
    colmap_path: StringProperty( name = "LUT File",
                                        description = "Location of LUT file",
                                        default = "",
                                        subtype = "FILE_PATH") # (def: )
    colmap_opacity: FloatProperty( name = "Opacity",
                                        description = "Blend with original colors",
                                        default = 1.0,
                                        min = 0.0,
                                        max = 1.0)    # (def: 1, min: 0, max: 1)
    colmap_use_log: BoolProperty( name = "Convert to LOG",
                                        description = "",
                                        default = False) # (def: false)

    # Vignette
    vignette_intensity: FloatProperty( name = "Intensity",
                                        description = "Intensity",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0) # (def: 0, min: 0, max: 1)
    vignette_falloff: FloatProperty( name = "Falloff",
                                        description = "Falloff",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0)   # (def: 0, min: 0, max: 1)
    vignette_offset_x: FloatProperty( name = "Offset X",
                                        description = "Offset X direction in screen units e.g. 1.0 equals screen width",
                                        default = 0.0,
                                        min = -1.0,
                                        max = 1.0)  # (def: 0, min: -1, max: 1)
    vignette_offset_y: FloatProperty( name = "Offset Y",
                                        description = "Offset Y direction in screen units e.g. 1.0 equals screen height",
                                        default = 0.0,
                                        min = -1.0,
                                        max = 1.0)  # (def: 0, min: -1, max: 1)

    # Bloom Glare
    bloom_enabled: BoolProperty( name = "Enable",
                                        description = "",
                                        default = False)

    bloom_intensity: FloatProperty( name = "Bloom Intensity",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 100000.0)       # (def: 0, min: 0, max: 100000)
    bloom_glare_intensity: FloatProperty( name = "Glare Intensity",
                                        description = "",
                                        default = 1.0,
                                        min = 0.0,
                                        max = 100000.0) # (def: 0, min: 0, max: 100000)
    bloom_streak_count: IntProperty( name = "Streak Count",
                                        description = "Number of streaks",
                                        default = 3,
                                        min = 1,
                                        max = 8)    # (def: 3, min: 1, max: 8)
    bloom_rotation: FloatProperty( name = "Streak Rotation",
                                        description = "",
                                        default = 0.0)        # (def: 0, min: -1e+10, max: 1e+10)
    bloom_streak_blur: FloatProperty( name = "Streak Blur",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0)     # (def: 0, min: 0, max: 1)
    bloom_color_intensity: FloatProperty( name = "Color Intensity",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0) # (def: 0, min: 0, max: 1)
    bloom_color_shift: FloatProperty( name = "Color Shift",
                                        description = "",
                                        default = 0.0,
                                        min = 0.0,
                                        max = 1.0)     # (def: 0, min: 0, max: 1)
    bloom_threshold: FloatProperty( name = "Threshold",
                                        description = "",
                                        default = 1.0,
                                        min = 0.0,
                                        max = 100000.0)       # (def: 1, min: 0, max: 100000)

    # PPM
    ppm_samples: IntProperty( name = "Samples Per Iteration",
                                        description = "Number of samples per pass",
                                        default = 1,
                                        min = 1,
                                        max = 50)

    ppm_photons: IntProperty( name = "Photons Per Iteration",
                                        description = "Number of photons emitted per pass",
                                        default = 500000,
                                        min = 1000,
                                        max = 99000000)

    ppm_alpha: FloatProperty( name = "Photon Alpha",
                                        description = "",
                                        default = 0.666,
                                        min = 0.01,
                                        max = 1.0)

    ppm_initial_rad: FloatProperty( name = "Initial Radius",
                                        description = "Photon initial radius",
                                        default = 2.0,
                                        min = 0.0001,
                                        max = 200)

    # Bidirectional MIS
    bidir_mis: BoolProperty( name = "Multiple Importance Sampling",
                                        description = "Use multiple importance sampling for bidirectional path tracing",
                                        default = True)

    vcm_mode: EnumProperty( items = [ # PathTracing, LightTracing, PPM, BidirectionalPT, BDPT, VCM
                                        ('PathTracing', 'PathTracing', ''),
                                        ('LightTracing', 'LightTracing', ''),
                                        ('PPM', 'PPM', ''),
                                        ('BidirectionalPT', 'BidirectionalPT', 'Use bidirectional path tracing'),
                                        ('BDPT', 'BDPT', ''),
                                        ('VCM', 'VCM', 'Use vertex connection and merging')],
                                        name = "Mode",
                                        description = "",
                                        default = 'BDPT')

    renderstamp_use: BoolProperty( name = "Use Render Stamp",
                                        description =  "Use render stamp on final image -- Passes | Rays | Time",
                                        default = False)

    vfb_type: EnumProperty( items = [('0', 'None', 'Use no-GUI unattended rendering'), ('2', 'Corona wx Framebuffer', 'Use Corona Standalone framebuffer GUI')],
                                        name = "Framebuffer",
                                        description = "Use virtual frame buffer or use no-GUI unattended rendering",
                                        default = '2')

    export_path: StringProperty( name = "Export path",
                                        description = "Where to export project files, will replace $filename with the current .blend filename ",
                                        subtype = 'DIR_PATH')


#   Float sharpening.amount   (def: 1, min: 0, max: 10)
#   Float sharpening.radius   (def: 0.5, min: 0, max: 50)
#   Float blurring.radius   (def: 1.33, min: 0, max: 50)
#    Bool sharpening.blurring.enable   (def: false)
    sharpen_blur_enable: BoolProperty( name = "Sharpening/Blur Enable",
                                        default = False)
    sharpen_amount: FloatProperty( name = "Sharpening Amount",
                                        default = 1,
                                        min = 0,
                                        max = 10)
    sharpen_radius: FloatProperty( name = "Sharpening Radius",
                                        default = 0.5,
                                        min = 0,
                                        max = 50)
    blur_radius: FloatProperty( name = "Blurring Radius",
                                        default = 1.33,
                                        min = 0,
                                        max = 50)

def register():
    bpy.utils.register_class( CoronaRenderSettings)
    bpy.types.Scene.corona = PointerProperty( type  =  CoronaRenderSettings)
def unregister():
    bpy.utils.unregister_class( CoronaRenderSettings)
