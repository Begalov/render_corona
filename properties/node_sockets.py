import bpy
from bpy.types import NodeSocketShader, NodeSocket, CurveMapping, PropertyGroup
from bpy.props import IntProperty, FloatProperty, FloatVectorProperty, EnumProperty, PointerProperty
from ..util    import strip_spaces, join_params, join_names_underscore, debug, find_group_node, print_exception
from .material import CoronaMatProps
from .object   import CoronaObjects

import xml.etree.cElementTree as ElementTree
from xml.etree.cElementTree import Element, Comment, SubElement

class CoronaUVWSocket( NodeSocketShader):
    bl_idname = "CoronaUVW"
    bl_label = "UVW"

    default_value: FloatProperty( name = "UVW",
                                min = 0,
                                max = 1,
                                default = 1)

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color(self, context, node):
        return (0.3, 0.5, 0.8, 1)

# Socket base class.
class CoronaSocket( NodeSocket):
    bl_idname = "Corona"
    bl_label = ""

    # Set to default None.
    default_value: FloatProperty( name = "Corona",
                                min = 0,
                                max = 1,
                                default = 1)

    def get_socket_value( self, node, root):
        ''' Method to return socket's value as a string'''
        return "%.6f" % ( self.default_value)

    def get_socket_params( self, node, root, inline = False):
        linked_node = self.links[0].from_node
        linked_socket_name = self.links[0].from_socket.name
        if linked_node.bl_idname == 'NodeReroute':
            linked_node = linked_node.inputs[0].links[0].from_node

        # node groups
        active_socket = self
        group = None
        while linked_node.type == 'GROUP' and group != linked_node:
            group = linked_node
            identifier = active_socket.links[0].from_socket.identifier
            group_output = group.node_tree.nodes['Group Output']
            for i in range(len(group_output.inputs)):
                if group_output.inputs[i].identifier == identifier:
                    if len(group_output.inputs[i].links) > 0:

                        if group_output.inputs[i].links[0].from_node.name == 'Group Input':
                            identifier = group_output.inputs[i].links[0].from_socket.identifier

                        linked_node = group_output.inputs[i].links[0].from_node
                        active_socket = group_output.inputs[i].links[0].from_socket
                        break

            if linked_node.name == 'Group Input':
                group_input = linked_node
                for i in range(len(group_input.outputs) - 1):
                    if group.inputs[i].identifier == identifier:
                        if len(group.inputs[i].links) > 0:
                            linked_node = group.inputs[i].links[0].from_node
                            active_socket = group.inputs[i].links[0].from_socket
                            break

        node_params = None
        try:
            node_params = linked_node.get_node_params(root, inline, source = linked_socket_name)
        except:
            pass

        return node_params

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_linked_node(self, socket):
        if len(socket.links) == 0:
            return None

        linked_node = socket.links[0].from_node
        while linked_node.bl_idname == 'NodeReroute' and len(linked_node.inputs[0].links) > 0:
            linked_node = linked_node.inputs[0].links[0].from_node
        if linked_node.bl_idname == 'NodeReroute':
            return None

        return linked_node

    def get_alpha_socket_params( self, node, root, inline = False):
        linked_node = self.get_linked_node(self)
        print('Alpha', 'Alpha' in linked_node.outputs)
        if not linked_node or 'Alpha' not in linked_node.outputs:
            return None
        return linked_node.get_node_params(root, inline, source = 'Alpha')

# Socket base class for :solid color properties.
class CoronaSolidSocket( NodeSocket):

    default_value: FloatVectorProperty( name = "Float" )

    def get_socket_value( self, name, root):
        ''' Method to return socket's value as a string'''
        # debug(self.default_value, str(type(self.default_value)), len(self.default_value))
        try:
            length = len(self.default_value)
            if length == 1:
                return '%.6f' % self.default_value[0]
            elif length == 2:
                return '%.6f %.6f' % (self.default_value[0], self.default_value[1])
            elif length == 3:
                return '%.6f %.6f %.6f' % (self.default_value[0], self.default_value[1], self.default_value[2])
            elif length == 4:
                return '%.6f %.6f %.6f %.6f' % (self.default_value[0], self.default_value[1], self.default_value[2], self.default_value[3])
        except:
            pass

        return'%.6f' % self.default_value

    def get_socket_params( self, node, root, inline = False):
        try:
            active_socket = self
            if self.id_data.name != 'Shader Nodetree':
                if len(active_socket.links) > 0:
                    identifier = active_socket.links[0].from_socket.identifier
                    nodes = bpy.context.scene.objects.active.active_material.node_tree.nodes
                    group_node, active_socket = find_group_node(nodes, identifier)
                else:
                    #to do: Take params from group input
                    return None

            if len(active_socket.links) > 0:
                linked_node = self.get_linked_node(active_socket)
                if not linked_node:
                    return None
                linked_socket_name = active_socket.links[0].from_socket.name
                node_params = linked_node.get_node_params(root, inline, source = linked_socket_name)
                return node_params
            else:
                return None
        except Exception as e:
            print_exception(e)

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_linked_node(self, socket):
        if len(socket.links) == 0:
            return None

        linked_node = socket.links[0].from_node
        while linked_node.bl_idname == 'NodeReroute' and len(linked_node.inputs[0].links) > 0:
            linked_node = linked_node.inputs[0].links[0].from_node
        if linked_node.bl_idname == 'NodeReroute':
            return None

        return linked_node


class CoronaAoDistMapSocket( CoronaSolidSocket):
    bl_idname = "CoronaAoDistMap"
    default_value: FloatVectorProperty( name = "Distance Map",
                                description = "Texture by which to multiply the distance value, allowing it to change spatially",
                                default = (1, 1, 1, 1),
                                size = 4,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')
    def get_socket_value( self, name, root):
        return None

# Common sockets.
class CoronaFloatSocket( CoronaSocket):
    bl_idname = "CoronaFloat"
    bl_label = "Level"
    default_value: FloatProperty( name = "Color",
                                default = 1)
class CoronaFloat0Socket( CoronaSocket):
    bl_idname = "CoronaFloat0"
    bl_label = "Level"
    default_value: FloatProperty( name = "Color",
                                default = 0)

class CoronaFloatUnitsSocket( CoronaSocket):
    bl_idname = "CoronaFloatUnits"
    bl_label = "Level"
    default_value: FloatProperty( name = "Units",
                                default = 1,
                                unit = 'LENGTH')

class CoronaSeedSocket( CoronaSocket):
    bl_idname = "CoronaSeed"
    bl_label = "Seed"
    default_value: IntProperty( name = "Seed",
                                description = "Random seed to reproduce results",
                                default = 0,
                                soft_min = 0,
                                soft_max = 100)

    def get_socket_value( self, node, root):
        ''' Method to return socket's value as a string'''
        return "%d" % ( self.default_value)

class CoronaRadiusSocket( CoronaSocket):
    bl_idname = "CoronaRadius"
    bl_label = "Radius"
    default_value = FloatProperty( name = "Radius",
                                soft_min = 0,
                                soft_max = 10,
                                default = 1)

class CoronaWeightSocket( CoronaSocket):
    bl_idname = "CoronaWeight"
    bl_label = "Weight"
    default_value: FloatProperty( name = "Weight",
                                soft_min = 0,
                                soft_max = 1,
                                default = 1)
class CoronaR0( CoronaSocket):
    bl_idname = "CoronaR0"
    bl_label = "R"
    default_value: FloatProperty( name = "Red",
                                min = 0,
                                max = 1,
                                default = 0)

    def get_socket_value( self, name, root):
        return '%.6f 0 0 0' % self.default_value

class CoronaG0( CoronaSocket):
    bl_idname = "CoronaG0"
    bl_label = "G"
    default_value: FloatProperty( name = "Green",
                                min = 0,
                                max = 1,
                                default = 0)

    def get_socket_value( self, name, root):
        return '0 %.6f 0 0' % self.default_value

class CoronaB0( CoronaSocket):
    bl_idname = "CoronaB0"
    bl_label = "B"
    default_value: FloatProperty( name = "Blue",
                                min = 0,
                                max = 1,
                                default = 0)

    def get_socket_value( self, name, root):
        return '0 0 %.6f 0' % self.default_value

class CoronaA0( CoronaSocket):
    bl_idname = "CoronaA0"
    bl_label = "A"
    default_value: FloatProperty( name = "Alpha",
                                min = 0,
                                max = 1,
                                default = 0)

    def get_socket_value( self, name, root):
        return '0 0 0 %.6f' % self.default_value

class CoronaFloatSocket010( CoronaSocket):
    bl_idname = "CoronaFloat010"
    bl_label = "Level"
    default_value: FloatProperty( name = "Color",
                                min = 0,
                                max = 1,
                                default = 0)

class CoronaFloatSocket011( CoronaSocket):
    bl_idname = "CoronaFloat011"
    bl_label = "Level"
    default_value: FloatProperty( name = "Color",
                                min = 0,
                                max = 1,
                                default = 1)

class CoronaFloatSocket0105( CoronaSocket):
    bl_idname = "CoronaFloat0105"
    bl_label = "Level"
    default_value: FloatProperty( name = "Color",
                                min = 0,
                                max = 1,
                                default = 0.5)

class CoronaColorSocket( CoronaSolidSocket):
    bl_idname = "CoronaColor"
    default_value: FloatVectorProperty( name = "Color",
                                default = (1, 1, 1, 1),
                                size = 4,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')
    def draw_color( self, context, node):
        return (0.8, 0.8, 0, 1)

class CoronaBlackSocket( CoronaSolidSocket):
    bl_idname = "CoronaBlack"
    default_value: FloatVectorProperty( name = "Color",
                                default = (0, 0, 0, 1),
                                size = 4,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaMelaninSocket( CoronaSocket):
    bl_idname = "CoronaMelanin"
    default_value: FloatProperty( name = "Melanin",
                                description = "Controls the absorption inside hair fibers by settings the total amount of melanin pigments in hair. The value equal to 0 leads to white hair, while value of 1 leads to completely black hair. The intermediate values then give physically plausible hair color.",
                                min = 0,
                                max = 1,
                                default = 0.4)

class CoronaPheomelaninSocket( CoronaSocket):
    bl_idname = "CoronaPheomelanin"
    default_value: FloatProperty( name = "Pheomelanin",
                                description = "Controls the relative amount of reddish pheomelanin pigment among the hair pigments and hence the hair redness. Value of 0 leads to 100% of eumelanin (brown pigment) and value of 1 leads to 100% of pheomelanin (reddish pigment).",
                                min = 0,
                                max = 1,
                                default = 0.5)

class CoronaDyeColorSocket( CoronaSolidSocket):
    bl_idname = "CoronaDyeColor"
    default_value: FloatVectorProperty( name = "DyeColor",
                                description = "Controls the absorption inside hair fibers. Settings this value to black will lead to total absorption within the hair and hence black hair, while setting it to white leads to no absorption and white hair. For physically plausible results it is recommended to use melanin and pheomelanin controls instead and use hair color as only additional hair dye.",
                                default = (0, 0, 0),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaColorlessGlossinessSocket( CoronaSolidSocket):
    bl_idname = "CoronaColorlessGlossiness"
    default_value: FloatVectorProperty( name = "Colorless Specular",
                                description = "Multiplier of glossiness parameter for the primary (colorless) specular lobe. Increasing the value will lead to decreasing the width of primary (colorless) specular lobe compared to other lobes (colored specular, transmission).",
                                default = (1, 1, 1),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaColoredSpecularSocket( CoronaSolidSocket):
    bl_idname = "CoronaColoredSpecular"
    default_value: FloatVectorProperty( name = "Colored Specular",
                                description = "Multiplier of the secondary (colored) specular lobe. For physically plausible results it is recommended to keep this value as pure white.",
                                default = (1, 1, 1),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaHairTransmissionSocket( CoronaSolidSocket):
    bl_idname = "CoronaHairTransmission"
    default_value: FloatVectorProperty( name = "Transmission",
                                description = "Multiplier of the transmission lobe. For physically plausible results it is recommended to keep this value as pure white.",
                                default = (1, 1, 1),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaHairGlossinessSocket( CoronaSocket):
    bl_idname = "CoronaHairGlossiness"
    default_value: FloatProperty( name = "Glossiness",
                                description = "Controls the glossiness of hair along the hair length. Value of 0 leads to very rough/diffuse hair, while value of 1 leads to most specular hair.",
                                min = 0,
                                max = 1,
                                default = 0.6)

class CoronaSoftnessSocket( CoronaSocket):
    bl_idname = "CoronaSoftness"
    default_value: FloatProperty( name = "Softness",
                                description = "Controls the overall softness of hair by changing the hair roughness along the hair width. Value of 0 leads to least soft hair (high specularity along the hair width), while value of 1 leads to most soft hair.",
                                min = 0,
                                max = 1,
                                default = 0.3)

class CoronaHighlightShiftSocket( CoronaSocket):
    bl_idname = "CoronaHighlightShift"
    default_value = FloatProperty( name = "Highlight Shift",
                                description = "Shifts the highlights on hair by changing the angle of scales on the hair fiber. Value of 0 will lead to identical position of colorless and colored specular highlights, increasing the value separates the highlights more.",
                                min = 0,
                                max = 1,
                                default = 0.3)

class CoronaGlintStrengthSocket( CoronaSocket):
    bl_idname = "CoronaGlintStrength"
    default_value: FloatProperty( name = "Glint Strength",
                                description = "Controls the strength of random glints (i.e. caustics caused by internal reflection within elliptical hair fibers). Value of 0 results in no random glints, value of 1 leads to most apparent random glints. Increasing the glints strength leads to overall decrease in colored specular, since the energy is now more focused in the random glints.",
                                min = 0,
                                max = 1,
                                default = 0.1)

class CoronaColorlessSpecularSocket( CoronaSocket):
    bl_idname = "CoronaColorlessSpecular"
    default_value: FloatProperty( name = "Colorless Highlight",
                                description = "Multiplier of the secondary (colored) specular lobe. For physically plausible results it is recommended to keep this value as pure white.",
                                min = 0,
                                max = 1,
                                default = 1)

class CoronaHairIorSocket( CoronaSocket):
    bl_idname = "CoronaHairIor"
    default_value: FloatProperty( name = "IOR",
                                description = "Controls the index of refraction on hair surface. Value of 1.55 is recommended for physically plausible results. Inreasing the value will lead to higher specularity of hair.",
                                soft_min = 0,
                                soft_max = 200,
                                default = 1.55)

class CoronaHairDiffuseSocket( CoronaSolidSocket):
    bl_idname = "CoronaHairDiffuse"
    default_value: FloatVectorProperty( name = "Hair Diffuse",
                                description = "Controls the diffuseness of hair. Value of 0 leads to fully specular hair, while value of 1 results in fully diffuse scattering. For physically plausible results this value should be set to black.",
                                default = (0, 0, 0),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaHairOpacitySocket( CoronaSolidSocket):
    bl_idname = "CoronaHairOpacity"
    default_value: FloatVectorProperty( name = "Hair Opacity",
                                description = "Controls the opacity of hair. Value of 0 leads to completely transparent hair, value of 1 leads to opaque hair. WARNING: Settings the opacity value to anything else than 1 leads to significant drop in performance.",
                                default = (1, 1, 1),
                                size = 3,
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaMultiplySocket( CoronaSolidSocket):
    bl_idname = "CoronaMultiply"
    default_value: FloatVectorProperty( name = "Multiply",
                                default = (1, 1, 1),
                                size = 3,
                                soft_min = -100,
                                soft_max = 100,
                                subtype = 'NONE')

class CoronaOffsetSocket( CoronaSolidSocket):
    bl_idname = "CoronaOffset"
    default_value: FloatVectorProperty( name = "Offset",
                                default = (0, 0, 0),
                                size = 3,
                                soft_min = -100,
                                soft_max = 100,
                                subtype = 'NONE')

class CoronaLinkedMaterialSocket( CoronaSolidSocket):
    bl_idname = "CoronaLinkedMaterial"
    bl_label = "Material"

    default_value: FloatProperty( name = "Linked",
                                min = 0,
                                max = 1,
                                default = 1)

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

    def draw_color( self, context, node):
        return (0.4, 0.8, 0.4, 1)

class CoronaLinkedMapSocket( CoronaSolidSocket):
    bl_idname = "CoronaLinkedMap"
    bl_label = "UV Link"

    default_value: FloatProperty( name = "" )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

class CoronaLinkedSocket( CoronaSocket):
    bl_idname = "CoronaLinked"
    bl_label = "UV map"

    default_value: FloatProperty( name = "" )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def get_socket_value( self, node, root):
        return None

    def draw_color( self, context, node):
        return (0.3, 0.5, 0.8, 1)

# class CoronaLayeredMaterialSocket( CoronaSolidSocket):
#     bl_idname = "CoronaLayeredMaterial"
#     bl_label = "Material"

#     weight: FloatProperty( name = "Weight",
#                                 description = "",
#                                 default = 1,
#                                 min = 0, max = 1)

#     def draw( self, context, layout, node, text):
#         layout.prop( self, "weight", text)

#     def get_socket_value( self, node, root):
#         return self.weight

#     def draw_color( self, context, node):
#         return (0.8, 0.3, 0.8, 1)

# Mix shader sockets.
class CoronaLayeredWeightSocket( CoronaSolidSocket):
    bl_idname = "CoronaLayeredWeight"
    bl_label = "Weight"

    default_value: FloatProperty( name = "Weight",
                                description = "Color / texture by which to mix two input colors",
                                default = 0.5,
                                min = 0, max = 1)

# Mix shader sockets.
class CoronaMixAmountSocket( CoronaSolidSocket):
    bl_idname = "CoronaMixAmount"
    bl_label = "Amount"

    default_value: FloatVectorProperty( name = "Amount",
                                description = "Color / texture by which to mix two input colors",
                                default = (1, 1, 1),
                                min = 0, max = 1,
                                subtype = 'COLOR')

# Ray switch shader sockets.
class CoronaRaySwitchSocket( CoronaSolidSocket):
    bl_idname = "CoronaRaySwitchColor"
    bl_label = "Ray Switch Color"

    default_value: FloatVectorProperty( name = "Color",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Fresnel shader sockets.
class CoronaFresnelChildSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelChild"
    bl_label = "Child"

    default_value: FloatVectorProperty( name = "Color",
                                description = "Applies a fresnel curve on mono input provided by a child node",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Fresnel shader sockets.
class CoronaFresnelPerpSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelPerp"
    bl_label = "Perpendicular"

    default_value: FloatVectorProperty( name = "Color",
                                description = "Color / texture used for grazing angles",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

class CoronaFresnelParSocket( CoronaSolidSocket):
    bl_idname = "CoronaFresnelPar"
    bl_label = "Parallel"

    default_value: FloatVectorProperty( name = "Color",
                                description = "Color / texture used for rays directly facing the camera",
                                default = (1, 1, 1),
                                min = 0,
                                max = 1,
                                subtype = 'COLOR')

# Material output node sockets.
# Diffuse.
class CoronaDiffuseLevelSocket( CoronaSocket):
    bl_idname = "CoronaDiffuseLevel"
    bl_label = "Diffuse Level"

    # default_value = CoronaMatProps.diffuse_level

    def init( self, context):
        self.default_value = CoronaMatProps.diffuse_level

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 0)

class CoronaKdSocket( CoronaSocket):
    bl_idname = "CoronaKd"
    bl_label = "Kd"

    # default_value = CoronaMatProps.kd
    def init( self, context):
        self.default_value = CoronaMatProps.kd

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs["Diffuse Level"].default_value
        # debug("Level:", node.inputs["Diffuse Level"].default_value)
        return '%.6f %.6f %.6f' % (self.default_value[0] * mult,
                                      self.default_value[1] * mult,
                                      self.default_value[2] * mult)

# Translucency.
class CoronaTranslucencyLevelSocket( CoronaSocket):
    bl_idname = "CoronaTranslucencyLevel"
    bl_label = "Translucency Level"

    # default_value = CoronaMatProps.translucency_level

    def init( self, context):
        self.default_value = CoronaMatProps.translucency_level

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = "Translucency Level")

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 0)

    def get_socket_value( self, node, root):
        return '%.6f' % self.default_value


class CoronaTranslucencySocket( CoronaSocket):
    bl_idname = "CoronaTranslucency"
    bl_label = "Translucency"

    # default_value =  CoronaMatProps.translucency

    def init( self, context):
        self.default_value = CoronaMatProps.translucency

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        return '%.6f %.6f %.6f' % (self.default_value[0],
                                                self.default_value[1],
                                                self.default_value[2])
# Reflection.
class CoronaKsSocket( CoronaSocket):
    bl_idname = "CoronaKs"
    bl_label = "Ks"

    # default_value =  CoronaMatProps.ks

    def init( self, context):
        self.default_value = CoronaMatProps.ks

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Reflect Level'].default_value
        return '%.6f %.6f %.6f' % ( self.default_value[0] * mult,
                                         self.default_value[1] * mult,
                                         self.default_value[2] * mult)

class CoronaReflectLevelSocket( CoronaSocket):
    bl_idname = "CoronaReflectLevel"
    bl_label = "Level"

    # default_value = CoronaMatProps.ns

    def init( self, context):
        self.default_value = CoronaMatProps.ns

    def draw( self, context, layout, node, text):
        layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 0)


class CoronaReflectGlossSocket( CoronaSocket):
    bl_idname = "CoronaReflectGloss"
    bl_label = "Glossiness"

    #default_value =  CoronaMatProps.reflect_glossiness

    def init( self, context):
        self.default_value = CoronaMatProps.reflect_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaReflectFresnelSocket( CoronaSocket):
    bl_idname = "CoronaReflectFresnel"
    bl_label = "Fresnel"

    # default_value =  CoronaMatProps.reflect_fresnel

    def init( self, context):
        self.default_value = CoronaMatProps.reflect_fresnel

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaAnisotropySocket( CoronaSocket):
    bl_idname = "CoronaAnisotropy"
    bl_label = "Anisotropy"

    # default_value = CoronaMatProps.anisotropy

    def init( self, context):
        self.default_value = CoronaMatProps.anisotropy

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    # def get_socket_value( self, node, root):
    #     aniso_rot = node.inputs['Anisotropy Rotation'].default_value
    #     return '%.6f %.6f' % ( self.default_value, aniso_rot)

class CoronaAnisotropyRotSocket( CoronaSocket):
    bl_idname = "CoronaAnisotropyRot"
    bl_label = "Anisotropy Rotation"

    # default_value = CoronaMatProps.aniso_rotation

    def init( self, context):
        self.default_value = CoronaMatProps.aniso_rotation

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Refraction.
class CoronaNiSocket( CoronaSocket):
    bl_idname = "CoronaRefractIOR"
    bl_label = "IOR"

    # default_value = CoronaMatProps.ni

    def init( self, context):
        self.default_value = CoronaMatProps.ni

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaRefractColorSocket( CoronaSocket):
    bl_idname = "CoronaRefractColor"
    bl_label = "Color"

    # default_value =  CoronaMatProps.refract

    def init( self, context):
            self.default_value = CoronaMatProps.refract

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Refract Level'].default_value
        return '%.6f %.6f %.6f' % ( self.default_value[0] * mult,
                                              self.default_value[1] * mult,
                                              self.default_value[2] * mult)

class CoronaRefractGlossSocket( CoronaSocket):
    bl_idname = "CoronaRefractGloss"
    bl_label = "Glossiness"

    # default_value =  CoronaMatProps.refract_glossiness
    
    def init( self, context):
        self.default_value = CoronaMatProps.refract_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaRefractModeSocket( CoronaSocket):
    bl_idname = "CoronaRefractMode"
    bl_label = "Mode"

    default_value: EnumProperty( name = "Mode",
                                description = "Refraction mode",
                                items = [
                                ('thin', "Thin", "Thin - no refraction"),
                                ('caustics', "Caustics", "Enable refractive caustics (slow)"),
                                ('hybrid', "Hybrid", "Hybrid mode of computing caustics only for direct rays")],
                                default = 'hybrid')

    def draw( self, context, layout, node, text):
        layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%s' % self.default_value

class CoronaRefractLevelSocket( CoronaSocket):
    bl_idname = "CoronaRefractLevel"
    bl_label = "Level"

    # default_value = CoronaMatProps.refract_level
    
    def init( self, context):
        self.default_value = CoronaMatProps.refract_level
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 0)


class CoronaAbsorptionDistSocket( CoronaSocket):
    bl_idname = "CoronaAbsorptionDist"
    bl_label = "Absorption Distance"

    # default_value = CoronaMatProps.absorption_distance

    def init( self, context):
            self.default_value = CoronaMatProps.absorption_distance

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0, 0, 0, 0)

class CoronaAbsorptionColorSocket( CoronaSocket):
    bl_idname = "CoronaAbsorptionColor"
    bl_label = "Absorption Color"

    # default_value = CoronaMatProps.absorption_color

    def init( self, context):
            self.default_value = CoronaMatProps.absorption_color

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.6f %.6f %.6f' % ( self.default_value[0],
                                                       self.default_value[1],
                                                       self.default_value[2])

# Emission.
class CoronaEmissionGlossSocket( CoronaSocket):
    bl_idname = "CoronaEmissionGloss"
    bl_label = "Emission Glossiness"

    # default_value =  CoronaMatProps.emission_gloss

    def init( self, context):
            self.default_value = CoronaMatProps.emission_gloss

    def draw( self, context, layout, node, text):

        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaKeSocket( CoronaSocket):
    bl_idname = "CoronaKe"
    bl_label = "Emission Color"

    # default_value =  CoronaMatProps.ke
    def init( self, context):
            self.default_value = CoronaMatProps.ke
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        mult = node.inputs['Multiplier'].default_value
        return '%.6f %.6f %.6f' % ( self.default_value[0] * mult,
                                         self.default_value[1] * mult,
                                         self.default_value[2] * mult)

class CoronaEmissionMultSocket( CoronaSocket):
    bl_idname = "CoronaEmissionMult"
    bl_label = "Emission Multiplier"

    # default_value = CoronaMatProps.emission_mult

    def init( self, context):
            self.default_value = CoronaMatProps.emission_mult

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return ''

class CoronaEmissionMultSocketZero( CoronaSocket):
    bl_idname = "CoronaEmissionMultZero"
    bl_label = "Emission Multiplier"

    # default_value = CoronaMatProps.emission_mult_zero
    def init( self, context):
            self.default_value = CoronaMatProps.emission_mult_zero

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return ''


class CoronaPointSocket( CoronaSocket):
    bl_idname = "CoronaPoint"
    bl_label = "Point"

    default_value: FloatProperty( name = "" )

    socket_x: FloatProperty( name = "X",
                                soft_min = 0,
                                soft_max = 1,
                                default = 0)

    socket_y: FloatProperty( name = "Y",
                                soft_min = 0,
                                soft_max = 1,
                                default = 0)

    def draw( self, context, layout, node, text):
        row = layout.row()
        row.prop( self, "socket_x", text = "X")
        row.prop( self, "socket_y", text = "Y")

    def draw_color( self, context, node):
        return (0.1, 0.1, 0.1, 0.1)

    def get_socket_value( self, node, root):
        xml_point = Element('point')
        xml_point.text = '%.6f' % self.socket_y
        xml_point.set("position", '%.6f' % self.socket_x)
        return xml_point

class CoronaPointColorSocket( CoronaSocket):
    bl_idname = "CoronaPointColor"
    bl_label = "Point"

    default_value: FloatProperty( name = "" )
    socket_x: FloatProperty( name = "X",
                                soft_min = 0,
                                soft_max = 1,
                                default = 0)
    socket_color: FloatVectorProperty(name = "Color",
                                    default = (0.5, 0.5, 1),
                                    subtype = 'COLOR'
                                    )

    def draw( self, context, layout, node, text):
        row = layout.row()
        row.prop( self, "socket_x", text = "X")
        row.prop( self, "socket_color", text = "Color")

    def draw_color( self, context, node):
        return (0.1, 0.1, 0.1, 0.1)

    def get_socket_value( self, node, root):
        xml_point = Element('point')
        xml_point.text = '%.6f %.6f %.6f' % (self.socket_color[0], self.socket_color[1], self.socket_color[2])
        xml_point.set("position", '%.6f' % self.socket_x)
        return xml_point

class CoronaOpacitySocket( CoronaSocket):
    bl_idname = "CoronaOpacity"
    bl_label = "Opacity"

    # default_value =   CoronaMatProps.opacity
    def init( self, context):
            self.default_value = CoronaMatProps.opacity
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node, root):
        return '%.6f %.6f %.6f' % ( self.default_value[0],
                                              self.default_value[1],
                                              self.default_value[2])

# Normal socket.
class CoronaNormalSocket( CoronaSocket):
    bl_idname = "CoronaNormal"
    bl_label = "Normal"

    default_value: FloatProperty( name = "" )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.67, 0.45, 1, 1)

# Bump socket.
class CoronaBumpSocket( CoronaSocket):
    bl_idname = "CoronaBump"
    bl_label = "Bump"

    default_value: FloatProperty( name = "Bump",
                                soft_min = 0,
                                soft_max = 10,
                                default = 1)

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaNormalMapSocket( CoronaSocket):
    bl_idname = "CoronaNormalMap"
    bl_label = "Normal"
    default_value: FloatVectorProperty(    name = "Normal",
                                            default = (0.5, 0.5, 1)
                                        )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.4, 0.4, 0.8, 1)

    def get_socket_value( self, node, root):
        return '%.6f %.6f %.6f' % ( self.default_value[0],
                                              self.default_value[1],
                                              self.default_value[2])

class CoronaObjectListSocket( CoronaSocket):
    bl_idname = "CoronaObjectList"
    bl_label = "Objects"

    default_value: FloatProperty( name = "" )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        #return (1, 0.2, 0, 1)
        return (0.8, 0.2, 0.2, 1)

    def get_socket_value( self, node, root):
        return self.default_value

# Factor socket.
class CoronaFactorSocket( CoronaSocket):
    bl_idname = "CoronaFactor"
    bl_label = "Factor"

    default_value: FloatProperty( name = "" )

    def draw( self, context, layout, node, text):
        layout.label( text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Scattering albedo socket.
class CoronaScatteringAlbedoSocket( CoronaSocket):
    bl_idname = "CoronaScattering"
    bl_label = "Scattering"

    # default_value = CoronaMatProps.scattering_albedo
    def init( self, context):
            self.default_value = CoronaMatProps.scattering_albedo

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.0, 1)

    def get_socket_value( self, node, root):
        return '%.6f %.6f %.6f' % ( self.default_value[0],
                                              self.default_value[1],
                                              self.default_value[2])

# Scattering albedo socket.
class CoronaMeanCosineSocket( CoronaSocket):
    bl_idname = "CoronaMeanCosine"
    bl_label = "Directionality"

    # default_value = CoronaMatProps.mean_cosine
    def init( self, context):
            self.default_value = CoronaMatProps.mean_cosine

    def draw( self, context, layout, node, text):
        layout.prop( self, "default_value", text = text)

    def draw_color( self, context, node):
        return (0, 0, 0, 0)

    def get_socket_value( self, node, root):
        return '%.6f' % ( self.default_value)

class CoronaStrengthSocket( CoronaSocket):
    bl_idname = "CoronaStrength"
    bl_label = "Strength"

    default_value: FloatProperty( name = "Strength",
                                soft_min = 0,
                                soft_max = 1,
                                default = 1,
                                step = 0.1)

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label(text)
        else:
            layout.prop(self, "default_value", text = text)

    def draw_color(self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value(self, node, root):
        return '%.6f' % (self.default_value)

    def get_socket_real_value(self):
        try:
            return self.links[0].from_socket.get_socket_real_value()
        except:
            return self.default_value

def register():
    # bpy.utils.register_class( CoronaColorSocket)
    # bpy.utils.register_class( CoronaAbsorptionColorSocket)
    # bpy.utils.register_class( CoronaAbsorptionDistSocket)
    # bpy.utils.register_class( CoronaMeanCosineSocket)
    # bpy.utils.register_class( CoronaScatteringAlbedoSocket)
    # bpy.utils.register_class( CoronaRefractLevelSocket)
    # bpy.utils.register_class( CoronaRefractModeSocket)
    # bpy.utils.register_class( CoronaRefractGlossSocket)
    # bpy.utils.register_class( CoronaRefractColorSocket)
    # bpy.utils.register_class( CoronaNiSocket)
    # bpy.utils.register_class( CoronaAnisotropyRotSocket)
    # bpy.utils.register_class( CoronaAnisotropySocket)
    # bpy.utils.register_class( CoronaReflectFresnelSocket)
    # bpy.utils.register_class( CoronaReflectGlossSocket)
    # bpy.utils.register_class( CoronaReflectLevelSocket)
    # bpy.utils.register_class( CoronaKsSocket)
    # bpy.utils.register_class( CoronaTranslucencySocket)
    # bpy.utils.register_class( CoronaKdSocket)
    # bpy.utils.register_class( CoronaEmissionGlossSocket)
    # bpy.utils.register_class( CoronaEmissionMultSocket)
    # bpy.utils.register_class( CoronaKeSocket)
    # bpy.utils.register_class( CoronaOpacitySocket)
    # bpy.utils.register_class( CoronaMixASocket)
    # bpy.utils.register_class( CoronaMixBSocket)
    # bpy.utils.register_class( CoronaMixAmountSocket)
    # bpy.utils.register_class( CoronaFresnelParSocket)
    # bpy.utils.register_class( CoronaFresnelPerpSocket)
    # bpy.utils.register_class( CoronaBumpSocket)
    # bpy.utils.register_class( CoronaDiffuseLevelSocket)
    # bpy.utils.register_class( CoronaTranslucencyLevelSocket)
    # bpy.utils.register_class( CoronaRaySwitchSocket)
    # bpy.utils.register_class( CoronaFactorSocket)
    bpy.utils.register_class( CoronaSolidSocket)
    bpy.utils.register_class( CoronaUVWSocket)

def unregister():
    # bpy.utils.unregister_class( CoronaColorSocket)
    # bpy.utils.unregister_class( CoronaAbsorptionColorSocket)
    # bpy.utils.unregister_class( CoronaAbsorptionDistSocket)
    # bpy.utils.unregister_class( CoronaMeanCosineSocket)
    # bpy.utils.unregister_class( CoronaScatteringAlbedoSocket)
    # bpy.utils.unregister_class( CoronaRefractLevelSocket)
    # bpy.utils.unregister_class( CoronaRefractModeSocket)
    # bpy.utils.unregister_class( CoronaRefractGlossSocket)
    # bpy.utils.unregister_class( CoronaRefractColorSocket)
    # bpy.utils.unregister_class( CoronaNiSocket)
    # bpy.utils.unregister_class( CoronaAnisotropyRotSocket)
    # bpy.utils.unregister_class( CoronaAnisotropySocket)
    # bpy.utils.unregister_class( CoronaReflectFresnelSocket)
    # bpy.utils.unregister_class( CoronaReflectGlossSocket)
    # bpy.utils.unregister_class( CoronaReflectLevelSocket)
    # bpy.utils.unregister_class( CoronaKsSocket)
    # bpy.utils.unregister_class( CoronaTranslucencySocket)
    # bpy.utils.unregister_class( CoronaKdSocket)
    # bpy.utils.unregister_class( CoronaEmissionGlossSocket)
    # bpy.utils.unregister_class( CoronaEmissionMultSocket)
    # bpy.utils.unregister_class( CoronaKeSocket)
    # bpy.utils.unregister_class( CoronaOpacitySocket)
    # bpy.utils.unregister_class( CoronaMixASocket)
    # bpy.utils.unregister_class( CoronaMixBSocket)
    # bpy.utils.unregister_class( CoronaMixAmountSocket)
    # bpy.utils.unregister_class( CoronaFresnelParSocket)
    # bpy.utils.unregister_class( CoronaFresnelPerpSocket)
    # bpy.utils.unregister_class( CoronaBumpSocket)
    # bpy.utils.unregister_class( CoronaDiffuseLevelSocket)
    # bpy.utils.unregister_class( CoronaTranslucencyLevelSocket)
    # bpy.utils.unregister_class( CoronaRaySwitchSocket)
    # bpy.utils.unregister_class( CoronaFactorSocket)
    bpy.utils.unregister_class( CoronaSolidSocket)
    bpy.utils.unregister_class( CoronaUVWSocket)
