import math, mathutils
import bpy
from bpy.props import FloatProperty, IntProperty, FloatVectorProperty, EnumProperty, BoolProperty, StringProperty, PointerProperty
from ..util    import node_enumerator, debug, view3D_mat_update, colorTemperature2rgb

def dummyupdate(self, context):
    # debug("dupdate")
    view3D_mat_update(self, context)
    # context.object.active_material.diffuse_color = context.object.active_material.diffuse_color


def updateKelvin(self, context):
    if self.kelvin != 0:
        r,g,b = colorTemperature2rgb(self.kelvin)
        if hasattr(self, 'inputs'):
            self.inputs['Emission Color'].default_value = (r, g, b)
        elif hasattr(self, 'ke'):
            self.ke = (r,g,b)

#------------------------------------
# Texture properties
#------------------------------------
class CoronaTexProps( bpy.types.PropertyGroup):

    show_settings: BoolProperty( name = "Show Texture Settings",
                                description = "",
                                default = False)


    texture: StringProperty( name = "Texture",
                                description = "Texture to influence attribute - only UV image maps are supported",
                                default = "",
                                update = dummyupdate)

    intensity: FloatProperty( name = "Intensity",
                                description = "Latitude-longitude map relative brightness",
                                default = 1.0,
                                min = 0.0,
                                soft_max = 5,
                                update = dummyupdate)

    uOffset: FloatProperty( name = "Offset U",
                                description = "Map U offset",
                                default = 0,
                                min = 0.0,
                                max = 1,
                                update = dummyupdate)

    vOffset: FloatProperty( name = "Offset V",
                                description = "Map V offset",
                                default = 0,
                                min = 0.0,
                                max = 1,
                                update = dummyupdate)

    uScaling: FloatProperty( name = "Scaling U",
                                description = "Map U scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10,
                                update = dummyupdate)

    vScaling: FloatProperty( name = "Scaling V",
                                description = "Map V scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10,
                                update = dummyupdate)


    uv_mode: EnumProperty(
                name="UV Mode",
                description="",
                items=[
                ('uvw', 'UVW', ''),
                ('world', '3D World', 'This option "flows" over the obect as it is rotated/translated'),
                ('local', '3D Local', 'This option sticks to the object when it is rotated/translated'),
                ('forceEnviro', 'Force Environment', 'The map will be evaluated as environment-mapped even when applied to geometry, see Environment Mode')]
            )

    uv_map_channel: IntProperty(
                name = "Map Channel",
                description = "Must use the binary object output to use this property. -1 Disables this property",
                default = -1,
                min = -1,
                soft_max = 10)

    uv_scale: FloatVectorProperty(
                name = "XYZ Scale",
                description = "",
                subtype = "XYZ",
                default = (1.0, 1.0, 1.0))

    uv_offset: FloatVectorProperty(
                name = "XYZ Offset",
                description = "",
                unit='LENGTH',
                subtype = "TRANSLATION")

    uv_rotate_z: FloatProperty(
                name="Rotate Z",
                description="Rotation of the map in the UV-plane when using the UVW mode, in degrees",
                unit='ROTATION',
                # subtype="ANGLE",
                default=0.0)

    uv_enviro_rotate: FloatProperty(
                name="Environment Rotate",
                description="",
                unit='ROTATION',
                # subtype="ANGLE",
                default=0.0)

    uv_enviro_mode: EnumProperty(
                name="Environment Mode",
                description="",
                default="spherical",
                items=[
                ('spherical', 'Spherical', ''),
                ('screen', 'Screen', '')]
            )

    uv_wrap_mode_u: EnumProperty(
                name="Wrap Mode U",
                description="Controls what happens outside of the 0-1 UVW range",
                default="repeat",
                items=[
                ('clamp', 'Clamp', 'The border pixels will be returned outside of the range'),
                ('repeat', 'Repeat', 'The entire map will be repeated outside of the range'),
                ('mirror', 'Mirror', 'The map will be mirrored outside the range')]
            )

    uv_wrap_mode_v: EnumProperty(
                name="Wrap Mode V",
                description="Controls what happens outside of the 0-1 UVW range",
                default="repeat",
                items=[
                ('clamp', 'Clamp', 'The border pixels will be returned outside of the range'),
                ('repeat', 'Repeat', 'The entire map will be repeated outside of the range'),
                ('mirror', 'Mirror', 'The map will be mirrored outside the range')]
            )

    uv_blur: FloatProperty(
                name="Blur",
                description="Amount of blur to apply to the map",
                default=0.0,
                min = 0.0)

    uv_use_real_scale: BoolProperty(
                name="Use Real World Scale",
                description="",
                default=False)

#------------------------------------
# Material properties
#------------------------------------
class CoronaMatProps(bpy.types.PropertyGroup):
    # Nodes
    node_tree: StringProperty( name = "Node Tree",
                                description = "Material node tree to link to the current material")

    node_output: StringProperty( name = "Output Node",
                                description = "Material node tree output node to link to the current material")

    use_nodes: BoolProperty( name = "Use Corona Nodes",
                                description = "Use Corona Node Tree",
                                update = dummyupdate)

    use_lightmix: BoolProperty( name = "Use Lightmix",
                                description = "Include this light in the lightmix options")

    lightmix_group: StringProperty( name = "Lightmix Group",
                                description = "Optional. Allows multiple lights to be included as a single lightmix control")

    # Material preview.
    preview_quality: FloatProperty( name = "Preview Quality",
                                description = "Quality of material preview. Raising this value will increase render time for material preview",
                                default = 0.3,
                                min = 0.0,
                                max = 10,
                                step = 10,
                                precision = 1,
                                update = dummyupdate)

    mtl_type: EnumProperty( name = "Corona Material",
                                description = "Corona material type",
                                items = [
                                ('coronamtl', 'CoronaMtl', 'Corona surface shading material'),
                                ('coronalightmtl', 'CoronaLightMtl', 'Corona mesh light material'),
                                ('coronavolumemtl', 'CoronaVolumeMtl', 'Corona volume material'),
                                ('coronaportalmtl', 'CoronaPortalMtl', 'Corona portal material')],
                                default = 'coronamtl',
                                update = dummyupdate)

    #Viewport color (corona output node)
    viewport_color: FloatVectorProperty( name = "Viewport", #FloatVectorProperty
                                description = "Viewport Color",
                                default = (0.9, 0.9, 0.9),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    # Diffuse properties.
    kd: FloatVectorProperty( name = "Diffuse Color",
                                description = "Diffuse color of material",
                                default = (0.9, 0.9, 0.9),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    diffuse_level: FloatProperty( name = "Level",
                                description = "Influence of diffuse color",
                                default = 1.0,
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    # Translucency properties.
    translucency: FloatVectorProperty( name = "Translucency Color",
                                description = "Translucency color of material",
                                default = (0.0, 0.0, 0.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    translucency_level: FloatProperty( name = "Level",
                                description = "Influence of Translucency",
                                default = 0.0,
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    # Reflection properties.
    ks: FloatVectorProperty( name = "Reflection Color",
                                description = "Reflection color of material",
                                default = (1, 1, 1),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    ns: FloatProperty( name = "Level",
                                description = "Reflectivity of material",
                                default = 0,
                                min = 0,
                                max = 1,
                                update = dummyupdate)

    reflect_glossiness: FloatProperty( name = "Glossiness",
                                description ="Glossiness of reflective material",
                                default = 1.0,
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    reflect_fresnel: FloatProperty( name = "Fresnel",
                                description = "Index of refraction for fresnel reflections",
                                default = 1.5,
                                soft_min = 1.0,
                                soft_max = 100.0,
                                update = dummyupdate  )

    anisotropy: FloatProperty( name = "Anisotropy",
                                description = "Anisotropy of reflective material",
                                default = 0.5,
                                soft_min = 0.0,
                                soft_max = 1.0,
                                update = dummyupdate)

    aniso_rotation: FloatProperty( name = "Anisotropy Rotation",
                                description = "Anisotropy rotation",
                                default = 0.0,
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    kelvin: FloatProperty(name = "Color Temperature",
                                        description = "Temperature color in Kelvin, is converted to an approximate RGB color so won't match Corona exactly. (zero = disabled and default)",
                                        default = 0,
                                        min = 0,
                                        max = 40000,
                                        soft_min = 0,
                                        soft_max = 15000,
                                        step = 1,
                                        precision = 0,
                                        update = updateKelvin)

    # Emission properties.
    ke: FloatVectorProperty( name = "Emission Color",
                                description = "Emission color of material",
                                default = (1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    emission_mult: FloatProperty( name = "Emission Multiplier",
                                description = "Emission energy multiplier",
                                # default = 0.0,
                                min = 0.0,
                                update = dummyupdate)

    emission_mult_zero: FloatProperty( name = "Emission Multiplier",
                                description = "Emission energy multiplier",
                                default = 0.0,
                                min = 0.0,
                                update = dummyupdate)

    self_illumination: BoolProperty( name = "Self Illumination",
                                description = "When true it disables sampling and provides self illumination only",
                                default = False,
                                update = dummyupdate)

    both_sides: BoolProperty( name = "Two Sided",
                                description = "When true it emits light from both sides",
                                default = False,
                                update = dummyupdate)

    shadowcatcher_illuminator: BoolProperty( name = "Shadowcatcher Illuminator",
                                description = "",
                                default = False,
                                update = dummyupdate)

    emission_gloss: FloatProperty( name = "Emission Directionality",
                                description = "Phong exponent describing emission distribution - 0 produces standard diffuse lights, higher values produce spotlights",
                                default = 0.0,
                                min = 0.0,
                                max = 100.0,
                                update = dummyupdate)

    ies_profile: StringProperty( name = "IES File",
                                description = "Path to IES emission profile",
                                default = '',
                                subtype = 'FILE_PATH',
                                update = dummyupdate)

    keep_sharp: BoolProperty( name = "Keep Sharp",
                                description = "Enable fake mode to enhance sharpness of the profile",
                                default = False,
                                update = dummyupdate)

    ies_translate: FloatVectorProperty( name = "Translate",
                                description = "Translation of IES profile",
                                default = (0.0, 0.0, 0.0),
                                subtype = "TRANSLATION",
                                size=3,
                                update = dummyupdate)

    ies_rotate: FloatVectorProperty( name = "Rotate",
                                description = "Rotation of IES profile",
                                default = (0.0, 0.0, 0.0),
                                subtype = "XYZ",
                                unit = "ROTATION",
                                size=3,
                                update = dummyupdate)

    ies_scale: FloatVectorProperty( name = "Scale",
                                description = "Scale of IES profile",
                                default = (1.0, 1.0, 1.0),
                                subtype = "XYZ",
                                unit = "NONE",
                                size=3,
                                update = dummyupdate)

    # ies_matrix: FloatVectorProperty( name = "IES Matrix",
    #                             description = "Transform Matrix, for this IES profile",
    #                             size=16,
    #                             subtype='MATRIX',
    #                             default=[b for a in mathutils.Matrix.Identity(4) for b in a],
    #                             update = dummyupdate)

    # Refraction properties.
    ni: FloatProperty( name = "IOR",
                                description = "Index of refraction for refractive material",
                                default = 1.3,
                                min = 1.0,
                                max = 4.0,
                                update = dummyupdate)

    refract: FloatVectorProperty( name = "Refraction Color",
                                description = "Refraction color of refractive material",
                                default = (1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    refract_glossiness: FloatProperty( name = "Glossiness",
                                description = "Glossiness of refractive material",
                                default = 1.0,
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    # twosidedGlass:
    # Don't write anything to .mtl file if refraction_caustics enabled
    # 1 = two-sided ( thin, no refraction or caustics)  = refraction_thin enabled
    # 2 = hybrid (one-sided for primary rays, two-sided for secondary) : neither enabled
    refract_level: FloatProperty( name = "Level",
                                description = "Amount of refraction",
                                default = 0.0,
                                min = 0.0,
                                max = 1.0,
                                precision = 2,
                                update = dummyupdate)

    refract_caustics: BoolProperty( name = "Caustics",
                                description = "Enable refractive caustics (slow)",
                                default = False,
                                update = dummyupdate)

    refract_thin: BoolProperty( name = "Thin",
                                description = "Thin - no refraction",
                                default = False,
                                update = dummyupdate)

    absorption_distance: FloatProperty( name = "Distance",
                                description = "Absorption distance of the refractive or volumetric material (in scene units)",
                                default = 0.00,
                                min = 0.0,
                                max = 1000,
                                subtype = 'DISTANCE',
                                unit = 'LENGTH',
                                step = 4,
                                precision = 4,
                                update = dummyupdate)

    absorption_color: FloatVectorProperty( name = "Color",
                                description = "Absorption color of the refractive or volumetric material (the color a white ray will be after traveling the absorption distance into the medium)",
                                default = (1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    opacity: FloatVectorProperty( name = "Opacity Color",
                                description = "Opacity color of material. White = fully opaque, black = fully transparent",
                                default = (1.0, 1.0, 1.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    scattering_albedo: FloatVectorProperty( name = "Scattering Color",
                                description = "Sets the material volumetric scattering albedo. When set to non-zero value, volumetric rendering/SSS is enabled",
                                default = (0.0, 0.0, 0.0),
                                subtype = "COLOR",
                                min = 0.0,
                                max = 1.0,
                                update = dummyupdate)

    mean_cosine: FloatProperty( name = "Directionality",
                                description = "Sets the volumetric scattering anisotropy. Value 0 means isotropic (=diffuse) scattering, value 0.999 means almost perfect forward scattering, value -0.999 means almost perfect backward scattering",
                                default = 0,
                                min = -0.999,
                                max = 0.999,
                                update = dummyupdate)

    #Texture bools
    use_map_kd: BoolProperty( name = "Use Diffuse Texture",
                                description = "Use a texture to influence diffuse color",
                                default = False,
                                update = dummyupdate)

    use_map_ks: BoolProperty( name = "Use Reflection Color Texture",
                                description = "Use a texture to influence reflection color",
                                default = False,
                                update = dummyupdate)

    use_map_ks_gloss: BoolProperty( name = "Use Reflection Gloss Texture",
                                description = "Use a texture to influence reflection glossiness",
                                default = False,
                                update = dummyupdate)

    use_map_opacity: BoolProperty( name = "Use Opacity Texture",
                                description = "Use a texture to influence opacity",
                                default = False,
                                update = dummyupdate)

    use_map_ns: BoolProperty( name = "Use Reflection Texture",
                                description = "Use a texture to influence reflection value",
                                default = False,
                                update = dummyupdate )

    use_map_ke: BoolProperty( name = "Use Emission Texture",
                                description = "Use a texture to influence emission",
                                default = False,
                                update = dummyupdate)

    use_map_translucency: BoolProperty( name = "Use Translucency Texture",
                                description = "Use a texture to influence translucency",
                                default = False,
                                update = dummyupdate)

    use_map_translucency_level: BoolProperty( name = "Use Translucency Level Texture",
                                description = "Use a textrue to influence translucency level",
                                default = False,
                                update = dummyupdate)

    use_map_refract: BoolProperty( name = "Use Refraction Texture",
                                description = "Use a texture to influence refraction",
                                default = False,
                                update = dummyupdate)

    use_map_normal: BoolProperty( name = "Use Normal Texture",
                                description = "Use a texture to influence normal",
                                default = False,
                                update = dummyupdate)

    use_map_bump: BoolProperty( name = "Use Bump Texture",
                                description = "Use a texture to influence bump",
                                default = False,
                                update = dummyupdate)

    use_map_aniso: BoolProperty( name = "Use Anisotropy Texture",
                                description = "Use a texture to influence anisotropy",
                                default = False,
                                update = dummyupdate)

    use_map_aniso_rot: BoolProperty( name = "Use Anisotropy Rotation Texture",
                                description = "Use a texture to influence anisotropy rotation",
                                default = False,
                                update = dummyupdate)

    use_map_scattering: BoolProperty( name = "Use Scattering Albedo Texture",
                                description = "Use a texture to influence scattering albedo",
                                default = False,
                                update = dummyupdate)

    use_map_absorption: BoolProperty( name = "Use Absorption Texture",
                                description = "Use a texture to influence absorption",
                                default = False,
                                update = dummyupdate)

    map_kd: PointerProperty( type = CoronaTexProps)

    map_ks: PointerProperty( type = CoronaTexProps)

    map_ks_gloss: PointerProperty( type = CoronaTexProps)

    map_opacity: PointerProperty( type = CoronaTexProps)

    map_ns: PointerProperty( type = CoronaTexProps)

    map_ke: PointerProperty( type = CoronaTexProps)

    map_translucency: PointerProperty( type = CoronaTexProps)

    map_translucency_level: PointerProperty( type = CoronaTexProps)

    map_refract: PointerProperty( type = CoronaTexProps)

    map_normal: PointerProperty( type = CoronaTexProps)

    map_bump: PointerProperty( type = CoronaTexProps)

    map_aniso: PointerProperty( type = CoronaTexProps)

    map_aniso_rot: PointerProperty( type = CoronaTexProps)

    map_scattering: PointerProperty( type = CoronaTexProps)

    map_absorption: PointerProperty( type = CoronaTexProps)

    #Portal property
    as_portal: BoolProperty( name = "Use material as light portal",
                                description = "Material will be used as a light portal",
                                default = False,
                                update = dummyupdate)

    rounded_corners: FloatProperty( name = "Rounded Corners",
                                description = "Rounded corner radius (in mm)",
                                default = 0.0,
                                min = 0.0,
                                subtype = 'DISTANCE',
                                unit = 'LENGTH',
                                update = dummyupdate)

    # ray_gi_inv: BoolProperty( name = "GI Ray", description = "Invisibility of material to GI rays", default = False,
    #                             update = dummyupdate)

    # ray_direct_inv: BoolProperty( name = "Direct Ray", description = "Invisibility of material to direct rays", default = False,
    #                             update = dummyupdate)

    # ray_refract_inv: BoolProperty( name = "Refract Ray", description = "Invisibility of material to refraction rays", default = False,
    #                             update = dummyupdate)

    # ray_reflect_inv: BoolProperty( name = "Reflection Ray", description = "Invisibility of material to reflection rays", default = False,
    #                             update = dummyupdate)

    # ray_shadows_inv: BoolProperty( name = "Shadow Ray", description = "Invisibility of material to shadow rays", default = False,
    #                             update = dummyupdate)

def register():
    bpy.utils.register_class( CoronaTexProps)
    bpy.utils.register_class( CoronaMatProps)
    bpy.types.Material.corona = PointerProperty( type = CoronaMatProps )

def unregister():
    bpy.utils.unregister_class( CoronaMatProps)
    bpy.utils.unregister_class( CoronaTexProps)
    del(bpy.types.Material.corona)
