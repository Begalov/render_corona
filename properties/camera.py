import bpy
from bpy.props import FloatProperty, EnumProperty, BoolProperty, PointerProperty, IntProperty
import math

def update_cam_type( self, context):
    cam_data = context.object.data
    if cam_data.corona.camera_type == 'ortho':
        cam_data.type = 'ORTHO'
    if cam_data.corona.camera_type == 'perspective':
        cam_data.type = 'PERSP'

def update_cam_width( self, context):
    cam_data = context.object.data
    cam_data.ortho_scale = 2 * cam_data.corona.ortho_width

#------------------------------------
# Camera properties
#------------------------------------
class CoronaCameraProps( bpy.types.PropertyGroup):

    camera_type: EnumProperty( items = [
                                    ('ortho', 'Orthographic Camera', ''),
                                    ('perspective', 'Perspective Camera', ''),
                                    ('spherical', 'Spherical Camera', ''),
                                    ('cylindrical', 'Cylindrical Camera', ''),
                                    ('cubemap', 'Cubemap Camera', '')],
                                    name = "Camera Type",
                                    description = "Camera model",
                                    default = 'perspective',
                                    update = update_cam_type)

    use_dof: BoolProperty( name = "Enable Depth of Field",
                                    description = "Enable depth of the field rendering",
                                    default = False)

    camera_dof: FloatProperty( name = "F-stop",
                                    description = "Perspective camera f-stop value",
                                    default = 16,
                                    min = 0.01,
                                    max = 50,
                                    step =3,
                                    precision = 1)

    bokeh_blades: IntProperty( name = "Blades",
                                    description = "Number of camera aperture blades, which sets the shape of out-of-focus highlights - must be 3 or higher for polygonal bokeh",
                                    default = 3,
                                    min = 3,
                                    soft_max = 128)

    bokeh_rotation: FloatProperty( name = "Rotation",
                                    description = "Relative rotation of bokeh shape in degrees",
                                    min = -math.pi,
                                    max = math.pi,
                                    subtype = 'ANGLE',
                                    unit = 'ROTATION')

    ortho_width: FloatProperty( name = "Width",
                                    description = "Width of the captured image",
                                    default = 4.0,
                                    min = 1.0,
                                    update = update_cam_width)

    cylindrical_height: FloatProperty( name = "Height",
                                    description = "Height of the cylindrical camera",
                                    default = 4.0,
                                    min = 1.0,
                                    unit = 'LENGTH',
                                    subtype = 'DISTANCE')

    use_region: BoolProperty( name = "Region Render Override",
                                    description = "Use border render region to zoom into the image, without changing resolution or creating black borders",
                                    default = False)

    render_clipping: BoolProperty( name = "Render Clipping",
                                    description = "Render camera clipping distances. Disabling renders without clipping",
                                    default = False)

    use_vr: BoolProperty( name = "Use Stereo/VR",
                                    description = "Spherical and cube map camera can also enable stereo rendering",
                                    default = False)

    vr_eyeSeparation: FloatProperty( name = "Eye Separation",
                                    description = "Distance between the eye images when captured",
                                    default = 0.63,
                                    soft_min = 0.50,
                                    soft_max = 0.80,
                                    unit = 'LENGTH',
                                    subtype = 'DISTANCE')

    vr_frontOffset: FloatProperty( name = "Front Offset",
                                    description = "",
                                    default = 0.0,
                                    unit = 'LENGTH',
                                    subtype = 'DISTANCE')

    vr_convergenceDistance: FloatProperty( name = "Convergence Distance",
                                    description = "",
                                    default = 0.0,
                                    unit = 'LENGTH',
                                    subtype = 'DISTANCE')

def register():
    bpy.utils.register_class( CoronaCameraProps)
    bpy.types.Camera.corona = PointerProperty( type = CoronaCameraProps)

def unregister():
    bpy.utils.unregister_class( CoronaCameraProps)
    del(bpy.types.Camera.corona)