import bpy
from bpy.props import StringProperty, BoolProperty, PointerProperty, EnumProperty


class CoronaObjects( bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Name")

#------------------------------------
# Object properties
#------------------------------------
class CoronaObjectProps(bpy.types.PropertyGroup):

    is_dirty: BoolProperty( name = "Is Dirty",
                                        description = "Object will be marked for export",
                                        default = False)

    is_proxy: BoolProperty( name = "Use Proxy",
                                        description = "Object will be used as a stand-in for another object",
                                        default = False)

    instance_mesh: StringProperty( name ="Instance Object",
                                        description = "Instance mesh to replace the proxy during export")

    use_external: BoolProperty( name = "Use External Object",
                                        description = "Use an external .obj file to replace the proxy during export")

    external_instance_mesh: StringProperty( name = "External Instance Object",
                                        description = "External .obj to replace the proxy during export",
                                        subtype = 'FILE_PATH')

    external_mtllib: StringProperty( name = "Instance Object .mtl",
                                        description = "External .mtl defining external instance object's materials",
                                        subtype = 'FILE_PATH')

    override_exclude: BoolProperty( name = "Exclude From Override",
                                        description = "Exclude this object from the scene-wide material override",
                                        default = False)

    export_vcol: BoolProperty( name = "Export Vertex Colors",
                                        description = "Export the stored vertex colors instead of the UVW map",
                                        default = False)

    use_mblur: BoolProperty( name = "Enable Motion Blur",
                                        description = "Enable rendering of motion blur",
                                        default = False)

    mblur_type: EnumProperty( items = [
                                        ('object', "Object", "Render object transformation motion blur for animated objects"),
                                        ('deform', "Deformation", "Render deformation motion blur for deformed objects")],
                                        name = "",
                                        description = "Type of rendered motion blur",
                                        default = 'object')

    #Export proxy
    proxy_export_path: StringProperty( name        = "Corona proxy export path",
                                        description = "Path to directory where proxy files should be exported.",
                                        subtype     = 'DIR_PATH',
                                        default     = '//proxy')

    proxy_export_name: StringProperty( name        = "Name",
                                        subtype     = 'FILE_NAME',
                                        description = "Name for obj/cgeo, mtl and mtlindex files.")



def register():
    bpy.utils.register_class( CoronaObjects)
    bpy.utils.register_class( CoronaObjectProps)
    bpy.types.Object.corona = PointerProperty( type = CoronaObjectProps)
def unregister():
    bpy.utils.unregister_class( CoronaObjects)
    bpy.utils.unregister_class( CoronaObjectProps)
