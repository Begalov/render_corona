import bpy
from bpy.props import StringProperty, BoolProperty, FloatVectorProperty, PointerProperty
from bpy.props import FloatProperty, IntProperty, EnumProperty, CollectionProperty

#------------------------------------
# Render Pass properties
#------------------------------------
class CoronaRenderPasses( bpy.types.PropertyGroup):
    render_pass: EnumProperty( items = [
                                        ('Albedo', 'Albedo', ''),
                                        ('Alpha', 'Alpha', ''),
                                        ('BloomGlare', 'Bloom Glare', ''),
                                        ('Components', 'Components', ''),
                                        ('Id', 'ID', ''),
                                        ('lightMix', 'Light Mix', ''),
                                        ('MapCoords', 'Map Coordinates', ''),
                                        ('Normals', 'Normals', ''),
                                        ('NormalsDiscrepancy', 'Normals Discrepancy', ''),
                                        ('NormalsDotProduct', 'Normals Dot Product', ''),
                                        ('PrimitiveCoords', 'Primitive Coordinates', ''),
                                        ('RawComponent', 'Raw Components', ''),
                                        ('RenderStamp', 'Render Stamp', ''),
                                        ('SamplingFocus', 'Sampling Focus', ''),
                                        ('Shadows', 'Shadows', ''),
                                        ('SourceColor', 'Source Color', ''),
                                        ('Velocity', 'Velocity', ''),
                                        ('WorldPosition', 'World Position', ''),
                                        ('ZDepth', 'Z Depth', ''),
                                        ],
                                        name = 'Render Pass',
                                        description = "Enable render pass",
                                        default = 'Alpha')

    multiplier: FloatProperty( name = "Multiplier",
                                        description = "",
                                        default = 1,
                                        soft_min = 0.0,
                                        soft_max = 1000)

    relative: BoolProperty( name = "Relative",
                                        description = "",
                                        default = False)

    cameraWorldMode: EnumProperty( items = [
                                        ('Camera', 'Camera', ''),
                                        ('World', 'World', ''),
                                        ],
                                        name = 'Mode',
                                        description = "",
                                        default = 'Camera')

    offset: FloatProperty( name = "Offset",
                                        description = "",
                                        subtype = 'DISTANCE',
                                        unit = 'LENGTH',
                                        default = 0,
                                        soft_min = -1000.0,
                                        soft_max = 1000)

    # Used only for ZDepth pass.
    z_min: FloatProperty( name = "Minimum",
                                        description = "Z distance minimum",
                                        subtype = 'DISTANCE',
                                        unit = 'LENGTH',
                                        default = 0,
                                        min = 0.0,
                                        soft_max = 1000)

    z_max: FloatProperty( name = "Maximum",
                                        description = "Z distance maximum",
                                        subtype = 'DISTANCE',
                                        unit = 'LENGTH',
                                        default = 100,
                                        min = 0.0,
                                        soft_max = 1000)

    # Options for Components passes.
    diffuseDirect: BoolProperty( name = "Diffuse",
                                        description = "Include direct diffuse material component",
                                        default = False)

    reflectDirect: BoolProperty( name = "Reflect",
                                        description = "Include direct reflection material component",
                                        default = False)

    refractDirect: BoolProperty( name = "Refract",
                                        description = "Include direct refraction material component",
                                        default = False)

    translucencyDirect: BoolProperty( name = "Translucency",
                                        description = "Include direct translucency material component",
                                        default = False)

    volumetricDirect: BoolProperty( name = "Volumetric",
                                        description = "Include direct volumetric material component",
                                        default = False)

    diffuseIndirect: BoolProperty( name = "Diffuse",
                                        description = "Include indirect diffuse material component",
                                        default = False)

    reflectIndirect: BoolProperty( name = "Reflect",
                                        description = "Include indirect reflection material component",
                                        default = False)

    refractIndirect: BoolProperty( name = "Refract",
                                        description = "Include indirect refraction material component",
                                        default = False)

    refract: BoolProperty( name = "Refract",
                                        description = "Include indirect refraction material component",
                                        default = False)

    translucencyIndirect: BoolProperty( name = "Translucency",
                                        description = "Include indirect translucency material component",
                                        default = False)

    volumetricIndirect: BoolProperty( name = "Volumetric",
                                        description = "Include indirect volumetric material component",
                                        default = False)

    emission: BoolProperty( name = "Emission",
                                        description = "Include emission material component",
                                        default = False)

    # Options for RawComponents passes.
    components: EnumProperty( name = "Lighting Component",
                                        description = "Lighting component to include",
                                        items = [
                                        ('diffuse', 'Diffuse', ''),
                                        ('reflect', 'Reflect', ''),
                                        ('refract', 'Refract', ''),
                                        ('translucency', 'Translucency', '')],
                                        default = 'diffuse')

    normals: EnumProperty( name = "Normals Component",
                                        description = "Normals component to include",
                                        items = [
                                        ('shading', 'Shading', ''),
                                        ('geometry', 'Geometry', '')],
                                        default = 'shading')

    source_color: EnumProperty( name = "Source Color Component",
                                        description = "Source color component to include",
                                        items = [
                                        ('diffuse', 'Diffuse', ''),
                                        ('reflect', 'Reflect', ''),
                                        ('refract', 'Refract', ''),
                                        ('translucency', 'Translucency', ''),
                                        ('opacity', 'Opacity', '')],
                                        default = 'diffuse')

    mask_id: EnumProperty( name = "Mask ID",
                                        description = "Mask ID",
                                        items = [
                                        ('primitive', 'Primitive', ''),
                                        ('material', 'Material', ''),
                                        #('sourceNode', 'Source Node', ''),
                                        ('instance', 'Instance', '')],
                                        #('geometryGroup', 'Geometry Group', '')],
                                        default = 'primitive')


class CoronaRenderPassProps(bpy.types.PropertyGroup):
    passes: CollectionProperty( type = CoronaRenderPasses,
                                        name = "Corona Render Passes",
                                        description = "")
    pass_index: IntProperty( name = "Pass Index",
                                        description = "",
                                        default = 0,
                                        min = 0,
                                        max = 30)

def register():
    bpy.utils.register_class( CoronaRenderPasses)
    bpy.utils.register_class( CoronaRenderPassProps)
    bpy.types.Scene.corona_passes = PointerProperty( type = CoronaRenderPassProps)
def unregister():
    bpy.utils.unregister_class( CoronaRenderPassProps)
    bpy.utils.unregister_class( CoronaRenderPasses)

