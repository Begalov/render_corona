#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Some code has been adapted or borrowed from various exporters, including those
# for NOX, Mitsuba, Luxrender, Sunflow, and appleseed. Many thanks to those developers
# for their ingenuity and elegant solutions.

bl_info = {
    "name": "Corona Render",
    "author": "Glen Blanchard, Joel Daniels, elindell, Francesc Juhe (and some code borrowed from Franz Beaune and Esteban Tovagliari's appleseed exporter)",
    "version": (9, 0, 47), # Make sure this matches the version in version.txt  Update both
    "blender": (2, 80, 0),
    "location": "Info Header (engine dropdown)",
    "description": "Corona Render integration",
    "warning": "Corona Renderer and this script are in alpha. Please contribute by reporting bugs to the issue tracker.",
    "wiki_url": "http://corona-renderer.com/wiki/blender2corona",
    "tracker_url": "https://bitbucket.org/coronablender/render_corona/issues?status=new&status=open",
    "category": "Render"}

if 'bpy' in locals():
    import importlib
    if "ui" in locals():
        importlib.reload(ui)
    if "export" in locals():
        importlib.reload(export)
    if "engine" in locals():
        importlib.reload(engine)
    if "util" in locals():
        importlib.reload(util)
    if "properties" in locals():
        importlib.reload(properties)
    if "operators" in locals():
        importlib.reload(operators)
    if "preferences" in locals():
        importlib.reload(preferences)

else:
    import bpy
    from . import ui
    from . import export
    from . import engine
    from . import util
    from . import properties
    from . import operators
    from . import preferences
    from . import changedetect

# updater ops import, all setup in this file
from . import addon_updater_ops

def registerHandlers():
    if engine.scene_loaded not in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(engine.scene_loaded)
    if engine.test_renderer_changed not in bpy.app.handlers.depsgraph_update_post:
        bpy.app.handlers.depsgraph_update_post.append(engine.test_renderer_changed)

def unregisterHandlers():
    if engine.scene_loaded in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(engine.scene_loaded)
    if engine.test_renderer_changed in bpy.app.handlers.depsgraph_update_post:
        bpy.app.handlers.depsgraph_update_post.remove(engine.test_renderer_changed)

#------------------------------------
# Register the script
#------------------------------------
submodules = (
    properties,
    ui,
    operators,
    preferences,
    changedetect,
    engine
    )

def register():
    '''Register the module'''

    # addon updater code and configurations
    # in case of broken version, try to register the updater first
    # so that users can revert back to a working version
    # addon_updater_ops.register(bl_info)

    print( "--------------------------------------------")
    # util.CrnUpdate( "Bitbucket version %s" % util.get_bitbucket_version())
    util.CrnUpdate( "Starting add-on, %s" % util.get_version_string())

    for sub in submodules:
        sub.register()
    # properties.register()
    # ui.register()
    # operators.register()
    # preferences.register()
    # changedetect.register()
    # engine.register()

    registerHandlers()
    util.CrnUpdate( "Add-on started")
    print( "--------------------------------------------")

def unregister():
    '''Unregister the module'''

    # addon updater unregister
    # addon_updater_ops.unregister()

    print( "---------------------------------")
    util.CrnUpdate("Disabling add-on")
    unregisterHandlers()

    for sub in reversed(submodules):
        sub.unregister()
    # properties.unregister()
    # ui.unregister()
    # operators.unregister()
    # preferences.unregister()
    # changedetect.unregister()
    # engine.unregister()

    util.CrnUpdate("Add-on disabled")
    print( "---------------------------------")

if __name__ == "__main__":
    register()