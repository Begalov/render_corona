import bpy
import bl_operators
from mathutils  import Color
from ..util     import *
from ..cycles   import migrate_cycles_material
from ..engine   import migrate_corona_use_nodes

#----------------------------------------
# A converter for Corona materials from BI
class CoronaMatConvert(bpy.types.Operator):
    bl_label = "Convert Blender Material to Corona Material"
    bl_idname = "corona.mat_convert"
    bl_description = "Convert Blender internal materials to Corona materials"

    scene_wide: bpy.props.BoolProperty( name = "", description = "", default = False)

    def execute(self, context):
        scene = context.scene

        diff_map = []
        transl_map = []
        alpha_map = []
        spec_map = []
        spec_col_map = []
        emit_map = []
        normal_map = []

        if self.scene_wide:
            materials = [mat for mat in bpy.data.materials]
        else:
            materials = [context.object.active_material]

        for mat in materials:
            diff_map.clear()
            transl_map.clear()
            alpha_map.clear()
            spec_map.clear()
            spec_col_map.clear()
            emit_map.clear()
            normal_map.clear()

            crn_mat = mat.corona
            for tex in mat.texture_slots:
                if tex:
                    if is_uv_img(tex.name):
                        if tex.use_map_color_diffuse:
                            diff_map.append(tex.name)
                        if tex.use_map_color_spec or tex.use_map_mirror:
                            spec_col_map.append(tex.name)
                        if tex.use_map_specular or tex.use_map_raymir:
                            spec_map.append(tex.name)
                        if tex.use_map_alpha:
                            alpha_map.append(tex.name)
                        if tex.use_map_emit:
                            emit_map.append(tex.name)
                        if tex.use_map_translucency:
                            transl_map.append(tex.name)
                        if tex.use_map_normal:
                            normal_map.append(tex.name)

            #Diffuse
            # Have to save BI diffuse_color before touching Corona Kd
            # or BI diffuse_color will be updated to Kd as Kd is being modified.
            diff_color = mat.diffuse_color
            crn_mat.kd = diff_color
            if len(diff_map) > 0:
                crn_mat.use_map_kd = True
                crn_mat.map_kd.texture = diff_map[0]

            #Translucency
            if mat.translucency > 0.0:
                crn_mat.translucency = mat.diffuse_color * mat.translucency
                if len(transl_map) > 0:
                    crn_mat.use_map_translucency = True
                    crn_mat.map_translucency.texture = transl_map[0]

            #Specular
            crn_mat.ks = mat.specular_color
            if mat.specular_intensity > 0.0:
                crn_mat.ns = mat.specular_intensity
                if mat.specular_shader == 'WARDISO':
                    #Convert to Corona's range
                    crn_mat.reflect_glossiness = abs(mat.specular_slope / 0.4 - 1)
                elif mat.specular_shader == 'TOON':
                    #The smaller the size, the glossier it is
                    size = abs(mat.specular_toon_size / 1.53 - 1)
                    #Same goes for smooth value
                    smooth = abs(mat.specular_toon_smooth - 1)
                    crn_mat.reflect_glossiness = size * smooth
                else:
                    crn_mat.reflect_glossiness = (mat.specular_hardness / 511)
                if len(spec_col_map) > 0:
                    crn_mat.use_map_ks = True
                    crn_mat.map_ks.texture = spec_col_map[0]
                if len(spec_map) > 0:
                    crn_mat.use_map_ns = True
                    crn_mat.map_ns.texture = spec_map[0]

            else:
                crn_mat.ns = 0.0
                crn_mat.reflect_glossiness = 0.0

            #Transparency
            if mat.use_transparency:
                crn_mat.opacity = Color((mat.alpha, mat.alpha, mat.alpha))
                if mat.transparency_method == 'RAYTRACE':
                    crn_mat.refract_caustics = True
                    crn_mat.refract_thin = False
                    crn_mat.ni = mat.raytrace_transparency.ior
                    crn_mat.refract_glossiness = mat.raytrace_transparency.gloss_factor
                    crn_mat.absorption_color = (mat.diffuse_color * mat.raytrace_transparency.filter)[:]
                    crn_mat.absorption_distance = 100 - mat.raytrace_transparency.depth_max    # Invert
                elif mat.transparency_method == 'Z_TRANSPARENCY':
                    crn_mat.refract_thin = True
                    crn_mat.refract_caustics = False
                    crn_mat.ni = 1.0
                    crn_mat.refract_glossiness = 1.0
                crn_mat.refract_level = mat.raytrace_transparency.fresnel / 5

            #Mirror - override BI specular values
            if mat.raytrace_mirror.use:
                if mat.raytrace_mirror.reflect_factor > 0.0:
                    crn_mat.ns = mat.raytrace_mirror.reflect_factor
                    crn_mat.reflect_glossiness = mat.raytrace_mirror.gloss_factor
                    crn_mat.reflect_fresnel = mat.raytrace_mirror.fresnel
                    crn_mat.ks = mat.mirror_color
                if len(spec_col_map) > 0:
                    crn_mat.use_map_ks = True
                    crn_mat.map_ks.texture = spec_col_map[0]
                if len(spec_map) > 0:
                    crn_mat.use_map_ns = True
                    crn_mat.map_ns.texture = spec_map[0]

            if len(normal_map) > 0:
                crn_mat.use_map_normal = True
                crn_mat.map_normal.texture = normal_map[0]

            #Emit
            if mat.emit > 0.0:
                crn_mat.mtl_type = 'coronalightmtl'
                crn_mat.ke = mat.diffuse_color
                crn_mat.emission_mult = mat.emit
                if len(emit_map) > 0:
                    crn_mat.use_map_ke = True
                    crn_mat.map_ke.texture = emit_map[0]

            #Shadeless
            if mat.use_shadeless:
                crn_mat.mtl_type = 'coronalightmtl'
                crn_mat.ke = mat.diffuse_color

        return {'FINISHED'}


# An operator for creating a Corona node tree for the current material
#    and linking the material to it
class CoronaNewNodeTree( bpy.types.Operator):
    bl_idname = "corona.add_material_nodetree"
    bl_label = "Add Corona Material Node Tree"
    bl_description = "Create a Corona material node tree and link it to the current material"
    
    def _get_refract_mode( self, crn_mat):
        if crn_mat.refract_caustics:
            return 'caustics'
        if crn_mat.refract_thin:
            return 'thin'
        else:
            return 'hybrid'
            
    def execute( self, context):
        material = context.object.active_material
        crn_mat = material.corona
        material.use_nodes = True
        material.corona.use_nodes = True
        nodetree = material.node_tree #bpy.data.node_groups.new( '%s Corona Nodetree' % material.name, 'CoronaNodeTree')
        #nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        node.location = -175, 0
        node.width = 256
        vals_dict = {'Diffuse Level' : crn_mat.diffuse_level, 
                     'Diffuse Color' : crn_mat.kd, 
                     'Translucency Level' : crn_mat.translucency_level, 
                     'Translucency Color' : crn_mat.translucency, 
                     'Reflect Level' : crn_mat.ns, 
                     'Reflect Color' : crn_mat.ks, 
                     'Reflect Gloss' : crn_mat.reflect_glossiness, 
                     'Fresnel' : crn_mat.reflect_fresnel,
                     'Anisotropy' : crn_mat.anisotropy, 
                     'Anisotropy Rotation' : crn_mat.aniso_rotation,
                     'Refract Level' : crn_mat.refract_level, 
                     'Refract Color' : crn_mat.refract,
                     'Refract Gloss' : crn_mat.refract_glossiness, 
                     'Refract IOR' : crn_mat.ni, 
                     'Refract Mode' : self._get_refract_mode( crn_mat),
                     'Absorption Color' : crn_mat.absorption_color,
                     'Absorption Distance' : crn_mat.absorption_distance,
                     'Scattering Color' : crn_mat.scattering_albedo,
                     'Scattering Direction' : crn_mat.mean_cosine,
                     'Opacity' : crn_mat.opacity,
                     'Emission Color' : crn_mat.ke,
                     'Directionality' : crn_mat.emission_gloss,
                     'Multiplier' : crn_mat.emission_mult}
        #More or less replicate the existing material in the node tree, without textures.
        for k in vals_dict.keys():
            node.inputs[k].default_value = vals_dict[k]
        node.as_portal = crn_mat.as_portal
        node.kelvin = crn_mat.kelvin
            
        output = nodetree.nodes.new('CoronaOutputNode')
        output.location = 200, 0
        nodetree.links.new(node.outputs[0], output.inputs[0])

        return {'FINISHED'}


class CoronaMigrateUseNodes( bpy.types.Operator):
    bl_idname = "corona.migrate_use_nodes"
    bl_label = "Migrate Use Nodes"
    bl_description = "For every material in this .blend file migrate the Corona use_nodes option.  Will detect if a CoronaOutputNode is in the material and set the use_nodes to True"

    def execute( self, context):
        migrate_corona_use_nodes()
        return {'FINISHED'}

class CoronaNewChocofurPlastic( bpy.types.Operator):
    bl_idname = "corona.add_chocofur_plastic"
    bl_label = "Plastic"
    bl_description = "Create a Chocofur.com example plastic Corona material node tree and link it to the current material"

    def execute( self, context):
        material = context.object.active_material
        material.use_nodes = True
        material.corona.use_nodes = True
        nodetree = material.node_tree #bpy.data.node_groups.new( '%s Chocofur Plastic' % material.name, 'CoronaNodeTree')
        # nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        node.location = -100, 0
        node.inputs["Diffuse Color"].default_value = (0.787, 0.787, 0.787)
        node.inputs["Reflect Level"].default_value = 0.4
        node.inputs["Reflect Gloss"].default_value = 0.65
        node.inputs["Fresnel"].default_value = 2.5
        output = nodetree.nodes.new('CoronaOutputNode')
        output.location = 200, 0
        nodetree.links.new(node.outputs[0], output.inputs[0])
        # material.corona.node_tree = nodetree.name
        # material.corona.node_output = output.name
        return {'FINISHED'}

class CoronaNewChocofurMetal( bpy.types.Operator):
    bl_idname = "corona.add_chocofur_metal"
    bl_label = "Metallic"
    bl_description = "Create a Chocofur.com example metallic Corona material node tree and link it to the current material"

    def execute( self, context):
        material = context.object.active_material
        material.use_nodes = True
        material.corona.use_nodes = True
        nodetree = material.node_tree #bpy.data.node_groups.new( '%s Chocofur Metallic' % material.name, 'CoronaNodeTree')
        # nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        node.location = -100, 0
        node.inputs["Diffuse Level"].default_value = 0.4
        node.inputs["Diffuse Color"].default_value = (0.319, 0.319, 0.319)
        node.inputs["Reflect Level"].default_value = 0.8
        node.inputs["Reflect Gloss"].default_value = 0.5
        node.inputs["Fresnel"].default_value = 8.0
        output = nodetree.nodes.new('CoronaOutputNode')
        output.location = 200, 0
        nodetree.links.new(node.outputs[0], output.inputs[0])
        # material.corona.node_tree = nodetree.name
        # material.corona.node_output = output.name
        return {'FINISHED'}

class CoronaNewChocofurGlass( bpy.types.Operator):
    bl_idname = "corona.add_chocofur_glass"
    bl_label = "Glass"
    bl_description = "Create a Chocofur.com example glass Corona material node tree and link it to the current material"

    def execute( self, context):
        material = context.object.active_material
        material.use_nodes = True
        material.corona.use_nodes = True
        nodetree = material.node_tree #bpy.data.node_groups.new( '%s Chocofur Glass' % material.name, 'CoronaNodeTree')
        # nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        node.location = -100, 0
        node.inputs["Refract Level"].default_value = 1.0
        node.inputs["Reflect Level"].default_value = 0.75
        node.inputs["Fresnel"].default_value = 1.5
        output = nodetree.nodes.new('CoronaOutputNode')
        output.location = 200, 0
        nodetree.links.new(node.outputs[0], output.inputs[0])
        # material.corona.node_tree = nodetree.name
        # material.corona.node_output = output.name
        return {'FINISHED'}

class CoronaNewChocofurTranslucent( bpy.types.Operator):
    bl_idname = "corona.add_chocofur_trans"
    bl_label = "Translucent"
    bl_description = "Create a Chocofur.com example translucent Corona material node tree and link it to the current material"

    def execute( self, context):
        material = context.object.active_material
        material.use_nodes = True
        material.corona.use_nodes = True
        nodetree = material.node_tree # bpy.data.node_groups.new( '%s Chocofur Translucent' % material.name, 'CoronaNodeTree')
        # nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        node.location = -100, 0
        node.inputs["Translucency Level"].default_value = 1.0
        node.inputs["Translucency Color"].default_value = (1.0, 1.0, 1.0)
        node.inputs["Reflect Level"].default_value = 0.5
        node.inputs["Fresnel"].default_value = 2.0
        node.inputs["Reflect Gloss"].default_value = 0.5
        output = nodetree.nodes.new('CoronaOutputNode')
        output.location = 200, 0
        nodetree.links.new(node.outputs[0], output.inputs[0])
        # material.corona.node_tree = nodetree.name
        # material.corona.node_output = output.name
        return {'FINISHED'}


#----------------------------------------
# An operator for creating a Cycles-based node for Corona Ubershader
# Thanks to elindell for this
class CoronaUbershader( bpy.types.Operator):
    bl_idname = "corona.create_ubershader"
    bl_label = "Create Corona Ubershader"
    bl_description = "Create a Cycles-Corona interchangeable ubershader node network"

    def execute(self, context):
        object = context.active_object
        active_mat = context.active_object.active_material
        tree = active_mat.node_tree
        nodes = tree.nodes
        new = nodes.new
        scene = context.scene

        main_group = new('ShaderNodeGroup')
        main_node_group = bpy.data.node_groups.new('Corona Nodetree', 'ShaderNodeTree')
        main_group.node_tree = main_node_group
        main_group.width = 250

        new = main_group.node_tree.nodes.new

        #Group input
        input = new('NodeGroupInput')
        input.location = -2475, 312
        #First power node
        math_pwr = new('ShaderNodeMath')
        math_pwr.location = -2073, -304
        math_pwr.operation = "POWER"
        math_pwr.inputs[1].default_value = 0.50


        math_pwr.inputs[0].default_value = 1.0
        #First math multiply node
        math_mlt = new('ShaderNodeMath')
        math_mlt.location = -1915, -304
        math_mlt.operation = 'MULTIPLY'
        math_mlt.inputs[1].default_value = 0.30
        #First math add node
        math_add = new('ShaderNodeMath')
        math_add.location = -1758, -304
        math_add.operation = 'ADD'
        math_add.inputs[1].default_value = 0.70
        #First subtract node
        math_sub1 = new('ShaderNodeMath')
        math_sub1.location = -1608, -304
        math_sub1.operation = 'SUBTRACT'
        math_sub1.inputs[0].default_value = 1.0
        #Second subtract node - on top
        math_sub2 = new('ShaderNodeMath')
        math_sub2.location = -1643, 678
        math_sub2.operation = 'SUBTRACT'
        math_sub2.inputs[0].default_value = 1.0
        math_sub2.inputs[1].default_value = 0.0
        #First fresnel
        fresnel1 = new('ShaderNodeFresnel')
        fresnel1.location = -1346, -574
        fresnel1.inputs[0].default_value = 1.45
        #First glossy bsdf - bottom
        glossy1 = new('ShaderNodeBsdfGlossy')
        glossy1.location = -1346, -662
        glossy1.distribution = 'BECKMANN'
        glossy1.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        glossy1.inputs[1].default_value = 0.00
        #Second glossy bsdf - bottom
        glossy2 = new('ShaderNodeBsdfGlossy')
        glossy2.location = -1346, -826
        glossy2.distribution = 'BECKMANN'
        #First diffuse bsdf
        diff1 = new('ShaderNodeBsdfDiffuse')
        diff1.location = -1077, 688
        diff1.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        diff1.inputs[1].default_value = 0.0
        #Second diffuse bsdf
        diff2 = new('ShaderNodeBsdfDiffuse')
        diff2.location = -1077, 376
        #First mix shade - bottom
        mix_sh1 = new('ShaderNodeMixShader')
        mix_sh1.location = -1077, -376
        #Second mix shader - top
        mix_sh2 = new('ShaderNodeMixShader')
        mix_sh2.location = -821, 556
        #First translucent shader
        transl = new('ShaderNodeBsdfTranslucent')
        transl.location = -821, 275
        #Third math sub
        math_sub3 = new('ShaderNodeMath')
        math_sub3.location = -806, -89
        math_sub3.operation = 'SUBTRACT'
        math_sub3.inputs[0].default_value = 1.0
        #First add shader
        add_sh1 = new('ShaderNodeAddShader')
        add_sh1.location = -821, -523
        #Third mix shader
        mix_sh3 = new('ShaderNodeMixShader')
        mix_sh3.location = -518, 426
        mix_sh3.inputs[0].default_value = 0.0
        #Refraction BSDF
        refr = new('ShaderNodeBsdfRefraction')
        refr.location = -518, -214
        #Fourt mix shader
        mix_sh4 = new('ShaderNodeMixShader')
        mix_sh4.location = -273, 508
        #Second add shader
        add_sh2 = new('ShaderNodeAddShader')
        add_sh2.location = -273, -23
        #Fourth math sub
        math_sub4 = new('ShaderNodeMath')
        math_sub4.location = -258, -341
        math_sub4.inputs[1].default_value = 1.0
        math_sub4.operation = 'SUBTRACT'
        #Fifth mix shader
        mix_sh5 = new('ShaderNodeMixShader')
        mix_sh5.location = -40, 278
        #Emission shader
        emission = new('ShaderNodeEmission')
        emission.location = -40, -52
        #Geometry
        geom = new('ShaderNodeNewGeometry')
        geom.location = 213, 634
        #Third diffuse bsdf
        diff3 = new('ShaderNodeBsdfDiffuse')
        diff3.location = 198, 405
        diff3.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        #Sixth mix shader
        mix_sh6 = new('ShaderNodeMixShader')
        mix_sh6.location = 198, 166
        mix_sh6.inputs[0].default_value = 0.0
        #Transparent bsdf
        transp = new('ShaderNodeBsdfTransparent')
        transp.location = 198, -109
        #Seventh mix shader
        mix_sh7 = new('ShaderNodeMixShader')
        mix_sh7.location = 477, 434
        #Eighth mix shader
        mix_sh8 = new('ShaderNodeMixShader')
        mix_sh8.location = 477, 16
        #Group out
        output = new('NodeGroupOutput')
        output.location = 773, 136

        #Connect the nodes
        new_link = nodes[main_group.name].node_tree.links.new

        new_link(input.outputs[0], math_sub2.inputs[0])

        new_link(input.outputs[1], diff2.inputs[0])
        new_link(input.outputs[2], mix_sh3.inputs[0])
        #Translucency node
        new_link(input.outputs[3], transl.inputs[0])

        new_link(input.outputs[4], mix_sh4.inputs[0])
        new_link(mix_sh3.outputs[0], mix_sh4.inputs[1])
        new_link(add_sh1.outputs[0], mix_sh4.inputs[2])

        new_link(input.outputs[5], glossy2.inputs[0])

        #Connect math nodes on bottom
        new_link(input.outputs[6], math_pwr.inputs[0])
        new_link(math_pwr.outputs[0], math_mlt.inputs[0])
        new_link(math_mlt.outputs[0], math_add.inputs[0])
        new_link(math_add.outputs[0], math_sub1.inputs[1])
        new_link(math_sub1.outputs[0], glossy2.inputs[1])

        new_link(input.outputs[7], fresnel1.inputs[0])
        new_link(fresnel1.outputs[0], mix_sh1.inputs[0])
        new_link(glossy1.outputs[0], mix_sh1.inputs[1])
        new_link(glossy2.outputs[0], mix_sh1.inputs[2])
        new_link(mix_sh1.outputs[0], add_sh1.inputs[1])
        new_link(mix_sh3.outputs[0], add_sh1.inputs[0])

        #Mix Shader 2 inputs
        new_link(diff1.outputs[0], mix_sh2.inputs[1])
        new_link(diff2.outputs[0], mix_sh2.inputs[2])
        new_link(math_sub2.outputs[0], mix_sh2.inputs[0])
        new_link(mix_sh2.outputs[0], mix_sh3.inputs[1])
        new_link(transl.outputs[0], mix_sh3.inputs[2])


        new_link(input.outputs[8], math_sub2.inputs[1])

        new_link(input.outputs[9], refr.inputs[0])


        new_link(input.outputs[10], math_sub3.inputs[1])
        new_link(math_sub3.outputs[0], refr.inputs[1])

        new_link(input.outputs[11], refr.inputs[2])
        new_link(refr.outputs[0], add_sh2.inputs[1])
        new_link(mix_sh4.outputs[0], add_sh2.inputs[0])
        new_link(add_sh2.outputs[0], mix_sh5.inputs[2])
        new_link(mix_sh4.outputs[0], mix_sh5.inputs[1])
        new_link(input.outputs[8], mix_sh5.inputs[0])

        new_link(input.outputs[12], mix_sh6.inputs[0])
        new_link(mix_sh5.outputs[0], mix_sh6.inputs[1])
        new_link(mix_sh7.outputs[0], mix_sh6.inputs[2])
        new_link(diff3.outputs[0], mix_sh7.inputs[2])
        new_link(geom.outputs[6], mix_sh7.inputs[0])
        new_link(emission.outputs[0], mix_sh7.inputs[1])

        new_link(input.outputs[13], emission.inputs[0])
        new_link(input.outputs[14], emission.inputs[1])

        new_link(input.outputs[15], math_sub4.inputs[1])

        new_link(input.outputs[16], transp.inputs[0])
        new_link(mix_sh6.outputs[0], mix_sh8.inputs[1])
        new_link(transp.outputs[0], mix_sh8.inputs[2])
        new_link(math_sub4.outputs[0], mix_sh8.inputs[0])

        new_link(mix_sh8.outputs[0], output.inputs[0])

        bpy.data.node_groups[main_node_group.name].inputs[0].name = 'Diffuse'
        bpy.data.node_groups[main_node_group.name].inputs[0].default_value = 1.0
        bpy.data.node_groups[main_node_group.name].inputs[1].name = 'Diffuse Color'
        bpy.data.node_groups[main_node_group.name].inputs[2].name = 'Translucency'
        bpy.data.node_groups[main_node_group.name].inputs[2].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[3].name = 'Translucency Color'
        bpy.data.node_groups[main_node_group.name].inputs[4].name = 'Reflection'
        bpy.data.node_groups[main_node_group.name].inputs[4].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[5].name = 'Reflection Color'
        bpy.data.node_groups[main_node_group.name].inputs[6].name = 'Reflection Glossiness'
        bpy.data.node_groups[main_node_group.name].inputs[7].name = 'Reflection IOR'
        bpy.data.node_groups[main_node_group.name].inputs[8].name = 'Refraction'
        bpy.data.node_groups[main_node_group.name].inputs[8].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[9].name = 'Refraction Color'
        bpy.data.node_groups[main_node_group.name].inputs[10].name = 'Refraction Glossiness'
        bpy.data.node_groups[main_node_group.name].inputs[11].name = 'Refraction IOR'
        bpy.data.node_groups[main_node_group.name].inputs[12].name = 'Emission'
        bpy.data.node_groups[main_node_group.name].inputs[12].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[13].name = 'Emission Color'
        bpy.data.node_groups[main_node_group.name].inputs[14].name = 'Emission Strength'
        bpy.data.node_groups[main_node_group.name].inputs[14].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[15].name = 'Opacity'
        bpy.data.node_groups[main_node_group.name].inputs[15].default_value = 1.0
        bpy.data.node_groups[main_node_group.name].inputs[16].name = 'Opacity Color'

        return {'FINISHED'}

class CoronaAddPreset( bl_operators.presets.AddPresetBase, bpy.types.Operator):
    bl_idname = "corona.add_preset"
    bl_label = ""
    preset_menu = "MATERIAL_MT_Corona_presets"
    
    preset_defines = ["crn_mat = bpy.context.object.active_material.corona"]
    preset_subdir = "corona/material"
    preset_values = ["crn_mat.mtl_type",
                     "crn_mat.diffuse_level",
                     "crn_mat.kd",
                     "crn_mat.translucency_level",
                     "crn_mat.translucency",
                     "crn_mat.ns",
                     "crn_mat.ks",
                     "crn_mat.reflect_glossiness",
                     "crn_mat.reflect_fresnel",
                     "crn_mat.anisotropy",
                     "crn_mat.aniso_rotation",
                     "crn_mat.refract_level",
                     "crn_mat.refract",
                     "crn_mat.refract_glossiness",
                     "crn_mat.refract_thin",
                     "crn_mat.refract_caustics",
                     "crn_mat.absorption_color",
                     "crn_mat.absorption_distance",
                     "crn_mat.scattering_albedo",
                     "crn_mat.mean_cosine",
                     "crn_mat.opacity",
                     "crn_mat.emission_mult",
                     "crn_mat.ke",
                     "crn_mat.emission_gloss",
                     "crn_mat.as_portal",
                     "crn_mat.use_lightmix",
                     "crn_mat.lightmix_group",
                     "crn_mat.kelvin",
                     "crn_mat.ies_profile",
                     "crn_mat.keep_sharp",
                     "crn_mat.ies_translate[0]",
                     "crn_mat.ies_translate[1]",
                     "crn_mat.ies_translate[2]",
                     "crn_mat.ies_rotate[0]",
                     "crn_mat.ies_rotate[1]",
                     "crn_mat.ies_rotate[2]",
                     "crn_mat.ies_scale[0]",
                     "crn_mat.ies_scale[1]",
                     "crn_mat.ies_scale[2]"]
           
class CoronaConvertCyclesToCorona( bpy.types.Operator):
    bl_idname = "corona.convert_cycles_to_corona"
    bl_label = "Convert To Corona"
    bl_description = "Create a node layout and use nodes that match Cycles as best as we can"

    def execute(self, context):
        object = context.active_object
        active_mat = object.active_material
        active_mat.corona.use_nodes = True
        migrate_cycles_material(active_mat)
        return {'FINISHED'}

classes = (
    CoronaAddPreset,
    CoronaConvertCyclesToCorona,
    CoronaMatConvert,
    CoronaMigrateUseNodes,
    CoronaNewChocofurGlass,
    CoronaNewChocofurMetal,
    CoronaNewChocofurPlastic,
    CoronaNewChocofurTranslucent,
    CoronaNewNodeTree,
    CoronaUbershader
    )

def register():
    for cls in classes:
        bpy.utils.register_class( cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class( cls)
