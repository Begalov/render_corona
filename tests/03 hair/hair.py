import unittest
import bpy
import render_corona
from render_corona.tests import *

class TestBasic(unittest.TestCase):
    # def test_export_scene(self):

    #     import cProfile
    #     cProfile.run("import bpy; bpy.ops.corona.export_scene()", ".tmp/blender_scene.prof")

    #     import pstats
    #     p = pstats.Stats(".tmp/blender_scene.prof")
    #     p.sort_stats("cumulative").print_stats(20)

    def test_export_objects(self):

        render_corona.util.MultiThreaded = False
        render_corona.util.EnableDebug = True
        import cProfile
        cProfile.run("import bpy; bpy.ops.corona.export()", ".tmp/blender_objects.prof")

        import pstats
        p = pstats.Stats(".tmp/blender_objects.prof")
        p.sort_stats("cumulative").print_stats(30)


    # def test_render(self):
    #     bpy.ops.render.render()
    #     # The image is as expected
    #     self.assertTrue(CompareImg('tests/02 uvmaps/Scene1.png', '.tmp/render/Scene1.png', 5))

    #     # The cube is as expected
    #     self.assertTrue(CompareFiles('tests/02 uvmaps/meshes/Cube.cgeo', '.tmp/meshes/Cube.cgeo'))

    #     # The cube.001 is as expected
    #     self.assertTrue(CompareFiles('tests/02 uvmaps/meshes/Cube.001.cgeo', '.tmp/meshes/Cube.001.cgeo'))

RunTests(TestBasic)