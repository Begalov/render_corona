import bpy

#---------------------------------------
# Render passes UIList
#---------------------------------------
class CORONA_RENDER_UL_Pass_slots(bpy.types.UIList):
    def draw_item( self, context, layout, data, item, icon, active_data, active_propname, index):
        PASS = item
        pass_type = PASS.render_pass

        if 'DEFAULT' in self.layout_type:
            layout.label( text = PASS.name + "  |  " + pass_type, translate=False, icon_value=icon)

#---------------------------------------
# Render passes UI
#---------------------------------------
class CORONA_RENDER_PT_RenderPasses( bpy.types.Panel):
    bl_label = "Corona Render Passes"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render_layer"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        corona_passes = scene.corona_passes

        layout.label( "Render Passes:", icon = 'RENDERLAYERS')

        row = layout.row()
        row.template_list( "RENDER_UL_Pass_slots", "corona_render_passes", corona_passes, "passes", corona_passes, "pass_index")

        row = layout.row(align=True)
        row.operator( "corona.add_renderpass", icon = "ZOOMIN")
        row.operator( "corona.remove_renderpass", icon = "ZOOMOUT")

        if corona_passes.passes:
            current_pass = corona_passes.passes[corona_passes.pass_index]
            layout.prop( current_pass, "render_pass")
            layout.prop( current_pass, "name", text = "Pass Name")
            if current_pass.render_pass == 'ZDepth':
                row = layout.row( align = True)
                row.prop( current_pass, "z_min")
                row.prop( current_pass, "z_max")

            if current_pass.render_pass == 'Components':
                layout.label("Direct:")
                box = layout.box()
                row = box.row()
                row.prop(current_pass, "diffuseDirect")
                row.prop(current_pass, "reflectDirect")
                row = box.row()
                row.prop(current_pass, "refractDirect")
                row.prop(current_pass, "translucencyDirect")
                row = box.row()
                row.prop(current_pass, "volumetricDirect")
                # row.prop(current_pass, "translucencyDirect")

                layout.label("Indirect:")
                box = layout.box()
                row = box.row()
                row.prop(current_pass, "diffuseIndirect")
                row.prop(current_pass, "reflectIndirect")
                row = box.row()
                row.prop(current_pass, "refractIndirect")
                row.prop(current_pass, "translucencyIndirect")

                row = box.row()
                row.prop(current_pass, "volumetricIndirect")

                layout.prop(current_pass, "emission")

            if current_pass.render_pass == 'RawComponent':
                layout.prop(current_pass, "components")

            if current_pass.render_pass == 'Normals':
                layout.prop(current_pass, "normals")

            if current_pass.render_pass == 'SourceColor':
                layout.prop(current_pass, "source_color")

            if current_pass.render_pass == 'Id':
                layout.prop(current_pass, "mask_id")

            if current_pass.render_pass == 'Velocity':
                layout.prop(current_pass, "cameraWorldMode")
                layout.prop(current_pass, "multiplier")
                layout.prop(current_pass, "offset")

            if current_pass.render_pass == 'SamplingFocus':
                layout.prop(current_pass, "multiplier")
                layout.prop(current_pass, "relative")

def register():
    bpy.utils.register_class( CORONA_RENDER_UL_Pass_slots)
    bpy.utils.register_class( CORONA_RENDER_PT_RenderPasses)
def unregister():
    bpy.utils.unregister_class( CORONA_RENDER_UL_Pass_slots)
    bpy.utils.unregister_class( CORONA_RENDER_PT_RenderPasses)
