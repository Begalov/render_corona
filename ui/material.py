import bpy
from ..util             import debug, getOutputNode

def node_tree_selector_draw(layout, mat, output_types):
    # try:
    #     layout.prop_search( mat, "node_tree", bpy.data, "node_groups")
    # except:
    #     return False

    node = getOutputNode(mat)
    if not node:
        layout.operator('corona.convert_cycles_to_corona', icon='NODETREE')
        layout.operator('corona.add_material_nodetree', icon='NODETREE')

        layout.separator()
        layout.label(text= "Chocofur.com sample materials", icon='NODETREE')
        row = layout.row()
        row.operator('corona.add_chocofur_plastic')
        row.operator('corona.add_chocofur_metal')
        row = layout.row()
        row.operator('corona.add_chocofur_glass')
        row.operator('corona.add_chocofur_trans')
        return False
    return True

#---------------------------------------
# Material preview UI
#---------------------------------------
class CORONA_RENDER_PT_MaterialPreview( bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'

    bl_context = "material"
    bl_label = "Preview"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        return context.scene.render.engine in cls.COMPAT_ENGINES and context.object is not None and context.object.active_material is not None

    def draw( self, context):
        # self.layout.template_preview( context.material, show_buttons=False)
        layout = self.layout
        scene = context.scene
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        row = layout.row()
        row.template_preview( context.material, show_buttons=False)
        layout.prop( crn_mat, "preview_quality")
        # layout.operator_context = 'EXEC_REGION_PREVIEW'
        layout.operator("corona.big_preview")


#---------------------------------------
# Material settings UI
#---------------------------------------
class CORONA_RENDER_PT_CMSMtlDiffuse ( bpy.types.Panel):
    bl_label = 'Diffuse'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # Diffuse settings.
        # Kd
        # layout.label( "Diffuse:")
        box = layout.box()

        split = box.split( factor = 0.7)
        col = split.column()
        col.prop( crn_mat, "diffuse_level", text = "Diffuse Level")

        split = split.split( factor = 0.5)
        col = split.column()
        col.prop( crn_mat, "kd", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_kd", text = "T", toggle = True)
        if crn_mat.use_map_kd:
            # box.prop( crn_mat.map_kd, "show_settings")
            # if crn_mat.map_kd.show_settings:
            box.prop_search( crn_mat.map_kd,
                            "texture",
                            context.blend_data,
                            "textures",
                            text = "Texture")
            # box.template_image( crn_mat.map_kd,
            #                 "texture",
            #                 crn_mat.image_user)
            box.prop( crn_mat.map_kd, "intensity")
            row = box.row( align = True)
            row.prop( crn_mat.map_kd, "uOffset")
            row.prop( crn_mat.map_kd, "vOffset")
            row = box.row( align = True)
            row.prop( crn_mat.map_kd, "uScaling")
            row.prop( crn_mat.map_kd, "vScaling")

        # Translucency
        split = box.split( factor = 0.85)
        col = split.column()

        col.prop( crn_mat, "translucency_level", text = "Translucency Level")
        if crn_mat.use_map_translucency_level:
            box.prop( crn_mat.map_translucency_level, "show_settings")
            if crn_mat.map_translucency_level.show_settings:
                box.prop_search( crn_mat.map_translucency_level,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_translucency_level, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency_level, "uOffset")
                row.prop( crn_mat.map_translucency_level, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency_level, "uScaling")
                row.prop( crn_mat.map_translucency_level, "vScaling")

        col = split.column()
        col.prop( crn_mat, "use_map_translucency_level", text = "T", toggle = True)

        split = box.split( factor = 0.85)
        col = split.column()

        col.prop( crn_mat, "translucency", text = "")
        if crn_mat.use_map_translucency:
            box.prop( crn_mat.map_translucency, "show_settings")
            if crn_mat.map_translucency.show_settings:
                box.prop_search( crn_mat.map_translucency,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_translucency, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency, "uOffset")
                row.prop( crn_mat.map_translucency, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency, "uScaling")
                row.prop( crn_mat.map_translucency, "vScaling")

        col = split.column()
        col.prop( crn_mat, "use_map_translucency", text = "T", toggle = True)

class CORONA_RENDER_PT_CMSMtlReflection ( bpy.types.Panel):
    bl_label = 'Reflection'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        # Reflection settings.
        box = layout.box()
        # Ns
        split = box.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "ns", text = "Reflection Level")

        col = split.column()
        col.prop( crn_mat, "use_map_ns", text = "T", toggle = True)
        if crn_mat.use_map_ns:
            box.prop( crn_mat.map_ns, "show_settings")
            if crn_mat.map_ns.show_settings:
                box.prop_search( crn_mat.map_ns,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_ns, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_ns, "uOffset")
                row.prop( crn_mat.map_ns, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_ns, "uScaling")
                row.prop( crn_mat.map_ns, "vScaling")

        # Ks
        split = box.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "ks", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_ks", text = "T", toggle = True)
        if crn_mat.use_map_ks:
            box.prop( crn_mat.map_ks, "show_settings")
            if crn_mat.map_ks.show_settings:
                box.prop_search( crn_mat.map_ks,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_ks, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks, "uOffset")
                row.prop( crn_mat.map_ks, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks, "uScaling")
                row.prop( crn_mat.map_ks, "vScaling")

        # Reflect glossiness
        split = box.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "reflect_glossiness")

        col = split.column()
        col.prop( crn_mat, "use_map_ks_gloss", text = "T", toggle = True)
        if crn_mat.use_map_ks_gloss:
            box.prop( crn_mat.map_ks_gloss, "show_settings")
            if crn_mat.map_ks_gloss.show_settings:
                box.prop_search( crn_mat.map_ks_gloss,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_ks_gloss, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks_gloss, "uOffset")
                row.prop( crn_mat.map_ks_gloss, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks_gloss, "uScaling")
                row.prop( crn_mat.map_ks_gloss, "vScaling")

        # Reflect fresnel
        box.prop( crn_mat, "reflect_fresnel")
        # Anisotropy
        split = box.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "anisotropy", text = "Anisotropy")

        col = split.column()
        col.prop( crn_mat, "use_map_aniso", text = "T", toggle = True)
        if crn_mat.use_map_aniso:
            box.prop( crn_mat.map_aniso, "show_settings")
            if crn_mat.map_aniso.show_settings:
                box.prop_search( crn_mat.map_aniso,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_aniso, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso, "uOffset")
                row.prop( crn_mat.map_aniso, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso, "uScaling")
                row.prop( crn_mat.map_aniso, "vScaling")
        # Anisotropy rotation.
        split = box.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "aniso_rotation", text = "Anisotropy Rotation")

        col = split.column()
        col.prop( crn_mat, "use_map_aniso_rot", text = "T", toggle = True)
        if crn_mat.use_map_aniso_rot:
            box.prop( crn_mat.map_aniso_rot, "show_settings")
            if crn_mat.map_aniso_rot.show_settings:
                box.prop_search( crn_mat.map_aniso_rot,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_aniso_rot, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso_rot, "uOffset")
                row.prop( crn_mat.map_aniso_rot, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso_rot, "uScaling")
                row.prop( crn_mat.map_aniso_rot, "vScaling")

class CORONA_RENDER_PT_CMSMtlRefraction ( bpy.types.Panel):
    bl_label = 'Refraction'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes
    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # Refraction settings.
        box = layout.box()
        split = box.split( factor = 0.7)
        col = split.column()
        col.prop( crn_mat, "refract_level", text = "Refraction Level")

        split = split.split( factor = 0.5)
        col = split.column()
        col.prop( crn_mat, "refract", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_refract", text = "T", toggle = True)
        if crn_mat.use_map_refract:
            box.prop( crn_mat.map_refract, "show_settings")
            if crn_mat.map_refract.show_settings:
                box.prop_search( crn_mat.map_refract,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                box.prop( crn_mat.map_refract, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_refract, "uOffset")
                row.prop( crn_mat.map_refract, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_refract, "uScaling")
                row.prop( crn_mat.map_refract, "vScaling")

        # Refraction Gloss / IOR
        row = box.row( align = True)
        row.prop( crn_mat, "refract_glossiness")
        row.prop( crn_mat, "ni")

        split = box.split()
        col = split.column()
        # col.active = crn_mat.refract_thin == False
        col.prop(  crn_mat, "refract_caustics")

        col = split.column()
        # col.active = crn_mat.refract_caustics == False
        col.prop( crn_mat, "refract_thin")


class CORONA_RENDER_PT_CMSMtlVolume ( bpy.types.Panel):
    bl_label = 'Volumetric Absorption / Scattering'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        layout.label(text= "Absorption:")

        split = layout.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "absorption_color", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_absorption", text = "T", toggle = True)
        if crn_mat.use_map_absorption:
            layout.prop( crn_mat.map_absorption, "show_settings")
            if crn_mat.map_absorption.show_settings:
                layout.prop_search( crn_mat.map_absorption,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                layout.prop( crn_mat.map_absorption, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_absorption, "uOffset")
                row.prop( crn_mat.map_absorption, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_absorption, "uScaling")
                row.prop( crn_mat.map_absorption, "vScaling")

        layout.prop( crn_mat, "absorption_distance", text = "Absorption Distance")

        layout.label(text= "Scattering:")
        split = layout.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "scattering_albedo", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_scattering", text = "T", toggle = True)
        if crn_mat.use_map_scattering:
            layout.prop( crn_mat.map_scattering, "show_settings")
            if crn_mat.map_scattering.show_settings:
                layout.prop_search( crn_mat.map_scattering,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                layout.prop( crn_mat.map_scattering, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_scattering, "uOffset")
                row.prop( crn_mat.map_scattering, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_scattering, "uScaling")
                row.prop( crn_mat.map_scattering, "vScaling")

        layout.prop( crn_mat, "mean_cosine")


class CORONA_RENDER_PT_CMSMtlOpacity ( bpy.types.Panel):
    bl_label = 'Opacity'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        # Opacity
        layout.label(text= "Opacity:")
        split = layout.split( factor = 0.85)
        col = split.column()
        col.prop( crn_mat, "opacity", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_opacity", text = "T", toggle = True)
        if crn_mat.use_map_opacity:
            layout.prop( crn_mat.map_opacity, "show_settings")
            if crn_mat.map_opacity.show_settings:
                layout.prop_search( crn_mat.map_opacity,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                layout.prop( crn_mat.map_opacity, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_opacity, "uOffset")
                row.prop( crn_mat.map_opacity, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_opacity, "uScaling")
                row.prop( crn_mat.map_opacity, "vScaling")


class CORONA_RENDER_PT_CMSMtlBump ( bpy.types.Panel):
    bl_label = 'Bump'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes
    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        layout.prop( crn_mat, "use_map_bump", text = "Use Bump Texture", toggle = True)
        if crn_mat.use_map_bump:
           # layout.prop( crn_mat.map_bump, "show_settings")
           # if crn_mat.map_bump.show_settings:
            layout.prop_search( crn_mat.map_bump,
                           "texture",
                            context.blend_data,
                            "textures",
                           text = "Texture")
            layout.prop( crn_mat.map_bump, "intensity")
            row = layout.row( align = True)
            row.prop( crn_mat.map_bump, "uOffset")
            row.prop( crn_mat.map_bump, "vOffset")
            row = layout.row( align = True)
            row.prop( crn_mat.map_bump, "uScaling")
            row.prop( crn_mat.map_bump, "vScaling")


class CORONA_RENDER_PT_CMSMtlEmission ( bpy.types.Panel):
    bl_label = 'Emission'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not material.corona.use_nodes

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        layout.prop( crn_mat, "kelvin", slider=True)
        # Emission settings
        split = layout.split( factor = 0.7)
        col = split.column()
        col.prop( crn_mat, "emission_mult")

        split = split.split( factor = 0.5)
        col = split.column()
        col.prop( crn_mat, "ke", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_ke", text = "T", toggle = True)
        if crn_mat.use_map_ke:
            layout.prop( crn_mat.map_ke, "show_settings")
            if crn_mat.map_ke.show_settings:
                layout.prop_search( crn_mat.map_ke,
                                "texture",
                                context.blend_data,
                                "textures",
                                text = "Texture")
                layout.prop( crn_mat.map_ke, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_ke, "uOffset")
                row.prop( crn_mat.map_ke, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_ke, "uScaling")
                row.prop( crn_mat.map_ke, "vScaling")

        layout.separator()

        layout.prop( crn_mat, "emission_gloss", text = "Directionality")
        layout.prop( crn_mat, "self_illumination")
        layout.prop( crn_mat, "both_sides")
        layout.prop( crn_mat, "shadowcatcher_illuminator")

class CORONA_RENDER_PT_CMSMtlIES ( bpy.types.Panel):
    bl_label = 'IES'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        use_nodes = material.corona.use_nodes if material is not None else None

        return renderer.engine == 'CORONA' and \
            obj.type in ['MESH', 'SURFACE', 'META'] and \
            material is not None and \
            not use_nodes and \
            material.corona.mtl_type in ['coronalightmtl']

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # IES settings.
        layout.prop( crn_mat, "ies_profile")
        layout.prop( crn_mat, "keep_sharp", text = "Keep Profile Sharp")
        # layout.prop( crn_mat, "ies_translate")
        layout.prop( crn_mat, "ies_rotate")
        # layout.prop( crn_mat, "ies_scale")

#---------------------------------------
# Material Preset Menus
#---------------------------------------
class MATERIAL_MT_Corona_presets( bpy.types.Menu):
    bl_label = "Presets"
    preset_subdir = "corona/material"
    preset_operator = "script.execute_preset"
    draw = bpy.types.Menu.draw_preset

#---------------------------------------
# Material settings UI
#---------------------------------------
class CORONA_RENDER_PT_MaterialShading( bpy.types.Panel):
    bl_label = 'Corona Surface Shading'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE', 'META'] and context.object.active_material is not None


    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        #layout.prop( crn_mat, "preview_quality")

        # layout.label( "Blender To Corona Material Conversion:")
        # row = layout.row( align = True)
        # row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert Material")
        # row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert All").scene_wide = True


        layout.prop( crn_mat, "use_nodes", toggle = True)
        
        layout.separator()
        row = layout.row( align = True)
        row.menu( "MATERIAL_MT_Corona_presets", text = MATERIAL_MT_Corona_presets.bl_label)
        row.operator( "corona.add_preset", text = "", icon = "ZOOM_IN")
        row.operator( "corona.add_preset", text = "", icon = "ZOOM_OUT")#.remove_active = True

        layout.separator()

        node_tree_selector_draw( layout, material, { 'CoronaMtlNode', 'CoronaLightMtlNode', 'CoronaVolumeMtlNode'})
        # if crn_mat.node_tree != '':
        #     node_tree = bpy.data.node_groups[ crn_mat.node_tree]
        # layout.prop_search( crn_mat, "node_output", material.node_tree, "nodes")

        if crn_mat.mtl_type == 'coronamtl':
            layout.prop( crn_mat, "as_portal")

        if crn_mat.mtl_type in ('coronalightmtl', 'coronamtl'):
            layout.prop( crn_mat, "use_lightmix")
            if crn_mat.use_lightmix:
                layout.prop( crn_mat, "lightmix_group")

        if not crn_mat.use_nodes:

        # if crn_mat.node_tree == '':
            layout.prop( crn_mat, "mtl_type")

            if crn_mat.mtl_type == 'coronaportalmtl':
                layout.label(text= "NOTE: Portal materials work differently in Corona")
                layout.label(text= "than in other renderers. You don't have to cover")
                layout.label(text= "all openings, and they work with scenes that have")
                layout.label(text= "both interior and exterior.")
                layout.label(text= "There is no poly count restriction, and there is")
                layout.label(text= "also a portal switch in regular CoronaMtl to turn")
                layout.label(text= " any material (e.g. glass window material) into a portal.")




#---------------------------------------
# Ubershader operator UI
#---------------------------------------
class CORONA_RENDER_PT_UbershaderPanel( bpy.types.Panel):
    bl_label = "Corona Material Nodes"
    bl_space_type = "NODE_EDITOR"
    bl_region_type = 'UI'

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer in ['CORONA', 'CYCLES'] and context.object is not None and context.object.type in ['MESH', 'SURFACE', 'META'] and context.object.active_material is not None

    def draw( self, context):
        layout = self.layout
        layout.operator( "corona.create_ubershader")
        layout.operator( "corona.migrate_use_nodes")

class CoronaButtonsPanel:
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll(cls, context):
        return context.engine in cls.COMPAT_ENGINES

class CORONA_PT_context_material(CoronaButtonsPanel, bpy.types.Panel):
    bl_label = ""
    bl_context = "material"
    bl_options = {'HIDE_HEADER'}

    @classmethod
    def poll(cls, context):
        if context.active_object and context.active_object.type == 'GPENCIL':
            return False
        else:
            return (context.material or context.object) and CoronaButtonsPanel.poll(context)

    def draw(self, context):
        layout = self.layout

        mat = context.material
        ob = context.object
        slot = context.material_slot
        space = context.space_data

        if ob:
            is_sortable = len(ob.material_slots) > 1
            rows = 1
            if (is_sortable):
                rows = 4

            row = layout.row()

            row.template_list("MATERIAL_UL_matslots", "", ob, "material_slots", ob, "active_material_index", rows=rows)

            col = row.column(align=True)
            col.operator("object.material_slot_add", icon='ADD', text="")
            col.operator("object.material_slot_remove", icon='REMOVE', text="")

            col.menu("MATERIAL_MT_context_menu", icon='DOWNARROW_HLT', text="")

            if is_sortable:
                col.separator()

                col.operator("object.material_slot_move", icon='TRIA_UP', text="").direction = 'UP'
                col.operator("object.material_slot_move", icon='TRIA_DOWN', text="").direction = 'DOWN'

            if ob.mode == 'EDIT':
                row = layout.row(align=True)
                row.operator("object.material_slot_assign", text="Assign")
                row.operator("object.material_slot_select", text="Select")
                row.operator("object.material_slot_deselect", text="Deselect")

        split = layout.split(factor=0.65)

        if ob:
            split.template_ID(ob, "active_material", new="material.new")
            row = split.row()

            if slot:
                row.prop(slot, "link", text="")
            else:
                row.label()
        elif mat:
            split.template_ID(space, "pin_id")
            split.separator()

classes = (
    CORONA_RENDER_PT_CMSMtlBump,
    CORONA_RENDER_PT_CMSMtlDiffuse,
    CORONA_RENDER_PT_CMSMtlEmission,
    CORONA_RENDER_PT_CMSMtlIES,
    CORONA_RENDER_PT_CMSMtlOpacity,
    CORONA_RENDER_PT_CMSMtlReflection,
    CORONA_RENDER_PT_CMSMtlRefraction,
    CORONA_RENDER_PT_CMSMtlVolume,
    CORONA_RENDER_PT_MaterialPreview,
    CORONA_RENDER_PT_MaterialShading,
    CORONA_RENDER_PT_UbershaderPanel,
    MATERIAL_MT_Corona_presets,
    CORONA_PT_context_material
    )

def register():
    for cls in classes:
        bpy.utils.register_class( cls)
        
    # bpy.utils.register_class( CMSMtlBump)
    # bpy.utils.register_class( CMSMtlDiffuse)
    # bpy.utils.register_class( CMSMtlEmission)
    # bpy.utils.register_class( CMSMtlIES)
    # bpy.utils.register_class( CMSMtlOpacity)
    # bpy.utils.register_class( CMSMtlReflection)
    # bpy.utils.register_class( CMSMtlRefraction)
    # bpy.utils.register_class( CMSMtlVolume)
    # bpy.utils.register_class( CoronaMaterialPreview)
    # bpy.utils.register_class( CoronaMaterialShading)
    # bpy.utils.register_class( CoronaUbershaderPanel)
    # bpy.utils.register_class( MATERIAL_MT_Corona_presets)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class( cls)

    # bpy.utils.unregister_class( CMSMtlBump)
    # bpy.utils.unregister_class( CMSMtlDiffuse)
    # bpy.utils.unregister_class( CMSMtlEmission)
    # bpy.utils.unregister_class( CMSMtlIES)
    # bpy.utils.unregister_class( CMSMtlOpacity)
    # bpy.utils.unregister_class( CMSMtlReflection)
    # bpy.utils.unregister_class( CMSMtlRefraction)
    # bpy.utils.unregister_class( CMSMtlVolume)
    # bpy.utils.unregister_class( CoronaMaterialPreview)
    # bpy.utils.unregister_class( CoronaMaterialShading)
    # bpy.utils.unregister_class( CoronaUbershaderPanel)
    # bpy.utils.unregister_class( MATERIAL_MT_Corona_presets)



