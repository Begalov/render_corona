import bpy

#---------------------------------------
# Sun UI
#---------------------------------------
class CORONA_RENDER_PT_SunPanel(bpy.types.Panel):
    bl_label = "Corona Sun"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    COMPAT_ENGINES = {'CORONA'}
    bl_context = "world"

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'

    def draw_header( self, context):
        crn_sky_props = context.scene.corona_sky
        header = self.layout

        header.scale_y = 1.0
        header.prop( crn_sky_props, "use_sun")

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_sky_props = scene.corona_sky

        layout.active = crn_sky_props.use_sun
        layout.label('Use only English characters for the sun name or it will fail')
        split = layout.split()
        col = split.column()
        col.label( "Sun to export:")
        col.prop( crn_sky_props, "sun_lamp", text = '')
        col.label( "Sun Color Multiplier:")
        col.prop( crn_sky_props, "sun_color", text = "")

        col = split.column()
        col.label( "Size Multiplier:")
        col.prop( crn_sky_props, "sun_size_mult", text = "")
        col.label( "Color Mode")
        col.prop( crn_sky_props, "color_mode", text = "")
        col.label( "Turbidity")
        col.prop( crn_sky_props, "turbidity", text = "")

#---------------------------------------
# Environment UI
#---------------------------------------
class CORONA_RENDER_PT_WorldPanel(bpy.types.Panel):
    bl_label = "Corona Environment"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "world"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        world = scene.world
        crn_world = world.corona

        layout.prop( crn_world, "mode")
        if crn_world.mode == 'color':
            layout.prop( crn_world, "enviro_color", text = "")

        if crn_world.mode == 'latlong':
            layout.prop( crn_world, "enviro_tex")

            layout.prop( crn_world, "latlong_use_gamma")
            if crn_world.latlong_use_gamma == True:
                layout.prop( crn_world, "latlong_gamma")
            layout.prop( crn_world.map_gi, "intensity")
            layout.prop( crn_world, "latlong_mode")
            layout.prop( crn_world, "latlong_map_channel")
            layout.prop( crn_world, "latlong_scale")
            layout.prop( crn_world, "latlong_offset")
            layout.prop( crn_world, "latlong_rotate_z")
            layout.prop( crn_world, "latlong_enviro_rotate")
            layout.prop( crn_world, "latlong_enviro_mode")
            layout.prop( crn_world, "latlong_wrap_mode_u")
            layout.prop( crn_world, "latlong_wrap_mode_v")
            layout.prop( crn_world, "latlong_blur")
            layout.prop( crn_world, "latlong_use_real_scale")

        if crn_world.mode == 'sky':
            row = layout.row()
            row.prop( crn_world, "sky_intensity")
            row.prop( crn_world, "sky_turbidity")
            layout.prop( crn_world, "sky_ground_color")
            layout.prop( crn_world, "sky_ground_affects_sky")

        if crn_world.mode == 'rayswitch':
            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "gi_color")
            col = split.column()
            col.prop( crn_world, "gi_use_tex", text = "T", toggle = True)
            if crn_world.gi_use_tex:
                layout.prop( crn_world.map_gi, "show_settings")
                if crn_world.map_gi.show_settings:
                    layout.prop( crn_world.map_gi, "texture")
                    layout.prop( crn_world.map_gi, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_gi, "uOffset")
                    row.prop( crn_world.map_gi, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_gi, "uScaling")
                    row.prop( crn_world.map_gi, "vScaling")

            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "reflect_color")
            col = split.column()
            col.prop( crn_world, "reflect_use_tex", toggle = True, text = "T")
            if crn_world.reflect_use_tex:
                layout.prop( crn_world.map_reflect, "show_settings")
                if crn_world.map_reflect.show_settings:
                    layout.prop( crn_world.map_reflect, "texture")
                    layout.prop( crn_world.map_reflect, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_reflect, "uOffset")
                    row.prop( crn_world.map_reflect, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_reflect, "uScaling")
                    row.prop( crn_world.map_reflect, "vScaling")

            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "refract_color")
            col = split.column()
            col.prop( crn_world, "refract_use_tex", toggle = True, text = "T")
            if crn_world.refract_use_tex:
                layout.prop( crn_world.map_refract, "show_settings")
                if crn_world.map_refract.show_settings:
                    layout.prop( crn_world.map_refract, "texture")
                    layout.prop( crn_world.map_refract, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_refract, "uOffset")
                    row.prop( crn_world.map_refract, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_refract, "uScaling")
                    row.prop( crn_world.map_refract, "vScaling")

            split = layout.split( percentage = 0.85)
            col = split.column()
            row = col.row()
            row.prop( crn_world, "direct_color")
            col = split.column()
            col.prop( crn_world, "direct_use_tex", toggle = True, text = "T")
            if crn_world.direct_use_tex:
                layout.prop( crn_world.map_direct, "show_settings")
                if crn_world.map_direct.show_settings:
                    layout.prop( crn_world.map_direct, "texture")
                    layout.prop( crn_world.map_direct, "intensity")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_direct, "uOffset")
                    row.prop( crn_world.map_direct, "vOffset")
                    row = layout.row( align = True)
                    row.prop( crn_world.map_direct, "uScaling")
                    row.prop( crn_world.map_direct, "vScaling")

        if crn_world.mode == 'node':
            layout.label('Use the first world node.')
            layout.label('In the node editor click on the world icon and add a Corona Output node.')
            layout.label('Connect a Corona Map to the output NOT a Material')

        layout.separator()

        # Volumetric / Glass Absorption
        layout.prop( crn_world, "use_global_medium")
        if crn_world.use_global_medium:
            box = layout.box()
            box.label( "Absorption:")

            split = box.split( percentage = 0.85)
            col = split.column()
            col.prop( crn_world, "absorption_color", text = "")

            col = split.column()
            col.prop( crn_world, "use_map_absorption", text = "T", toggle = True)
            if crn_world.use_map_absorption:
                box.prop( crn_world.map_absorption, "show_settings")
                if crn_world.map_absorption.show_settings:
                    box.prop_search( crn_world.map_absorption,
                                    "texture",
                                    world,
                                    "texture_slots",
                                    text = "Texture")
                    box.prop( crn_world.map_absorption, "intensity")
                    row = box.row( align = True)
                    row.prop( crn_world.map_absorption, "uOffset")
                    row.prop( crn_world.map_absorption, "vOffset")
                    row = box.row( align = True)
                    row.prop( crn_world.map_absorption, "uScaling")
                    row.prop( crn_world.map_absorption, "vScaling")

            box.prop( crn_world, "absorption_distance", text = "Absorption Distance")

            box.label( "Scattering:")
            split = box.split( percentage = 0.85)
            col = split.column()
            col.prop( crn_world, "scattering_albedo", text = "")

            col = split.column()
            col.prop( crn_world, "use_map_scattering", text = "T", toggle = True)
            if crn_world.use_map_scattering:
                box.prop( crn_world.map_scattering, "show_settings")
                if crn_world.map_scattering.show_settings:
                    box.prop_search( crn_world.map_scattering,
                                    "texture",
                                    world,
                                    "texture_slots",
                                    text = "Texture")
                    box.prop( crn_world.map_scattering, "intensity")
                    row = box.row( align = True)
                    row.prop( crn_world.map_scattering, "uOffset")
                    row.prop( crn_world.map_scattering, "vOffset")
                    row = box.row( align = True)
                    row.prop( crn_world.map_scattering, "uScaling")
                    row.prop( crn_world.map_scattering, "vScaling")

            box.prop( crn_world, "mean_cosine")


        # layout.prop( crn_world, "use_global_material")

classes = (
    CORONA_RENDER_PT_WorldPanel,
    CORONA_RENDER_PT_SunPanel
    )

def register():
    for cls in classes:
        bpy.utils.register_class( cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class( cls)
