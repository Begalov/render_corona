import bpy

#---------------------------------------
# Object proxy settings UI
#---------------------------------------
class CORONA_RENDER_PT_ObjectPropsPanel( bpy.types.Panel):
    bl_label = "Corona Object Properties"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer in cls.COMPAT_ENGINES and context.active_object is not None and context.active_object.type in ['MESH', 'SURFACE', 'META']

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_obj = context.object.corona
        row = layout.row()
        row.prop( crn_obj, "use_mblur")
        if crn_obj.use_mblur:
            row.prop( crn_obj, "mblur_type")

        layout.separator()

        layout.label( "Proxy Settings:")
        layout.prop( crn_obj, "is_proxy", text = "Use As Corona Proxy")
        col = layout.column()
        col.active = crn_obj.is_proxy
        col.prop( crn_obj, "use_external")
        if crn_obj.use_external:
            col.prop( crn_obj, "external_instance_mesh")
            col.prop( crn_obj, "external_mtllib", text = "External .mtl")
        else:
            col.prop_search( crn_obj, "instance_mesh", scene, "objects")

        layout.separator()

        layout.label( "Object Overrides:")
        layout.prop( crn_obj, "override_exclude", text = "Exclude Object From Material Override")


class CORONA_RENDER_PT_ObjectProxyExportPanel( bpy.types.Panel):
    bl_label = "Corona Proxy Exporter"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll(cls, context):
        renderer = context.scene.render.engine
        return renderer in cls.COMPAT_ENGINES and context.active_object is not None and context.active_object.type in ['MESH']

    def draw(self, context):
        crn_obj = context.object.corona

        layout = self.layout


        split = layout.split(percentage=0.2)
        col = split.column()
        col.label("%s:" % "Path")

        col = split.row(align=True)
        col.prop(crn_obj, "proxy_export_path", text="")


        split = layout.split(percentage=0.2)
        col = split.column()
        col.label("%s:" % "Name")

        row = split.row(align=True)
        row.prop(crn_obj, "proxy_export_name", text="")
        row.operator("corona.copy_object_name", text="", icon="SYNTAX_OFF")


        row = layout.row()
        row.operator("corona.create_proxy")


class CORONA_RENDER_PT_ObjectMeshPanel( bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Vertex Colors"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll(cls, context):
        engine = context.scene.render.engine
        return context.mesh and (engine in cls.COMPAT_ENGINES)

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_obj = context.object.corona
        layout.prop( crn_obj, "export_vcol", text = "Export Vertex Colors")

def register():
    bpy.utils.register_class( CORONA_RENDER_PT_ObjectPropsPanel)

def unregister():
    bpy.utils.unregister_class( CORONA_RENDER_PT_ObjectPropsPanel)
