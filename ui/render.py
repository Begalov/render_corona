import bpy

#---------------------------------------
# Render UI
#---------------------------------------
class CoronaRenderPanelBase( object):
    bl_context = "render"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
# class CoronaButtonsPanel:
#     bl_space_type = "PROPERTIES"
#     bl_region_type = "WINDOW"
#     bl_context = "render"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        # renderer = context.scene.render
        # return renderer.engine == 'CORONA'
    # @classmethod
#     def poll(cls, context):
        return context.engine in cls.COMPAT_ENGINES


# from cycles/ui.py:42
#
# class CoronaButtonsPanel:
#     bl_space_type = "PROPERTIES"
#     bl_region_type = "WINDOW"
#     bl_context = "render"
#     COMPAT_ENGINES = {'CORONA'}

#     @classmethod
#     def poll(cls, context):
#         return context.engine in cls.COMPAT_ENGINES
#         
#---------------------------------------
# Render buttons UI
#---------------------------------------
class CORONA_RENDER_PT_Buttons( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Render"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_scene_props = scene.corona

        row = layout.row( align=True)
        row.operator( "render.render", text = "Render", icon = 'RENDER_STILL')
        # if crn_scene_props.vfb_type == '0':
        row.operator( "render.render", text = "Animation", icon = 'RENDER_ANIMATION').animation = True
        row = layout.row( align=True)
        row.operator( "corona.export", text = "Export Objects", icon = 'EXPORT')
        row.operator( "corona.export_mat", text = "Materials", icon = 'MATERIAL')
        row.operator( "corona.export_scene", text = "Configuration", icon = 'FILE')

        row = layout.row( align=True)
        row.operator( "corona.check_textures", text = "Check Textures", icon = 'EXPORT')

        row = layout.row()
        row.prop( crn_scene_props, "obj_export_bool")
        if crn_scene_props.obj_export_bool:
            row.prop( crn_scene_props, "obj_export_mode", text = "")

        layout.prop( crn_scene_props, "binary_obj")
        layout.prop( crn_scene_props, "export_hair")
        row = layout.row()
        row.prop( crn_scene_props, "stream_hair")
        row.active = crn_scene_props.export_hair and crn_scene_props.binary_obj
        layout.prop( crn_scene_props, "local_view_only")
        if not crn_scene_props.local_view_only:
            layout.prop( scene.render, "display_mode")
        layout.separator()

        layout.prop( crn_scene_props, "export_path", text = "Export Path")

        layout.prop( crn_scene_props, "corona_conf")


        layout.prop( crn_scene_props, "image_format")
        # if crn_scene_props.image_format != '.jpg':
        layout.prop( crn_scene_props, "save_alpha")

        box = layout.box()
        box.label(text= 'Additional formats:')
        if crn_scene_props.image_format != '.exr':
            box.prop( crn_scene_props, "image_format_exr")
        if crn_scene_props.image_format != '.png':
            box.prop( crn_scene_props, "image_format_png")
        if crn_scene_props.image_format != '.jpg':
            box.prop( crn_scene_props, "image_format_jpg")
        if crn_scene_props.image_format != '.tiff':
            box.prop( crn_scene_props, "image_format_tiff")
        if crn_scene_props.image_format != '.tga':
            box.prop( crn_scene_props, "image_format_tga")

#---------------------------------------
# Add dimensions and stamp panels
#---------------------------------------
# from bl_ui import properties_output
# properties_output.RENDER_PT_dimensions.COMPAT_ENGINES.add( 'CORONA')
# properties_output.RENDER_PT_stamp.COMPAT_ENGINES.add( 'CORONA')
# properties_output.RENDER_PT_output.COMPAT_ENGINES.add( 'CORONA')
# del properties_render

#---------------------------------------
# Render settings UI
#---------------------------------------
class CORONA_RENDER_PT_SettingsPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Render Settings"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        # Renderer settings
        # Bucket
        layout.prop( crn_props, "renderer_type")
        if crn_props.renderer_type != 'VCM':
            layout.prop( crn_props, "gi_primarysolver")
            layout.prop( crn_props, "gi_secondarysolver")

        layout.separator()
        if crn_props.renderer_type == 'VPL':
            row = layout.row()
            row.prop( crn_props, "vpl_emitted_count")
            row.prop( crn_props, "vpl_used_count")
            row = layout.row()
            row.prop( crn_props, "vpl_clamping")
            row.prop( crn_props, "vpl_progressive_batch")


        # VCM/Bidir
        if crn_props.renderer_type == 'VCM':
            row = layout.row()
            row.prop( crn_props, "vcm_mode")
            # if crn_props.vcm_mode == '4':
            row.prop( crn_props, "bidir_mis")

        # PPM
        if crn_props.renderer_type == 'PPM':
            row = layout.row()
            row.prop( crn_props, "ppm_samples")
            row.prop( crn_props, "ppm_photons")

            row = layout.row()
            row.prop( crn_props, "ppm_initial_rad")
            row.prop( crn_props, "ppm_alpha")
        # Progressive
        if crn_props.renderer_type == 'Progressive':
            layout.label(text= "Noise Target:")
            layout.prop( crn_props, "prog_recalculate")
            layout.prop( crn_props, "prog_adaptivity")

        layout.separator()

        layout.prop( crn_props, "limit_prog_time")
        if crn_props.limit_prog_time:
            layout.label(text=  "Time Limit:")
            row = layout.row(align = True)
            row.prop( crn_props, "prog_timelimit_hour", text = "Hours")
            row.prop( crn_props, "prog_timelimit_min", text = "Minutes")
            row.prop( crn_props, "prog_timelimit_sec", text = "Seconds")
        layout.label(text= "Pass Limit:")
        layout.prop( crn_props, "prog_max_passes")

        layout.separator()

        layout.label(text=  "Sampling:")
        layout.prop( crn_props, "path_samples")
        layout.prop( crn_props, "max_sample_intensity")
        layout.prop( crn_props, "max_depth")
        layout.prop( crn_props, "random_seed")

        split = layout.split()
        col = split.column()
        col.prop( crn_props, "num_threads")
        col.active = crn_props.auto_threads is False

        col = split.column()
        col.prop(crn_props, "auto_threads", text = "Auto Threads")

        row = layout.row()
        row.prop( crn_props, "material_override")
        if crn_props.material_override:
            row.prop( crn_props, "clay_render")
            row = layout.row()
            row.prop( crn_props, "override_material")
            if crn_props.clay_render == True:
                row.active = False

        layout.separator()
        layout.separator()

        layout.label(text= "Light Sampling:")
        layout.prop( crn_props, "arealight_samples")

class CORONA_RENDER_PT_SharpenBlur( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Sharpen/Blur"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        layout.prop( crn_props, "sharpen_blur_enable", text = "Enable")
        if crn_props.sharpen_blur_enable:
            layout.prop( crn_props, "sharpen_amount")
            layout.prop( crn_props, "sharpen_radius")
            layout.prop( crn_props, "blur_radius")

class CORONA_RENDER_PT_Displacement( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Displacement"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        # layout.label( "Displacement:")
        layout.prop(crn_props, "displace_type")
        row = layout.row()
        row.prop(crn_props, "displace_max_screen")
        row.prop(crn_props, "displace_max_world")
        row = layout.row()
        row.prop(crn_props, "displace_filter_map")
        row.prop(crn_props, "displace_smooth_normals")
        layout.prop(crn_props, "displace_frustrum_mult")

class CORONA_RENDER_PT_Denoise( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Denoise"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        # layout.label( "Denoise:")
        row = layout.row()
        row.prop( crn_props, "denoise_filter")
        row.prop( crn_props, "denoise_sensitivity")
        row = layout.row()
        row.prop( crn_props, "denoise_blur")
        row.prop( crn_props, "denoise_amt")

        layout.separator()

#---------------------------------------
# Render GI UI
#---------------------------------------
class CORONA_RENDER_PT_GIPanel(bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona UHD Cache"
    COMPAT_ENGINES = {'CORONA'}

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        #----------------------------------
        # Global Illumination parameters
        # Secondary solver
        # layout.prop( crn_props, "gi_secondarysolver", text = "Solver")
        # if crn_props.gi_secondarysolver == '3' and crn_props.gi_primarysolver != '3':
        #     layout.separator()
        #     layout.prop( crn_props, "hd_precomp_mult", text = "Precomp Multiplier")
        #     layout.prop( crn_props, "hd_pt_samples")
        #     layout.prop( crn_props, "hd_interpolation_count")
        #     layout.prop( crn_props, "hd_max_records")

        #     layout.separator()

        #     layout.label("Record Sensitivity:")

        #     box = layout.box()
        #     box.prop( crn_props, "hd_sens_direct", text = "Directional")
        #     box.prop( crn_props, "hd_sens_normal")
        if crn_props.gi_primarysolver == 'UHDCache' or crn_props.gi_secondarysolver == 'UHDCache':
            layout.prop( crn_props, "hd_precomp_mode")
            layout.prop( crn_props, "hd_precomp_mult")
            layout.prop( crn_props, "hd_sens_direct")
            layout.prop( crn_props, "hd_interpolation_count")
            layout.prop( crn_props, "hd_sens_normal")
            layout.prop( crn_props, "hd_pt_samples")
            layout.prop( crn_props, "hd_glossy_thresh")
            layout.prop( crn_props, "uhd_strictness")
            layout.prop( crn_props, "uhd_msi")
            layout.prop( crn_props, "uhd_correlatePrecomp")
            layout.prop( crn_props, "uhd_precision")
            layout.prop( crn_props, "hd_max_records")

            layout.separator()

            row = layout.row()
            row.prop( crn_props, "save_secondary_gi")

            row.prop( crn_props, "load_secondary_gi")
            layout.prop( crn_props, "gi_secondaryfile")

#---------------------------------------
# VFB UI
#---------------------------------------
class CORONA_RENDER_PT_VFBPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Virtual Framebuffer"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        #------------------------------
        # Virtual framebuffer parameters
        layout.prop( crn_props, "vfb_type", text = "VFB")
        layout.prop( crn_props, "vfb_update", text = "VFB Update Rate [ms]")

        split = layout.split()
        col = split.column()
        col.label(text= "Image Filter:")
        col.prop( crn_props, "filter_width")

        col = split.column()
        col.prop( crn_props, "image_filter", text = "")
        col.prop( crn_props, "filter_blur")

        layout.prop( crn_props, "renderstamp_use")

#---------------------------------------
# Post-processing UI
#---------------------------------------
class CORONA_RENDER_PT_PostPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Color Mapping"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        layout.prop( crn_props, "colmap_use_photographic")
        row = layout.row()
        row.prop( crn_props, "colmap_exposure")
        if crn_props.colmap_use_photographic:
            row.active = False

        layout.prop( crn_props, "colmap_compression")

        layout.separator()

        layout.prop( crn_props, "colmap_tint")
        layout.prop( crn_props, "colmap_color_temp")
        row = layout.row()
        row.prop( crn_props, "colmap_contrast")
        row.prop( crn_props, "colmap_saturation")
        row = layout.row()
        row.prop( crn_props, "colmap_filmic_shadow")
        row.prop( crn_props, "colmap_filmic_highlight")

class CORONA_RENDER_PT_PostBloom( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Bloom & Glare"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona
        layout.prop( crn_props, "bloom_enabled")
        row = layout.row()
        row.prop( crn_props, "bloom_intensity")       # (def: 0, min: 0, max: 100000)
        row.prop( crn_props, "bloom_glare_intensity") # (def: 0, min: 0, max: 100000)
        row = layout.row()
        row.prop( crn_props, "bloom_streak_count")    # (def: 3, min: 1, max: 8)
        row.prop( crn_props, "bloom_rotation")        # (def: 0, min: -1e+10, max: 1e+10)
        row = layout.row()
        row.prop( crn_props, "bloom_streak_blur")     # (def: 0, min: 0, max: 1)
        row.prop( crn_props, "bloom_color_intensity") # (def: 0, min: 0, max: 1)
        row = layout.row()
        row.prop( crn_props, "bloom_color_shift")     # (def: 0, min: 0, max: 1)
        row.prop( crn_props, "bloom_threshold")       # (def: 1, min: 0, max: 100000)

class CORONA_RENDER_PT_PostVignette( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Vignette"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona
        row = layout.row()
        row.prop( crn_props, "vignette_intensity") # (def: 0, min: 0, max: 1)
        row.prop( crn_props, "vignette_falloff")   # (def: 0, min: 0, max: 1)
        row = layout.row()
        row.prop( crn_props, "vignette_offset_x")  # (def: 0, min: -1, max: 1)
        row.prop( crn_props, "vignette_offset_y")  # (def: 0, min: -1, max: 1)


class CORONA_RENDER_PT_PostLUT( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Look Up Table"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona
        layout.prop( crn_props, "colmap_lut_enable")
        row = layout.row()
        row.active = crn_props.colmap_lut_enable
        row.prop( crn_props, "colmap_path")
        row = layout.row()
        row.active = crn_props.colmap_lut_enable
        row.prop( crn_props, "colmap_opacity")
        row = layout.row()
        row.active = crn_props.colmap_lut_enable
        row.prop( crn_props, "colmap_use_log")

classes = (
    CORONA_RENDER_PT_Buttons,
    CORONA_RENDER_PT_SettingsPanel,
    CORONA_RENDER_PT_SharpenBlur,
    CORONA_RENDER_PT_Displacement,
    CORONA_RENDER_PT_Denoise,
    CORONA_RENDER_PT_GIPanel,
    CORONA_RENDER_PT_VFBPanel,
    CORONA_RENDER_PT_PostPanel,
    CORONA_RENDER_PT_PostBloom,
    CORONA_RENDER_PT_PostVignette,
    CORONA_RENDER_PT_PostLUT
    )

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class( cls)

    # bpy.utils.register_class( CoronaRenderButtons)
    # bpy.utils.register_class( CoronaRenderSettingsPanel)
    # bpy.utils.register_class( CoronaRenderDenoise)
    # bpy.utils.register_class( CoronaRenderDisplacement)
    # bpy.utils.register_class( CoronaRenderSharpenBlur)
    # bpy.utils.register_class( CoronaPostPanel)
    # bpy.utils.register_class( CoronaPostBloom)
    # bpy.utils.register_class( CoronaPostVignette)
    # bpy.utils.register_class( CoronaPostLUT)
    # bpy.utils.register_class( CoronaVFBPanel)
    # bpy.utils.register_class( CoronaGIPanel)

def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class( cls)
    # bpy.utils.unregister_class( CoronaRenderButtons)
    # bpy.utils.unregister_class( CoronaRenderSettingsPanel)
    # bpy.utils.unregister_class( CoronaRenderDenoise)
    # bpy.utils.unregister_class( CoronaRenderDisplacement)
    # bpy.utils.unregister_class( CoronaRenderSharpenBlur)
    # bpy.utils.unregister_class( CoronaPostPanel)
    # bpy.utils.unregister_class( CoronaPostBloom)
    # bpy.utils.unregister_class( CoronaPostVignette)
    # bpy.utils.unregister_class( CoronaPostLUT)
    # bpy.utils.unregister_class( CoronaVFBPanel)
    # bpy.utils.unregister_class( CoronaGIPanel)
