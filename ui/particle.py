import bpy

#---------------------------------------
# Particle settings UI
#---------------------------------------
class CORONA_RENDER_PT_PsysPanel( bpy.types.Panel):
    bl_label = "Corona Hair Rendering"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "particle"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        psys = context.particle_system
        return renderer == 'CORONA' and context.object is not None and psys and psys.settings.type == 'HAIR'

    def draw_header( self, context):
        pass

    def draw( self, context):
        layout = self.layout
        crn_psys = context.particle_system.settings.corona

        layout.prop( crn_psys, "shape")
        layout.prop( crn_psys, "export_color")
        layout.prop( crn_psys, "resolution")
        layout.prop( crn_psys, "hair_shape")
        layout.label( "Thickness:")
        row = layout.row()
        row.prop( crn_psys, "root_size")
        row.prop( crn_psys, "tip_size")
        row = layout.row()
        row.prop( crn_psys, "scaling")
        row.prop( crn_psys, "close_tip")


# Copied from addons\cycles\ui.py
class CORONA_RENDER_PT_ParticleTextures(bpy.types.Panel):
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_label = "Textures"
    bl_context = "particle"
    bl_options = {'DEFAULT_CLOSED'}
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll(cls, context):
        psys = context.particle_system
        rd = context.scene.render
        return psys and rd.engine in cls.COMPAT_ENGINES

    def draw(self, context):
        layout = self.layout

        psys = context.particle_system
        part = psys.settings

        row = layout.row()
        row.template_list("TEXTURE_UL_texslots", "", part, "texture_slots", part, "active_texture_index", rows=2)

        col = row.column(align=True)
        col.operator("texture.slot_move", text="", icon='TRIA_UP').type = 'UP'
        col.operator("texture.slot_move", text="", icon='TRIA_DOWN').type = 'DOWN'
        col.menu("TEXTURE_MT_specials", icon='DOWNARROW_HLT', text="")

        if not part.active_texture:
            layout.template_ID(part, "active_texture", new="texture.new")
        else:
            slot = part.texture_slots[part.active_texture_index]
            layout.template_ID(slot, "texture", new="texture.new")

def register():
    bpy.utils.register_class( CORONA_RENDER_PT_PsysPanel)
    bpy.utils.register_class( CORONA_RENDER_PT_ParticleTextures)

def unregister():
    bpy.utils.unregister_class( CORONA_RENDER_PT_PsysPanel)
    bpy.utils.unregister_class( CORONA_RENDER_PT_ParticleTextures)