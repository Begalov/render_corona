import bpy
from . import material, object, particle, passes, render, world, camera, lamp
from ..util import CrnUpdate

def CrnAddPanel( element):
    print( "Adding UI Panel:", element)

# Use some of the existing Blender UI elements.
# from cycles/ui.py:2081
def get_panels():
    exclude_panels = {
        'DATA_PT_area',
        'DATA_PT_camera_dof',
        'DATA_PT_falloff_curve',
        'DATA_PT_light',
        'DATA_PT_preview',
        'DATA_PT_spot',
        'MATERIAL_PT_context_material',
        'MATERIAL_PT_preview',
        'NODE_DATA_PT_light',
        'NODE_DATA_PT_spot',
        'VIEWLAYER_PT_filter',
        'VIEWLAYER_PT_layer_passes',
        'RENDER_PT_post_processing',
        'RENDER_PT_simplify',
        }

    panels = []
    for panel in bpy.types.Panel.__subclasses__():
        if hasattr(panel, 'COMPAT_ENGINES') and 'BLENDER_RENDER' in panel.COMPAT_ENGINES:
            if panel.__name__ not in exclude_panels:
                panels.append(panel)
    # print('Panels:', '\n'.join([str(p.__name__) for p in panels]))
    return panels

submodules = (
    render,
    camera,
    material,
    world,
    particle,
    passes,
    lamp
    )

def register():
    for sub in submodules:
        sub.register()

    for panel in get_panels():
        panel.COMPAT_ENGINES.add('CORONA')


def unregister():
    for sub in submodules:
        sub.unregister()

    for panel in get_panels():
        if 'CYCLES' in panel.COMPAT_ENGINES:
            panel.COMPAT_ENGINES.remove('CORONA')

