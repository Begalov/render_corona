import bpy
from bpy.app.handlers   import persistent

@persistent
def dg_upd_post_updated_data(scene):
    for du in bpy.context.depsgraph.updates.values():
        # print(du.id)
        # print(du.id.type) if hasattr(du.id, 'type') else 'no type'
        # print(du.id.data.name) if hasattr(du.id, 'data') and du.id.data is not None else 'no data'
        if hasattr(du.id, 'type') and du.id.type == 'MESH' and du.id.data is not None and not du.id.corona.is_dirty:
            du.id.corona.is_dirty = True

def register():
    bpy.app.handlers.depsgraph_update_post.append(dg_upd_post_updated_data)

def unregister():
    for f in bpy.app.handlers.depsgraph_update_post:
        if f.__name__ == 'dg_upd_post_updated_data':
            bpy.app.handlers.depsgraph_update_post.remove(f)